# Latitude & Notifi Protocols

This project contains Java implementation of the latitude and notifi protocols for location sharing. The project was completed as part of Data Communications class at Baylor University.

## Features

This project allows groups of users to share their locations on a distinct maps. User client can see last positions of all other users on map, add new locations and receive realtime notifications of location updates. Moreover, the server provides a map with pins at the locations.

## Structure

Project consists of 4 parts.

- Latitude server which manages the maps and locations with TCP.
- Latitude client with simple command-line interface allowing to connect to the server.
- NoTiFi server, providing the realtime updates through UDP.
- NoTiFi client to recieve the location updates.

## Installation

The project had to follow exact specitication of the course: it is not a maven, it complies with java 10 and uses jUnit 5 tests.

- After checking this repository, open the project in Eclipse
- Manually add library to draw the map: Rightclick on project folder -> Preferences -> JavaBuild Path -> Libraries -> Classpath -> Add JARs -> /lib/Map/map.jar
- Next, still in the preferences go to Java Compiler, Enable roject Specific and set Compiler Compliance Level to 10.
- The jUnit probably won't be installed by default, so open any test file, Eclipse will show missing dependencies, click on resolve and add jUnit 5 Jupiter.

## Running the tests

Now, the eclipse should be able to run the tests and the main classes. Right click on the /src folder -> run as -> jUnit tests.

## Running the project locally

To run the project, first start the Latitude Server from latitude.app/LatitudeServer.java with parameters: port pool-size passwordsFileName (e.g. 12345 10 passwords)
(The passwords file simulates repository of known users.)

Next run the NoTiFi client from notifi.app/NoTiFiClient.java with parameters: serverLocation port clientLocation (e.g. localhost 12345 localhost)

And finally run the Latitude client from latitude.app/LatitudeClientApp.java with parameters: serverLocation port (e.g. localhost 12345)

### Interaction

The Latitude Client supports two commands: ALL - downloads all locations and NEW - creates (or updates) user's location
The default Map ID for the course testing purposes is 345, that is the only map working in this demo. The userIDs are saved in the passwords file, you can use 79.

An example interation could be:

```
Operation> NEW
MapID> 345
UserID> 79
Longitude> 22.4568
Latitude> 32.456844
Location Name> Test Place
Location Description> Random place description
mapID=345 - Location Response
User 79: tony: Test Place - Random place description at (22.4568, 32.456844)

Operation> ALL
MapID> 345
mapID=345 - Location Response
User 79: tony: Test Place - Random place description at (22.4568, 32.456844)
```

Once you enter the NEW location, your NoTiFi client should receive a notification, that location has bee updated:

```
Addition lat: 32.456844 lon: 22.4568 name: Test Place desc: Random place description
```

The change is also logged to file connections.log and if you open the lib/Map/index.html, you should see a pin on the specified location.

![map view with pin](https://bitbucket.org/antoninsmid/latitude-notifi-protocols/raw/e8bb0684b1ef55af483e4135e1df1ccd055c037a/screen.png)
