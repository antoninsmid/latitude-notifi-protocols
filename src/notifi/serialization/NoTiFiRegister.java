/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4
* Class: CSI4321
*
************************************************/
package notifi.serialization;

import java.io.EOFException;
import java.io.IOException;
import java.net.Inet4Address;
import java.util.Arrays;

import notifi.util.IOHelper;

/**
 * Class representing NoTiFi register message
 * Used to register for receiving updates about new positions.
 * 
 */
public class NoTiFiRegister extends NoTiFiMessage {

    /**
     * Code for NoTiFi register
     */
    public static final int CODE = 0;

    private Inet4Address address;
    private int port;

    /**
     * Constructs NoTiFi register from values
     * 
     * @param msgId   message ID
     * @param address address to register
     * @param port    port to register
     * @throws java.lang.IllegalArgumentException if message ID invalid
     */
    public NoTiFiRegister(int msgId, Inet4Address address, int port) throws IllegalArgumentException {
        super(msgId);
        setAddress(address);
        setPort(port);
    }

    /**
     * Simple constructor used during decode
     * 
     * @param msgId
     */
    protected NoTiFiRegister(int msgId) {
        super(msgId);
    }

    /**
     * Get (de)register address
     * 
     * @return (de)register address
     */
    public Inet4Address getAddress() {
        return IOHelper.cloneAddress(address);
    }

    /**
     * Set (de)register address
     * 
     * @param address (de)register address
     * @throws java.lang.IllegalArgumentException if address is null
     */
    public void setAddress(Inet4Address address) throws IllegalArgumentException {
        if (address == null) {
            throw new IllegalArgumentException("address is null");
        }
        this.address = IOHelper.cloneAddress(address);
    }

    /**
     * sets addrss to null for testing purposes
     */
    public void setAddressNull() {
        this.address = null;
    }

    /**
     * Get (de)register port
     * 
     * @return (de)register port
     */
    public int getPort() {
        return port;
    }

    /**
     * Set (de)register port
     * 
     * @param port port - (de)register port
     * @throws java.lang.IllegalArgumentException if port is out of range
     *         (0...65535)
     */
    public void setPort(int port) throws IllegalArgumentException {
        if (!notifi.util.Validator.validatePort(port)) {
            throw new IllegalArgumentException("bad port");
        }
        this.port = port;
    }

    @Override
    public int getCode() {
        return CODE;
    }

    @Override
    protected void decodeSpecific(byte[] pkt) throws IOException {
        // validate length
        if (pkt.length != 8) {
            throw new EOFException("register message packet does not have the right length");
        }

        // decode address from 32bit little endian
        byte[] address = IOHelper.swapEndianity(Arrays.copyOfRange(pkt, 2, 6));
        try {
            setAddress((Inet4Address) Inet4Address.getByAddress(address));
        } catch (Exception e) {
            // should not happen, there should be always some address
            throw new IOException();
        }

        // decode port from 16bit little endian
        int port = pkt[7] & 0xFF;
        port = port << 8;
        port |= pkt[6] & 0xFF;
        setPort(port);
    }

    @Override
    public byte[] encode() {
        byte[] header = super.encode();
        byte[] addr = IOHelper.swapEndianity(address.getAddress());
        byte[] portOut = new byte[2];
        portOut[0] = (byte) port;
        portOut[1] = (byte) (port >> 8);

        byte[] encoded = new byte[8];
        System.arraycopy(header, 0, encoded, 0, header.length);
        System.arraycopy(addr, 0, encoded, header.length, addr.length);
        System.arraycopy(portOut, 0, encoded, header.length + addr.length, portOut.length);

        return encoded;
    }

    @Override
    public String toString() {
        return "NoTiFiRegister [msgId:" + getMsgId() + ", address=" + address + ", port=" + port + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + port;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        NoTiFiRegister other = (NoTiFiRegister) obj;
        if (address == null) {
            if (other.address != null)
                return false;
        } else if (!address.equals(other.address))
            return false;
        if (port != other.port)
            return false;
        return true;
    }

}
