package notifi.serialization;

/**
 * Class representing an unexpected code expection.
 * Is supposed to be thrown in case, the notifi message code is unrecognized.
 * Contains the code, so that it could be returned to the client.
 *
 */
public class UnexpectedCodeException extends IllegalArgumentException{

	private static final long serialVersionUID = -2851933051942521951L;
	
	private int code;
	
	public UnexpectedCodeException(String message, int code) {
		super(message);
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
	
}
