/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4
* Class: CSI4321
*
************************************************/
package notifi.serialization;

import java.io.EOFException;
import java.io.IOException;

import notifi.util.IOHelper;
import notifi.util.Validator;

/**
 * NoTiFi abstract message
 * An abstract parent for all notify messages. Contains the basic protocol setup,
 * decoding messages and encoding the message headers.
 */
public abstract class NoTiFiMessage {

    public static final int VERSION = 3;	//the version of notifi protocol
    private static final int HEADER_LENGTH = 2;	// length of the notifi message header in bytes
    public static final String UNEXPECTED_CODE = "unexpected_code";	// code used to handle unexpected messages

    /**
     * Message id
     */
    private int msgId;

    /**
     * Constructs base message with given values
     * 
     * @param msgId message ID
     * @throws java.lang.IllegalArgumentException if bad message ID
     */
    public NoTiFiMessage(int msgId) throws IllegalArgumentException {
        setMsgId(msgId);
        this.msgId = msgId;
    }

    /**
     * Deserializes from byte array
     * 
     * @param pkt byte array to deserialize
     * @return a specific NoTiFi message resulting from deserialization
     * @throws java.lang.IllegalArgumentException if bad version or code
     * @throws java.io.IOException if I/O problem including packet too long/short
     *         (EOFException)
     */
    public static NoTiFiMessage decode(byte[] pkt) throws IllegalArgumentException, IOException {
        // check length
        if (pkt.length < 2) {
            throw new EOFException("too short pkt");
        }

        // check version
        int version = pkt[0] >>> 4; // move to apply just upper 4 bits
        if (version != 3) {
            throw new IllegalArgumentException("bad version");
        }
        // get code
        int code = pkt[0] & 0b0001111; // mask the upper 4 bits
        if (!Validator.validateCode(code)) {
        	throw new UnexpectedCodeException(UNEXPECTED_CODE, code);
        }

        // get message id, convert unsigned byte to signed int
        int msgId = pkt[1] & 0xFF;

        // pick the correct specific type
        NoTiFiMessage message = null;
        switch (code) {
        case 0:
            message = new NoTiFiRegister(msgId);
            break;
        case 1:
            message = new NoTiFiLocationAddition(msgId);
            break;
        case 2:
            message = new NoTiFiLocationDeletion(msgId);
            break;
        case 3:
            message = new NoTiFiDeregister(msgId);
            break;
        case 4:
            message = new NoTiFiError(msgId);
            break;
        case 5:
            message = new NoTiFiACK(msgId);
            if(pkt.length != 2) {
            	throw new EOFException("wrong length of ack");
            }
            break;
        default: throw new UnexpectedCodeException(UNEXPECTED_CODE, code);
        }

        // create specific class
        message.decodeSpecific(pkt);

        return message;
    }

    /**
     * Serializes message
     * 
     * @return serialized message bytes
     */
    public byte[] encode() {
        // prepare header for subclasses
        byte[] header = new byte[HEADER_LENGTH];
        try {
            // version
            header[0] = IOHelper.intTo4bitUI(VERSION, true);
            // code
            header[0] |= IOHelper.intTo4bitUI(getCode(), false);
            // message id
            header[1] = IOHelper.intTo8bitUI(msgId);

            // IOHelper.printByteArrayBinary(header);
            return header;

        } catch (IllegalArgumentException e) {
            // should not ever happen
            // e.printStackTrace();
        }
        return null;
    }

    
    /**
     * Set message ID
     * 
     * @param msgId message ID
     * @throws java.lang.IllegalArgumentException if message ID is out of range
     */
    public void setMsgId(int msgId) throws IllegalArgumentException {
        if (!Validator.check8bitUI(msgId)) {
            throw new IllegalArgumentException("wrong message id");
        }
        this.msgId = msgId;
    }

    /**
     * Set message ID force no validation because testing requires max coverage
     * 
     * @param msgId message ID
     */
    public void setMsgIdForce(int msgId) {
        this.msgId = msgId;
    }

    /**
     * Get message ID
     * 
     * @return message ID
     */
    public int getMsgId() {
        return msgId;
    }

    /**
     * Get operation code
     * 
     * @return operation code
     */
    public abstract int getCode();

    /**
     * Decodes the message-type specific information
     * 
     * @param pkt
     * @throws             java.io.EOFException
     * @throws IOException if the byte array is too short
     */
    protected abstract void decodeSpecific(byte[] pkt) throws EOFException, IOException;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + msgId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NoTiFiMessage other = (NoTiFiMessage) obj;
        if (msgId != other.msgId)
            return false;
        return true;
    }

}
