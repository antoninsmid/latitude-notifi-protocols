/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4
* Class: CSI4321
*
************************************************/
package notifi.serialization;

import java.io.IOException;

/**
 * NoTiFi location addition message
 */
public class NoTiFiLocationAddition extends NoTiFiMessage {

    /**
     * Code for NoTiFi for addition message
     */
    public static final int CODE = 1;

    private NoTiFiLocation location;

    /**
     * Constructs NoTiFi location addition from values
     * 
     * @param msgId    message id
     * @param location location to add
     * @throws java.lang.IllegalArgumentException if message ID invalid
     */
    public NoTiFiLocationAddition(int msgId, NoTiFiLocation location) throws IllegalArgumentException {
        super(msgId);
        setLocation(location);
    }

    /**
     * Simple constructor used during decode
     * 
     * @param msgId
     */
    protected NoTiFiLocationAddition(int msgId) {
        super(msgId);
    }

    /**
     * Get location record
     * 
     * @return location record
     */
    public NoTiFiLocation getLocation() {
        return new NoTiFiLocation(location);
    }

    /**
     * Set location record
     * 
     * @param location new location to add
     * @throws IllegalArgumentException if location is null
     */
    public void setLocation(NoTiFiLocation location) throws IllegalArgumentException {
        if (location == null) {
            throw new IllegalArgumentException("location is null");
        }
        this.location = new NoTiFiLocation(location);
    }

    /**
     * sets location to null must have max coverage, method because of tests
     */
    public void setLocationNull() {
        this.location = null;
    }

    
    @Override
    public int getCode() {
        return CODE;
    }

    @Override
    protected void decodeSpecific(byte[] pkt) throws IllegalArgumentException, IOException {
        setLocation(NoTiFiLocation.decode(pkt));
    }

    @Override
    public byte[] encode() {
        byte[] header = super.encode();
        byte[] locEncoded = location.encode();
        byte[] encoded = new byte[header.length + locEncoded.length];
        System.arraycopy(header, 0, encoded, 0, header.length);
        System.arraycopy(locEncoded, 0, encoded, header.length, locEncoded.length);
        return encoded;
    }

    @Override
    public String toString() {
        return "NoTiFiLocationAddition [location=" + location + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        NoTiFiLocationAddition other = (NoTiFiLocationAddition) obj;
        if (location == null) {
            if (other.location != null)
                return false;
        } else if (!location.equals(other.location))
            return false;
        return true;
    }

}
