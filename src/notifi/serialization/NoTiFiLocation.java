/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4
* Class: CSI4321
*
************************************************/
package notifi.serialization;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;

import latitude.serialization.LocationRecord;
import notifi.util.IOHelper;
import notifi.util.Validator;

/**
 * NoTiFi location
 * Represents location as part of the notifi protocol.
 * Can be constructed from parameters or from latitude location record
 */
public class NoTiFiLocation {

    public static final int MAX_STRING_LENGTH = 255;

    private long userId;
    private double longitude;
    private double latitude;
    private String locationName;
    private String locationDescription;

    
    
    /**
     * Constructs location record with set values
     * @param userId ID for user
     * @param longitude   position of location
     * @param latitude   position of location
     * @param locationName  name of location
     * @param locationDescription description of location
     * @throws IllegalArgumentException if parameter validation fails
     */
    public NoTiFiLocation(long userId, double longitude, double latitude, String locationName,
            String locationDescription) throws IllegalArgumentException {
        setUserId(userId);
        setLongitude(longitude);
        setLatitude(latitude);
        setLocationName(locationName);
        setLocationDescription(locationDescription);
    }

    /**
     * Simple copy constructor
     * @param location original notifi location, used as source of information
     * @throws IllegalArgumentException if values are invalid
     */
    public NoTiFiLocation(NoTiFiLocation location) throws IllegalArgumentException {
        setUserId(location.getUserId());
        setLongitude(location.getLongitude());
        setLatitude(location.getLatitude());
        setLocationName(location.getLocationName());
        setLocationDescription(location.getLocationDescription());
    }
    
    /**
     * Constructor from latitude location record
     * @param record latitude location record to be used as source of information
     */
    public NoTiFiLocation(LocationRecord record) {
		setUserId(record.getUserId());
		setLongitude(Double.parseDouble(record.getLongitude()));
		setLatitude(Double.parseDouble(record.getLatitude()));
		setLocationName(record.getLocationName());
		setLocationDescription(record.getLocationDescription());
	}

    @Override
    public String toString() {
        return "NoTiFiLocation [userId=" + userId + ", longitude=" + longitude + ", latitude=" + latitude
                + ", locationName=" + locationName + ", locationDescription=" + locationDescription + "]";
    }

    /**
     * Returns user ID
     * 
     * @return user ID
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Sets user ID
     * 
     * @param userId user ID
     * @throws IllegalArgumentException if user ID out of range
     */
    public void setUserId(long userId) throws IllegalArgumentException {
        if (!Validator.validateUnsignedInt(userId)) {
            throw new IllegalArgumentException("user id out of bounds");
        }
        this.userId = userId;
    }

    /**
     * Returns longitude
     * 
     * @return longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Sets longitude
     * 
     * @param longitude new longitude
     * @throws IllegalArgumentException if longitude out of range
     */
    public void setLongitude(double longitude) throws IllegalArgumentException {
        if (!Validator.validateLongitude(longitude)) {
            throw new IllegalArgumentException("Longitude out of bounds.");
        }
        this.longitude = longitude;
    }

    /**
     * Returns latitude
     * 
     * @return latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Sets latitude
     * 
     * @param latitude new latitude
     * @throws IllegalArgumentException if latitude out of range
     */
    public void setLatitude(double latitude) throws IllegalArgumentException {
        if (!Validator.validateLatitude(latitude)) {
            // System.out.println(latitude);
            throw new IllegalArgumentException("Latitude out of bounds.");
        }
        this.latitude = latitude;
    }

    /**
     * Returns location name
     * 
     * @return location name
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * Sets location name
     * 
     * @param locationName new location name
     * @throws IllegalArgumentException - if name null or too long
     */
    public void setLocationName(String locationName) throws IllegalArgumentException {
        if (!Validator.validateStringLength(locationName, MAX_STRING_LENGTH)) {
            throw new IllegalArgumentException("wrong location name: null or too long");
        }
        if(!Validator.validateStringChars(locationName)) {
        	throw new IllegalArgumentException("wrong encoding for location name");
        }
        this.locationName = locationName;
    }

    /**
     * Returns location description
     * 
     * @return location description
     */
    public String getLocationDescription() {
        return locationDescription;
    }

    /**
     * Sets location description
     * 
     * @param locationDescription new location description
     * @throws IllegalArgumentException if description null or too long
     */
    public void setLocationDescription(String locationDescription) throws IllegalArgumentException {
        if (!Validator.validateStringLength(locationDescription, MAX_STRING_LENGTH)) {
            throw new IllegalArgumentException("wrong location description: null or too long");
        }
        if(!Validator.validateStringChars(locationDescription)) {
        	throw new IllegalArgumentException("wrong encoding for location description");
        }
        this.locationDescription = locationDescription;
    }

    /**
     * Decodes Location from packet.
     * 
     * @param pkt byte array packet. The full packet (with header).
     * @return NoTiFi Location
     * @throws IOException if IO exception occurs (wrong packet length)
     */
    public static NoTiFiLocation decode(byte[] pkt) throws IOException {

        if (pkt.length < 24) {
            throw new EOFException("the packet holding location is too short");
        }

        // read user id, first 2 bytes is header, user id 4 bytes, therefore read 2 to 6
        // byte.
        long userId = 0;
        for (int i = 2; i < 6; i++) // starts at 3rd packet, ends at 6th
        {
            userId = (userId << 8) + (pkt[i] & 0xff);
        }

        // read longitude, 8 bytes from 6 to 14.
        double longitude = IOHelper.decodeDouble(Arrays.copyOfRange(pkt, 6, 14));

        // read latitude, 8 bytes from 15 to 22
        double latitude = IOHelper.decodeDouble(Arrays.copyOfRange(pkt, 14, 22));

        try {
            // read length of name and name
            int nameLength = pkt[22] & 0xff;
            byte[] nameRaw = Arrays.copyOfRange(pkt, 23, 23 + nameLength);
            String name = new String(nameRaw, IOHelper.CHARENC);

            // read length of description and description
            int descriptionStart = 23 + nameLength;
            int descriptionLength = pkt[descriptionStart] & 0xff;
            byte[] descriptionRaw = Arrays.copyOfRange(pkt, descriptionStart + 1,
                    descriptionStart + 1 + descriptionLength);
            String description = new String(descriptionRaw, IOHelper.CHARENC);

            // check correct length (header + userid + long + lat + name and desc = 24)
            int correctLength = 24 + nameLength + descriptionLength;
            if (pkt.length != correctLength) {
                throw new EOFException("the packet holding location has wrong length");
            }

            return new NoTiFiLocation(userId, longitude, latitude, name, description);

        } catch (Exception e) {
        	//e.printStackTrace();
            throw new EOFException("the packet holding location has wrong length");
        }
        
    }

    /**
     * set description null to force max coverage, need to test nulls
     */
    public void setLocationDescriptionNull() {
        this.locationDescription = null;
    }

    /**
     * set name null to force max coverage, need to test nulls
     */
    public void setLocationNameNull() {
        this.locationName = null;
    }

    /**
     * Serializes location record
     * 
     * @return serialized byte array
     */
    public byte[] encode() {

        // encode user id
        byte[] userIdEncoded = new byte[4];
        long userIdWork = this.userId;
        for (int i = 0; i < userIdEncoded.length; i++) {
            userIdEncoded[userIdEncoded.length - i - 1] = (byte) userIdWork;
            userIdWork = userIdWork >> Byte.SIZE;
        }

        // encode longitude
        byte[] longitudeEncoded = IOHelper.encodeDouble(longitude);

        // encode latitude
        byte[] latitudeEncoded = IOHelper.encodeDouble(latitude);

        // encode the name length and name
        byte[] nameEncoded = IOHelper.encodeString(locationName);

        // encode the description length and name
        byte[] descriptionEncoded = IOHelper.encodeString(locationDescription);

        // connect all together and return
        byte[] locationEncoded = new byte[userIdEncoded.length + longitudeEncoded.length + latitudeEncoded.length
                + nameEncoded.length + descriptionEncoded.length];
        int position = 0;
        System.arraycopy(userIdEncoded, 0, locationEncoded, position, userIdEncoded.length);
        position += userIdEncoded.length;
        System.arraycopy(longitudeEncoded, 0, locationEncoded, position, longitudeEncoded.length);
        position += longitudeEncoded.length;
        System.arraycopy(latitudeEncoded, 0, locationEncoded, position, latitudeEncoded.length);
        position += latitudeEncoded.length;
        System.arraycopy(nameEncoded, 0, locationEncoded, position, nameEncoded.length);
        position += nameEncoded.length;
        System.arraycopy(descriptionEncoded, 0, locationEncoded, position, descriptionEncoded.length);

        return locationEncoded;

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(latitude);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((locationDescription == null) ? 0 : locationDescription.hashCode());
        result = prime * result + ((locationName == null) ? 0 : locationName.hashCode());
        temp = Double.doubleToLongBits(longitude);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + (int) (userId ^ (userId >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NoTiFiLocation other = (NoTiFiLocation) obj;
        if (Double.doubleToLongBits(latitude) != Double.doubleToLongBits(other.latitude))
            return false;
        if (locationDescription == null) {
            if (other.locationDescription != null)
                return false;
        } else if (!locationDescription.equals(other.locationDescription))
            return false;
        if (locationName == null) {
            if (other.locationName != null)
                return false;
        } else if (!locationName.equals(other.locationName))
            return false;
        if (Double.doubleToLongBits(longitude) != Double.doubleToLongBits(other.longitude))
            return false;
        if (userId != other.userId)
            return false;
        return true;
    }

}
