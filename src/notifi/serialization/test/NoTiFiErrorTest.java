/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4 Test
* Class: CSI4321
*
************************************************/
package notifi.serialization.test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import notifi.serialization.NoTiFiError;
import notifi.serialization.NoTiFiMessage;

/**
 * error message test class
 */
class NoTiFiErrorTest {

    // simple encode decode
    @Test
    void encodeDecode() throws IOException {
        
        NoTiFiMessage msg = new NoTiFiError(254, "");
        byte[] encoded = msg.encode();
        NoTiFiMessage msg2 = NoTiFiMessage.decode(encoded);
        
        assertEquals(msg.getClass(), msg2.getClass());
        assertEquals(msg.getMsgId(), msg2.getMsgId());
        assertEquals(msg.getCode(), msg2.getCode());
        assertEquals(((NoTiFiError)msg).getErrorMessage(), ((NoTiFiError)msg2).getErrorMessage());        
        
    }
    
    //setter test invalid
    @Test
    public void setterInvalidTest() {
        NoTiFiError msg = new NoTiFiError(254, "");
        
        assertThrows(IllegalArgumentException.class, ()->{
            msg.setErrorMessage(null);;
        });      
    }
    
    //test to string
    @Test
    public void toStringTest() {
        NoTiFiError msg = new NoTiFiError(254, "ahojError");
        assertEquals("NoTiFiError [errorMessage=ahojError, msgId=254]", msg.toString());
    }
    
    //test hashcode
    @Test
    public void testHashcode() {
        NoTiFiError msg = new NoTiFiError(254, "");
        NoTiFiError msg2 = new NoTiFiError(254, "");
        
        assertEquals(msg.hashCode(), msg2.hashCode());
        
        msg2.setMsgId(22);
        assertNotEquals(msg.hashCode(), msg2.hashCode());
        msg2 = new NoTiFiError(254, "");
        msg2.setErrorMessage("a");
        assertNotEquals(msg.hashCode(), msg2.hashCode());
        
        msg2 = new NoTiFiError(254, "");
        msg.setErrorMessageNull();
        assertEquals(msg.hashCode(), msg2.hashCode());
        
    }
    
    //test equals
    @SuppressWarnings("unlikely-arg-type")
    @Test
    public void testEquals() {
        NoTiFiError msg = new NoTiFiError(254, "");
        NoTiFiError msg2 = new NoTiFiError(254, "");
        
        assertFalse(msg.equals(null));
        assertFalse(msg.equals("ahoj"));
        assertTrue(msg.equals(msg));
        
        assertTrue(msg.equals(msg2));
        msg.setErrorMessageNull();
        assertFalse(msg.equals(msg2));
        msg2.setErrorMessageNull();
        assertTrue(msg.equals(msg2));
        msg = new NoTiFiError(254, "");
        msg2 = new NoTiFiError(254, "a");
        assertFalse(msg.equals(msg2));
        
    }
    
}


































