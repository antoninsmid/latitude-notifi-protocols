/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4 Test
* Class: CSI4321
*
************************************************/
package notifi.serialization.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import notifi.serialization.NoTiFiACK;

/**
 * class with ACK testing (in fact testing abstract message)
 */
class NoTiFiACKTest {

    // msg code getter test
    @Test
    void testGetCode() {
        var msg = new NoTiFiACK(25);
        assertEquals(5, msg.getCode());
    }

    // msg constructor test
    @Test
    void testNoTiFiACKConstructor() {
        var msg = new NoTiFiACK(25);
        assertEquals(5, msg.getCode());
        assertEquals(25, msg.getMsgId());
        assertEquals(NoTiFiACK.class, msg.getClass());
    }

    // to string test
    @Test
    void testToString() {
        var msg = new NoTiFiACK(25);
        assertEquals("NoTiFiACK [msgId=25]", msg.toString());
    }

    // hashcode test
    @Test
    void testHashCode() {
        var msg = new NoTiFiACK(25);
        var msg2 = new NoTiFiACK(25);
        assertEquals(msg.hashCode(), msg2.hashCode());
        msg.setMsgId(22);
        assertNotEquals(msg.hashCode(), msg2.hashCode());

    }

    // equals test
    @SuppressWarnings("unlikely-arg-type")
    @Test
    void testEqualsObject() {
        var msg = new NoTiFiACK(25);
        assertFalse(msg.equals(null));
        assertFalse(msg.equals("ahoj"));
        assertTrue(msg.equals(msg));

        var msg2 = new NoTiFiACK(26);
        assertFalse(msg.equals(msg2));

        msg2.setMsgId(25);
        assertTrue(msg.equals(msg2));
    }

}
