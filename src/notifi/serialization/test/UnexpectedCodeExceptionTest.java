package notifi.serialization.test;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;
import notifi.serialization.UnexpectedCodeException;

/**
 * Class for simple unexpectedcode exception test.
 * @author smid
 *
 */
class UnexpectedCodeExceptionTest {

	// testing constructor and getters
	@Test
	void constructorGetterTest() {
		UnexpectedCodeException e = new UnexpectedCodeException("fail", 25);
		assertEquals(25, e.getCode());
		assertEquals("fail", e.getMessage());
	}

}
