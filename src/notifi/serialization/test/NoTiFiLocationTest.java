/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4 Test
* Class: CSI4321
*
************************************************/
package notifi.serialization.test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import notifi.serialization.NoTiFiLocation;

/**
 * test class for location
 */
class NoTiFiLocationTest {

    // simple test hash method
    @Test
    void testHashCode() {
        var location = new NoTiFiLocation(42, 22, -33, "here", "there");
        var location2 = new NoTiFiLocation(43, 22, -33, "here", "there");
        var location3 = new NoTiFiLocation(42, 22, -33, "here", "there");

        assertEquals(location.hashCode(), location3.hashCode());
        assertNotEquals(location.hashCode(), location2.hashCode());

        location.setLocationNameNull();
        assertNotEquals(location.hashCode(), location3.hashCode());
        location3.setLocationDescriptionNull();
        assertNotEquals(location3.hashCode(), location.hashCode());
    }

    // test simple to string method
    @Test
    void testToString() {
        var location = new NoTiFiLocation(42, 22, -33, "here", "there");
        assertEquals(
                "NoTiFiLocation [userId=42, longitude=22.0, latitude=-33.0, locationName=here, locationDescription=there]",
                location.toString());
    }

    // test setters and getters valid
    @Test
    void testSettersGetters() {
        var location = new NoTiFiLocation(42, 22, -33, "here", "there");
        location.setUserId(4_294_967_295L);
        assertEquals(4_294_967_295L, location.getUserId());
        location.setLongitude(-4);
        assertEquals(-4, location.getLongitude());
        location.setLatitude(20);
        assertEquals(20, location.getLatitude());
        location.setLocationName(generateShortString());
        assertEquals(generateShortString(), location.getLocationName());
        location.setLocationDescription(generateShortString());
        assertEquals(generateShortString(), location.getLocationDescription());
    }

    // test setters and getters invalid
    @Test
    void testSettersGettersInvalid() {
        var location = new NoTiFiLocation(42, 22, -33, "here", "there");
        assertThrows(IllegalArgumentException.class, () -> {
            location.setUserId(-1);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            location.setUserId(4_294_967_296L);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            location.setLongitude(180.01);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            location.setLatitude(-90.1);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            location.setLocationName(null);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            location.setLocationName(generateLongString());
        });

        assertThrows(IllegalArgumentException.class, () -> {
            location.setLocationDescription(null);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            location.setLocationDescription(generateLongString());
        });
    }

    // test encode decode valid
    @Test
    void testEncodeDecode() throws IOException {

        var location = new NoTiFiLocation(4_294_967_295L, 180, 90, generateShortString(), generateShortString());
        byte[] encoded = location.encode();
        byte[] fakeEncode = new byte[encoded.length + 2];
        System.arraycopy(encoded, 0, fakeEncode, 2, encoded.length);

        var decoded = NoTiFiLocation.decode(fakeEncode);

        assertEquals(location.getUserId(), decoded.getUserId());
        assertEquals(location.getLongitude(), decoded.getLongitude());
        assertEquals(location.getLatitude(), decoded.getLatitude());
        assertEquals(location.getLocationName(), decoded.getLocationName());
        assertEquals(location.getLocationDescription(), decoded.getLocationDescription());

    }

    // test encode decode invalid
    @Test
    void testEncodeDecodeInvalid() throws IOException {

        var location = new NoTiFiLocation(4_294_967_295L, 180, 90, generateShortString(), generateShortString());
        byte[] encoded = location.encode();

        // too short
        assertThrows(IOException.class, () -> {
            byte[] fakeEncode = new byte[23];
            System.arraycopy(encoded, 0, fakeEncode, 2, 21);
            NoTiFiLocation.decode(fakeEncode);
        });

        // too long
        assertThrows(IOException.class, () -> {
            byte[] fakeEncode = new byte[encoded.length + 3];
            System.arraycopy(encoded, 0, fakeEncode, 2, encoded.length);
            NoTiFiLocation.decode(fakeEncode);
        });

    }

    // unnecessary equals test
    @SuppressWarnings("unlikely-arg-type")
    @Test
    void testEqualsObject() {
        var location = new NoTiFiLocation(42, 22, -33, "here", "there");
        assertTrue(location.equals(location));
        assertFalse(location.equals(null));
        assertFalse(location.equals("ahoj"));

        var location2 = new NoTiFiLocation(42, 22, -33, "here", "there");
        assertTrue(location.equals(location2));

        location2 = new NoTiFiLocation(42, 21, -33, "here", "there");
        assertFalse(location.equals(location2));
        location2 = new NoTiFiLocation(42, 22, -32, "here", "there");
        assertFalse(location.equals(location2));
        location2 = new NoTiFiLocation(41, 22, -33, "here", "there");
        assertFalse(location.equals(location2));
        location2 = new NoTiFiLocation(42, 21, -33, "her", "there");
        assertFalse(location.equals(location2));
        location2 = new NoTiFiLocation(42, 21, -33, "here", "ther");
        assertFalse(location.equals(location2));

        location2 = new NoTiFiLocation(42, 22, -33, "here", "there");
        location.setLocationDescriptionNull();
        assertFalse(location.equals(location2));

        location = new NoTiFiLocation(42, 22, -33, "here", "there");
        location.setLocationNameNull();
        assertFalse(location.equals(location2));

        location2 = new NoTiFiLocation(42, 22, -33, "here", "there");
        location.setLocationDescriptionNull();
        location2.setLocationDescriptionNull();
        assertFalse(location.equals(location2));

        location = new NoTiFiLocation(42, 22, -33, "here", "there");
        location2 = new NoTiFiLocation(42, 22, -33, "here", "there");
        location.setLocationNameNull();
        location2.setLocationNameNull();
        assertTrue(location.equals(location2));

    }

    // generates string with length > 255
    private String generateLongString() {
        return "Ahoj, jak se mas. Ja se nemam moc dobre." + "Musim psat spoustu testu. Ale nemyslim testu ve skole,"
                + "myslim tim unit testu. Doufam, ze to brzy skonci. Uz se tesim,"
                + "az se budu moct venovat jenom projektu a vsechny hloupe predmety"
                + "budou minulosti. Vanoce se blizi. :)";
    }

    // generates string with length = 255
    private String generateShortString() {
        return "Ahoj, jak se mas. Ja se nemam moc dobre." + "Musim psat spoustu testu. Ale nemyslim testu ve skole,"
                + "myslim tim unit testu. Doufam, ze to brzy skonci. Uz se tesim,"
                + "az se budu moct venovat jenom projektu a vsechny hloupe predmety"
                + "budou minulosti. Vanoce se blizi.:)";
    }

}
