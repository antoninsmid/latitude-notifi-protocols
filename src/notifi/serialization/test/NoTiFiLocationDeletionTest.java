/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4 Test
* Class: CSI4321
*
************************************************/
package notifi.serialization.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import notifi.serialization.NoTiFiLocation;
import notifi.serialization.NoTiFiLocationDeletion;
import notifi.serialization.NoTiFiMessage;

/**
 * class testing the NoTiFi location deletion
 */
class NoTiFiLocationDeletionTest {

    // encode decode test
    @Test
    @DisplayName("test encode decode")
    public void locationDeletionTest() throws IOException {
        int id = 4242;
        double lon = 25.582340957;
        double lat = -86.23450987;
        String name = "here";
        String desc = "there";
        NoTiFiLocation location = new NoTiFiLocation(id, lon, lat, name, desc);
        NoTiFiLocationDeletion ld = new NoTiFiLocationDeletion(42, location);

        assertEquals(2, ld.getCode());
        assertEquals(id, ld.getLocation().getUserId());
        assertEquals(lon, ld.getLocation().getLongitude());
        assertEquals(lat, ld.getLocation().getLatitude());
        assertEquals(name, ld.getLocation().getLocationName());
        assertEquals(desc, ld.getLocation().getLocationDescription());

        byte[] encoded = ld.encode();

        NoTiFiMessage decoded = NoTiFiMessage.decode(encoded);

        assertEquals(ld.getClass(), decoded.getClass());
        assertEquals(ld.getMsgId(), decoded.getMsgId());
        assertEquals(ld.getCode(), decoded.getCode());

        assertEquals(id, ((NoTiFiLocationDeletion) decoded).getLocation().getUserId());
        assertEquals(lon, ((NoTiFiLocationDeletion) decoded).getLocation().getLongitude());
        assertEquals(lat, ((NoTiFiLocationDeletion) decoded).getLocation().getLatitude());
        assertEquals(name, ((NoTiFiLocationDeletion) decoded).getLocation().getLocationName());
        assertEquals(desc, ((NoTiFiLocationDeletion) decoded).getLocation().getLocationDescription());

    }

    // setter test
    @Test
    public void locationAdditionSettersTest() {

        assertThrows(IllegalArgumentException.class, () -> {
            new NoTiFiLocationDeletion(42, null);

        });

    }

    // to string test
    @Test
    void testToString() {
        int id = 4242;
        double lon = 25.582340957;
        double lat = -86.23450987;
        String name = "here";
        String desc = "there";
        NoTiFiLocation location = new NoTiFiLocation(id, lon, lat, name, desc);
        NoTiFiLocationDeletion la = new NoTiFiLocationDeletion(42, location);
        assertEquals(
                "NoTiFiLocationDeletion [location=NoTiFiLocation [userId=4242, longitude=25.582340957, latitude=-86.23450987, locationName=here, locationDescription=there]]",
                la.toString());
    }

    // hashcode tst
    @Test
    void testHash() {

        NoTiFiLocation location = new NoTiFiLocation(42, 42, 42, "42", "42");
        NoTiFiLocationDeletion la = new NoTiFiLocationDeletion(42, location);
        NoTiFiLocationDeletion la2 = new NoTiFiLocationDeletion(42, location);

        assertEquals(la.hashCode(), la2.hashCode());

        la2.setMsgId(22);
        assertNotEquals(la.hashCode(), la2.hashCode());

        la2 = new NoTiFiLocationDeletion(42, location);
        la.setLocationNull();
        assertNotEquals(la.hashCode(), la2.hashCode());
    }

    // test location addition equals
    @SuppressWarnings("unlikely-arg-type")
    @Test
    public void testEquals() {
        NoTiFiLocation location = new NoTiFiLocation(42, 42, 42, "42", "42");
        NoTiFiLocation location2 = new NoTiFiLocation(41, 42, 42, "42", "42");
        NoTiFiLocationDeletion la = new NoTiFiLocationDeletion(42, location);
        NoTiFiLocationDeletion la2 = new NoTiFiLocationDeletion(42, location);

        assertTrue(la.equals(la));
        assertFalse(la.equals(null));
        assertFalse(la.equals("ahoj"));

        assertTrue(la.equals(la2));
        la2.setLocationNull();
        assertFalse(la2.equals(la));
        la.setLocationNull();
        assertTrue(la2.equals(la));

        la = new NoTiFiLocationDeletion(42, location);
        la2 = new NoTiFiLocationDeletion(42, location2);

        assertFalse(la.equals(la2));
    }

}
