/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4 Test
* Class: CSI4321
*
************************************************/
package notifi.serialization.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.EOFException;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import notifi.serialization.NoTiFiACK;
import notifi.serialization.NoTiFiError;
import notifi.serialization.NoTiFiMessage;

class NoTiFiMessageTest {

    // simple encode test
    @Test
    void encodeTest() throws IOException {

        NoTiFiMessage message = new NoTiFiACK(254);
        message.encode();

    }

    // simple decode test valid
    @Test
    void decodeTest() throws IOException {

        NoTiFiMessage message = new NoTiFiACK(254);
        byte[] inout = message.encode();

        NoTiFiMessage message2 = NoTiFiMessage.decode(inout);

        assertEquals(message.getClass(), message2.getClass());
        assertEquals(message.getMsgId(), message2.getMsgId());
        assertEquals(message.getCode(), message2.getCode());
        
        byte[] inoutLong = new byte[536];
        System.arraycopy(inout, 0, inoutLong, 0, inout.length);
        
//        NoTiFiMessage message3 = NoTiFiMessage.decode(inoutLong);
//
//        assertEquals(message.getClass(), message3.getClass());
//        assertEquals(message.getMsgId(), message.getMsgId());
//        assertEquals(message.getCode(), message3.getCode());
        
        message = new NoTiFiError(254, "test");
        inout = message.encode();

        message2 = NoTiFiMessage.decode(inout);

        assertEquals(message.getClass(), message2.getClass());
        assertEquals(message.getMsgId(), message2.getMsgId());
        assertEquals(message.getCode(), message2.getCode());
        
    }
    
    // fixing donatest bugs
    @Test
    void decodeTest2() throws IOException {
    	byte[] in = new byte[3];
    	in[0] = 53;
    	in[2] = 4;
    	
    	//IOHelper.printByteArrayBinary(in);
    	
        NoTiFiMessage message = new NoTiFiACK(254);
        message.encode();
        //IOHelper.printByteArrayBinary(inout);
        
    }

    // simple decode test invalid
    @Test
    void decodeTestInvalid() throws IOException {

        assertThrows(EOFException.class, () -> {
            NoTiFiMessage.decode(new byte[1]);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            byte[] in = { 64, 0 };
            NoTiFiMessage.decode(in);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            byte[] in = { 63, 0 };
            NoTiFiMessage.decode(in);
        });

    }

    // simple encode test invalid
    @Test
    void encodeTestInvalid() throws IOException {

        NoTiFiMessage message = new NoTiFiACK(254);
        message.setMsgIdForce(1500);

        assertNull(message.encode());
    }

    // simple setter test invalid
    @Test
    void settterTestInvalid() throws IOException {

        NoTiFiMessage message = new NoTiFiACK(254);
        assertThrows(IllegalArgumentException.class, () -> {
            message.setMsgId(1500);
        });
    }

    // example test from canvas
    @Test
    void exampleTest() throws IllegalArgumentException, IOException {
        var in = new byte[] { 0x34, 0x5, 'Y', 'u', 'r', 'p' };
        var msg = NoTiFiMessage.decode(in);
        assertEquals(5, msg.getMsgId());
        assertEquals(4, msg.getCode());
        var newMsg = (NoTiFiError) msg;
        assertEquals("Yurp", newMsg.getErrorMessage());
    }

}
