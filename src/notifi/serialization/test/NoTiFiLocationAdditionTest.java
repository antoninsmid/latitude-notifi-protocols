/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4 Test
* Class: CSI4321
*
************************************************/
package notifi.serialization.test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import notifi.serialization.NoTiFiLocation;
import notifi.serialization.NoTiFiLocationAddition;
import notifi.serialization.NoTiFiMessage;

/**
 * clas for location addition testing
 */
class NoTiFiLocationAdditionTest {

    // encode decode test
    @Test
    @DisplayName("test encode decode")
    public void locationAdditionTest() throws IOException {
        int id = 4242;
        double lon = 25.582340957;
        double lat = -86.23450987;
        String name = "here";
        String desc = "there";
        NoTiFiLocation location = new NoTiFiLocation(id, lon, lat, name, desc);
        NoTiFiLocationAddition la = new NoTiFiLocationAddition(42, location);

        assertEquals(1, la.getCode());
        assertEquals(id, la.getLocation().getUserId());
        assertEquals(lon, la.getLocation().getLongitude());
        assertEquals(lat, la.getLocation().getLatitude());
        assertEquals(name, la.getLocation().getLocationName());
        assertEquals(desc, la.getLocation().getLocationDescription());

        byte[] encoded = la.encode();

        NoTiFiMessage decoded = NoTiFiMessage.decode(encoded);

        assertEquals(la.getClass(), decoded.getClass());
        assertEquals(la.getMsgId(), decoded.getMsgId());
        assertEquals(la.getCode(), decoded.getCode());

        assertEquals(id, ((NoTiFiLocationAddition) decoded).getLocation().getUserId());
        assertEquals(lon, ((NoTiFiLocationAddition) decoded).getLocation().getLongitude());
        assertEquals(lat, ((NoTiFiLocationAddition) decoded).getLocation().getLatitude());
        assertEquals(name, ((NoTiFiLocationAddition) decoded).getLocation().getLocationName());
        assertEquals(desc, ((NoTiFiLocationAddition) decoded).getLocation().getLocationDescription());

    }

    // setter test
    @Test
    public void locationAdditionSettersTest() {

        assertThrows(IllegalArgumentException.class, () -> {
            new NoTiFiLocationAddition(42, null);

        });

    }

    // to string test
    @Test
    void testToString() {
        int id = 4242;
        double lon = 25.582340957;
        double lat = -86.23450987;
        String name = "here";
        String desc = "there";
        NoTiFiLocation location = new NoTiFiLocation(id, lon, lat, name, desc);
        NoTiFiLocationAddition la = new NoTiFiLocationAddition(42, location);
        assertEquals(
                "NoTiFiLocationAddition [location=NoTiFiLocation [userId=4242, longitude=25.582340957, latitude=-86.23450987, locationName=here, locationDescription=there]]",
                la.toString());
    }

    // hashcode tst
    @Test
    void testHash() {

        NoTiFiLocation location = new NoTiFiLocation(42, 42, 42, "42", "42");
        NoTiFiLocationAddition la = new NoTiFiLocationAddition(42, location);
        NoTiFiLocationAddition la2 = new NoTiFiLocationAddition(42, location);

        assertEquals(la.hashCode(), la2.hashCode());

        la2.setMsgId(22);
        assertNotEquals(la.hashCode(), la2.hashCode());

        la2 = new NoTiFiLocationAddition(42, location);
        la.setLocationNull();
        assertNotEquals(la.hashCode(), la2.hashCode());
    }

    // test location addition equals
    @SuppressWarnings("unlikely-arg-type")
    @Test
    public void testEquals() {
        NoTiFiLocation location = new NoTiFiLocation(42, 42, 42, "42", "42");
        NoTiFiLocation location2 = new NoTiFiLocation(41, 42, 42, "42", "42");
        NoTiFiLocationAddition la = new NoTiFiLocationAddition(42, location);
        NoTiFiLocationAddition la2 = new NoTiFiLocationAddition(42, location);

        assertTrue(la.equals(la));
        assertFalse(la.equals(null));
        assertFalse(la.equals("ahoj"));

        assertTrue(la.equals(la2));
        la2.setLocationNull();
        assertFalse(la2.equals(la));
        la.setLocationNull();
        assertTrue(la2.equals(la));

        la = new NoTiFiLocationAddition(42, location);
        la2 = new NoTiFiLocationAddition(42, location2);

        assertFalse(la.equals(la2));
    }
}
