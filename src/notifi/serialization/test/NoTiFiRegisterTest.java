/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4 Test
* Class: CSI4321
*
************************************************/
package notifi.serialization.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.UnknownHostException;

import org.junit.jupiter.api.Test;

import notifi.serialization.NoTiFiMessage;
import notifi.serialization.NoTiFiRegister;

/**
 * class testing register messages
 */
class NoTiFiRegisterTest {

    // simple encode decode test
    @Test
    void encodeDecode() throws IOException {
        byte[] addrBytes = new byte[] { (byte) 192, 0, (byte) 168, (byte) 253 };
        Inet4Address addr = (Inet4Address) Inet4Address.getByAddress(addrBytes);
        NoTiFiMessage msg = new NoTiFiRegister(254, addr, 64);

        byte[] encoded = msg.encode();

        // IOHelper.printByteArrayBinary(encoded);

        NoTiFiMessage msg2 = NoTiFiMessage.decode(encoded);

        assertEquals(msg.getClass(), msg2.getClass());
        assertEquals(msg.getMsgId(), msg2.getMsgId());
        assertEquals(msg.getCode(), msg2.getCode());
        assertEquals(((NoTiFiRegister) msg).getAddress(), ((NoTiFiRegister) msg2).getAddress());
        assertEquals(((NoTiFiRegister) msg).getPort(), ((NoTiFiRegister) msg2).getPort());
    }

    // setter test invalid
    @Test
    public void setterTestInvalid() throws UnknownHostException {
        byte[] addrBytes = new byte[] { (byte) 192, 0, (byte) 168, (byte) 253 };
        Inet4Address addr = (Inet4Address) Inet4Address.getByAddress(addrBytes);
        var msg = new NoTiFiRegister(25, addr, 23564);

        assertThrows(IllegalArgumentException.class, () -> {
            msg.setAddress(null);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            msg.setPort(-1);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            msg.setPort(65536);
        });

        assertThrows(IOException.class, () -> {
            byte[] encoded = msg.encode();
            byte[] fake = new byte[encoded.length + 2];
            System.arraycopy(encoded, 0, fake, 0, encoded.length);
            NoTiFiMessage.decode(fake);
        });
    }

    // to string test
    @Test
    public void toStringTest() throws UnknownHostException {
        byte[] addrBytes = new byte[] { (byte) 192, 0, (byte) 168, (byte) 253 };
        Inet4Address addr = (Inet4Address) Inet4Address.getByAddress(addrBytes);
        var msg = new NoTiFiRegister(25, addr, 23564);
        assertEquals("NoTiFiRegister [msgId:25, address=/192.0.168.253, port=23564]", msg.toString());
    }

    // hash function test
    @Test
    public void testHashFunction() throws UnknownHostException {
        byte[] addrBytes = new byte[] { (byte) 192, 0, (byte) 168, (byte) 253 };
        Inet4Address addr = (Inet4Address) Inet4Address.getByAddress(addrBytes);
        var msg = new NoTiFiRegister(25, addr, 23564);
        var msg2 = new NoTiFiRegister(25, addr, 23564);

        assertEquals(msg.hashCode(), msg2.hashCode());

        msg2.setPort(2);
        assertNotEquals(msg.hashCode(), msg2.hashCode());

        msg2 = new NoTiFiRegister(25, addr, 23564);

        msg.setAddressNull();
        assertNotEquals(msg.hashCode(), msg2.hashCode());
        msg2.setAddressNull();
        assertEquals(msg.hashCode(), msg2.hashCode());

    }

    // test equals
    @SuppressWarnings("unlikely-arg-type")
    @Test
    public void testEquals() throws UnknownHostException {
        byte[] addrBytes = new byte[] { (byte) 192, 0, (byte) 168, (byte) 253 };
        Inet4Address addr = (Inet4Address) Inet4Address.getByAddress(addrBytes);
        byte[] addrBytes2 = new byte[] { (byte) 191, 0, (byte) 168, (byte) 253 };
        Inet4Address addr2 = (Inet4Address) Inet4Address.getByAddress(addrBytes2);
        var msg = new NoTiFiRegister(25, addr, 23564);

        assertTrue(msg.equals(msg));
        assertFalse(msg.equals(""));
        assertFalse(msg.equals(null));

        var msg2 = new NoTiFiRegister(25, addr, 23564);

        assertTrue(msg.equals(msg2));

        msg.setAddressNull();
        assertFalse(msg.equals(msg2));

        msg2.setAddressNull();
        assertTrue(msg.equals(msg2));

        msg = new NoTiFiRegister(25, addr, 23564);
        msg2 = new NoTiFiRegister(25, addr, 23562);
        assertFalse(msg.equals(msg2));

        msg = new NoTiFiRegister(25, addr, 23564);
        msg2 = new NoTiFiRegister(25, addr2, 23564);
        assertFalse(msg.equals(msg2));

    }

}
