/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4
* Class: CSI4321
*
************************************************/
package notifi.serialization;


/**
 * NoTiFi ACK message
 * ACK messages are used to confirm registrations/deregistrations
 * 
 */
public class NoTiFiACK extends NoTiFiMessage {

    /**
     * Code for NoTiFi ACK
     */
    public static final int CODE = 5;

    /**
     * Constructs NoTiFi ACK from values
     * 
     * @param msgId message ID
     * @throws IllegalArgumentException if message ID invalid
     */
    public NoTiFiACK(int msgId) throws IllegalArgumentException {
        super(msgId);
    }

    @Override
    public int getCode() {
        return CODE;
    }

    @Override
    protected void decodeSpecific(byte[] pkt) {
    }

    @Override
    public String toString() {
        return "NoTiFiACK [msgId=" + getMsgId() + "]";
    }

}
