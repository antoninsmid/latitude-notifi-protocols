/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4
* Class: CSI4321
*
************************************************/
package notifi.serialization;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import notifi.util.IOHelper;
import notifi.util.Validator;

/**
 * NoTiFi Error message
 * represents notifi error message, apart from id contains textual description of the failure
 */
public class NoTiFiError extends NoTiFiMessage {

    /**
     * Code for NoTiFi error
     */
    public static final int CODE = 4;

    private String errorMessage;

    /**
     * Constructs NoTiFi error from values
     * 
     * @param msgId        message ID
     * @param errorMessage error message
     * @throws IllegalArgumentException if values invalid
     */
    public NoTiFiError(int msgId, String errorMessage) throws IllegalArgumentException {
        super(msgId);
        setErrorMessage(errorMessage);
    }

    /**
     * Simple constructor used during decode
     * 
     * @param msgId
     */
    protected NoTiFiError(int msgId) {
        super(msgId);
    }

    /**
     * Set error message
     * 
     * @param errorMessage new error message
     * @throws IllegalArgumentException if error message is null
     */
    public void setErrorMessage(String errorMessage) throws IllegalArgumentException {
        if (errorMessage == null) {
            throw new IllegalArgumentException("error message is null");
        }
        if(!Validator.validateStringChars(errorMessage)) {
        	throw new IllegalArgumentException("error message is invalid");
        }
        this.errorMessage = errorMessage;
    }

    /**
     * sets message null because of max coverage requirement
     */
    public void setErrorMessageNull() {
        this.errorMessage = null;
    }

    /**
     * Get error message
     * 
     * @return error message
     */
    public String getErrorMessage() {
        return this.errorMessage;
    }

    @Override
    public int getCode() {
        return CODE;
    }

    @Override
    protected void decodeSpecific(byte[] pkt) throws IOException {
        // no error message
        if (pkt.length == 2) {
            setErrorMessage("");
        }

        // load error message
        try {
            setErrorMessage(new String(Arrays.copyOfRange(pkt, 2, pkt.length), IOHelper.CHARENC));
        } catch (Exception e) {
            // should not ever happen
            // throw new IOException();
        }
    }

    @Override
    public byte[] encode() {
        byte[] header = super.encode();
        byte[] message;
        try {
            message = errorMessage.getBytes(IOHelper.CHARENC);
            byte[] encoded = new byte[message.length + header.length];
            System.arraycopy(header, 0, encoded, 0, header.length);
            System.arraycopy(message, 0, encoded, header.length, message.length);
            return encoded;
        } catch (UnsupportedEncodingException e) {
            // should not ever happen
            // e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "NoTiFiError [errorMessage=" + errorMessage + ", msgId=" + getMsgId() + "]";
    }

    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((errorMessage == null) ? 0 : errorMessage.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        NoTiFiError other = (NoTiFiError) obj;
        if (errorMessage == null) {
            if (other.errorMessage != null)
                return false;
        } else if (!errorMessage.equals(other.errorMessage))
            return false;
        return true;
    }

}
