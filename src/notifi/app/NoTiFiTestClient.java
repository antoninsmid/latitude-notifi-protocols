package notifi.app;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class NoTiFiTestClient {
    private static final int MAXDG = 65507;
    private static final int LOCALPORT = 8000;

    public static void main(final String[] args) throws UnknownHostException, IOException {
        if (args.length != 2) { // Test for correct # of args
            throw new IllegalArgumentException("Parameter(s): <server> <server port>");
        }

        String server = args[0]; // Server name or IP address
        int servPort = Integer.parseInt(args[1]);

        try (DatagramSocket socket = new DatagramSocket(LOCALPORT)) {
            socket.connect(InetAddress.getByName(server), servPort);
            // Register 129.62.148.17:8000
            byte[] sndBuf = { 0x30, 0x06, 0x11, (byte) 0x94, 0x3E, (byte) 0x81, 0x40, 0x1f };
            DatagramPacket sndPkt = new DatagramPacket(sndBuf, sndBuf.length);
            socket.send(sndPkt);
            DatagramPacket rcvPkt = new DatagramPacket(new byte[MAXDG], MAXDG);
            while (true) {
                socket.receive(rcvPkt);
                for (int i = 0; i < rcvPkt.getLength(); i++) {
                    System.out.printf("%02x ", rcvPkt.getData()[i]);
                }
                System.out.printf("(%d bytes)", rcvPkt.getLength());
                System.out.println();
                rcvPkt.setLength(MAXDG);
            }
        }
    }
}
