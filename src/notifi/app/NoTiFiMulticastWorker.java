/************************************************
*
* Author: Antonin Smid
* Assignment: Program 8
* Class: CSI4321
*
************************************************/

package notifi.app;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;

/**
 * Multicast worker class envelops the infinite loop of waiting for messages and
 * printing them with the process method
 *
 */
public class NoTiFiMulticastWorker implements Runnable {

	private MulticastSocket socket; // multicast socket already configured from thhe client
	private byte[] bytesToReceive; // space to save the incoming messges
	private DatagramPacket receivePacket; // receive UDP packet with the messages

	/**
	 * Simple constructor
	 * 
	 * @param socket multicast socket to listen on
	 */
	public NoTiFiMulticastWorker(MulticastSocket socket) {
		this.socket = socket;
		bytesToReceive = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];
		receivePacket = new DatagramPacket(bytesToReceive, NoTiFiMulticastClient.MAX_DATAGRAM_LENGTH);
	}

	@Override
	public void run() {

		// infinite loop, running the receive and printing result until user writes
		// 'quit'
		while (!NoTiFiMulticastClient.shutdown) {
			try {
				// receive updates
				socket.receive(receivePacket);

				// process update
				NoTiFiClientProcessor.processData(0, false, receivePacket);

			} catch (SocketTimeoutException ste) {
			} catch (IOException e) {
				break;
			}
		}

	}
}
