/************************************************
*
* Author: Antonin Smid
* Assignment: Program 6
* Class: CSI4321
*
************************************************/

package notifi.app;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import latitude.serialization.LocationRecord;
import notifi.serialization.NoTiFiACK;
import notifi.serialization.NoTiFiDeregister;
import notifi.serialization.NoTiFiError;
import notifi.serialization.NoTiFiLocation;
import notifi.serialization.NoTiFiLocationAddition;
import notifi.serialization.NoTiFiLocationDeletion;
import notifi.serialization.NoTiFiMessage;
import notifi.serialization.NoTiFiRegister;
import notifi.serialization.UnexpectedCodeException;
import notifi.util.IOHelper;

/**
 * NoTiFi server running as a thread under latitude server
 *
 */
public class NoTiFiServer implements Runnable {

	private DatagramSocket socket;
	private int port;
	private DatagramPacket receivePacket;
	private byte[] bytesToReceive;
	private byte[] bytesToSend;
	// set of active subscribers we want to send updates to
	public Set<AddressPort> subscribers;
	// when inet4address to string, then it contains / at beginning, therefore can
	// not be used to create inet4address again based on that name, this constant
	// marks the beginning of string that can be parsed back to address
	private static final int ADDRESS_TO_STR_GET_BEGINNING = 1;
	Logger logger;

	/**
	 * NoTiFi server constructor init logger, sockets, subscriber set and prepare
	 * receive and send packets
	 * 
	 * @param port   server port
	 * @param logger to use for logging packets
	 * @throws IOException if can not create socket
	 */
	public NoTiFiServer(int port, Logger logger) throws IOException {
		
		this.port = port;

		// create socket
		try {
			socket = new DatagramSocket(this.port);
		} catch (SocketException e) {
			System.err.println("Could not init the NoTiFi server on port " + port);
			System.exit(1);
		}

		// logger to file
		this.logger = logger;

		// socket settings
		socket.setSoTimeout(NoTiFiClient.CONNECTION_TIMEOUT);
		socket.setReuseAddress(true);

		// make packets
		bytesToReceive = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];
		receivePacket = new DatagramPacket(bytesToReceive, NoTiFiClient.MAX_DATAGRAM_LENGTH);
		bytesToSend = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];

		// init subscribers
		subscribers = new HashSet<>();
	}

	/**
	 * Sends location addition update to all subscribers
	 * 
	 * @param location the location record to publish
	 */
	public void publishAdd(LocationRecord location) {
		subscribers.stream().forEach((subscriber) -> {
			try {
				sendMessage(new NoTiFiLocationAddition(IOHelper.getRandomMessageId(), new NoTiFiLocation(location)),
						(Inet4Address) InetAddress
								.getByName(subscriber.addressName.substring(ADDRESS_TO_STR_GET_BEGINNING)),
						subscriber.port);
				System.out.println("message sent to " + subscriber.addressName.substring(ADDRESS_TO_STR_GET_BEGINNING));
			} catch (IllegalArgumentException | UnknownHostException e) {
				// should not happen, for the purpose of the assignment do not print stacktrace
				e.printStackTrace();
			}
		});
	}

	/**
	 * @param location the location record to publish
	 */
	public void publishDelete(LocationRecord location) {
		subscribers.stream().forEach((subscriber) -> {
			try {
				sendMessage(new NoTiFiLocationDeletion(IOHelper.getRandomMessageId(), new NoTiFiLocation(location)),
						(Inet4Address) InetAddress
								.getByName(subscriber.addressName.substring(ADDRESS_TO_STR_GET_BEGINNING)),
						subscriber.port);
			} catch (IllegalArgumentException | UnknownHostException e) {
				// should not happen, for the purpose of the assignment do not print stacktrace
				e.printStackTrace();
			}
		});
	}

	/**
	 * The infinite loop waiting for subscribers to be added or deleted
	 */
	@Override
	public void run() {

		while (true) {
			try {
				//System.out.println("notifi server accept");
				socket.receive(receivePacket);
				processMessage(receivePacket);

			} catch (SocketTimeoutException ste) {
			} catch (IOException e) {
				// e.printStackTrace();
				break;
			}
		}
	}

	/**
	 * Decodes the message and decides the following actions
	 * 
	 * @param packet datagram packet with the received message
	 */
	private void processMessage(DatagramPacket packet) {

		// crop the byte array length to the actually received bytes
		int length = packet.getLength();
		byte[] data = new byte[length];
		System.arraycopy(packet.getData(), 0, data, 0, length);

		try {
			NoTiFiMessage message = NoTiFiMessage.decode(data);

			if (message instanceof NoTiFiRegister) {
				handleRegister(packet, (NoTiFiRegister) message);
				return;
			}

			if (message instanceof NoTiFiDeregister) {
				handleDeregister(packet, (NoTiFiDeregister) message);
				return;
			}
			unexpectedMessage(packet, message);
		} catch (UnexpectedCodeException uce) {
			unexpectedCode(packet, uce.getCode());
			return;
		} catch (IllegalArgumentException | IOException e) {
			problemParsingMessage(packet);
		}
	}

	/**
	 * The deregister message received
	 * 
	 * @param packet  datagram packet with the deregister message
	 * @param message deserialized deregister message itself
	 */
	private void handleDeregister(DatagramPacket packet, NoTiFiDeregister message) {
		logger.info("Deregister " + packet.getAddress() + ":" + packet.getPort());
		AddressPort toRemove = new AddressPort(message.getAddress().toString(), message.getPort());
		// there is no record of client to deregister
		if (!subscribers.contains(toRemove)) {
			notInTheList(packet, message);
			return;
		}
		// everything is fine, remove the client from subscribers
		subscribers.remove(toRemove);
		sendMessage(new NoTiFiACK(message.getMsgId()), packet);
	}

	/**
	 * The register message received
	 * 
	 * @param packet  datagram packet witht he register message
	 * @param message the deserialized register message itself
	 */
	private void handleRegister(DatagramPacket packet, NoTiFiRegister message) {
		logger.info("Register " + packet.getAddress() + ":" + packet.getPort());
		// port in message does not correspond to the socket port
		if (packet.getPort() != message.getPort()) {
			incorrectPort(packet, message);
			return;
		}
		// wrong address, it is multicast
		if (message.getAddress().isMulticastAddress()) {
			badAddress(packet, message);
			return;
		}
		AddressPort newSubscriber = new AddressPort(message.getAddress().toString(), message.getPort());
		// client already registered
		if (subscribers.contains(newSubscriber)) {
			alreadyRegistered(packet, message);
			return;
		}
		// everything is fine, register new client
		subscribers.add(newSubscriber);
		sendMessage(new NoTiFiACK(message.getMsgId()), packet);
	}

	/**
	 * handle client not in list error
	 * 
	 * @param packet  datagram packet with the message
	 * @param message deserialized message
	 */
	private void notInTheList(DatagramPacket packet, NoTiFiMessage message) {
		sendMessage(new NoTiFiError(message.getMsgId(), "Unknown client"), packet);
	}

	/**
	 * handle client is already in the list error
	 * 
	 * @param packet  datagram packet with the message
	 * @param message deserialized message
	 */
	private void alreadyRegistered(DatagramPacket packet, NoTiFiMessage message) {
		sendMessage(new NoTiFiError(message.getMsgId(), "Already registered"), packet);
	}

	/**
	 * handle bad address to register received
	 * 
	 * @param packet  datagram packet with the message
	 * @param message deserialized message
	 */
	private void badAddress(DatagramPacket packet, NoTiFiMessage message) {
		sendMessage(new NoTiFiError(message.getMsgId(), "Bad address"), packet);
	}

	/**
	 * handle port validation failed
	 * 
	 * @param packet  datagram packet with the message
	 * @param message deserialized message
	 */
	private void incorrectPort(DatagramPacket packet, NoTiFiMessage message) {
		sendMessage(new NoTiFiError(message.getMsgId(), "Incorrect Port"), packet);
	}

	/**
	 * handle could not parse the message
	 * 
	 * @param packet datagram packet with the wrong message
	 */
	private void problemParsingMessage(DatagramPacket packet) {
		sendMessage(new NoTiFiError(0, "Unable to parse message"), packet);
	}

	/**
	 * handle message with unknown notifi code
	 * 
	 * @param code the message code of the wrong message
	 */
	private void unexpectedCode(DatagramPacket packet, int code) {
		sendMessage(new NoTiFiError(0, "Unknown code: " + code), packet);
	}

	/**
	 * handle unexpected message received
	 * 
	 * @param packet  datagram packet with the message
	 * @param message deserialized message
	 */
	private void unexpectedMessage(DatagramPacket packet, NoTiFiMessage message) {
		if (message instanceof NoTiFiLocationAddition) {
			logger.info("Addition " + packet.getAddress() + ":" + packet.getPort());
		}
		if (message instanceof NoTiFiLocationDeletion) {
			logger.info("Delete " + packet.getAddress() + ":" + packet.getPort());
		}
		if (message instanceof NoTiFiError) {
			logger.info("Error " + packet.getAddress() + ":" + packet.getPort());
		}
		if (message instanceof NoTiFiACK) {
			logger.info("ACK " + packet.getAddress() + ":" + packet.getPort());
		}
		sendMessage(new NoTiFiError(message.getMsgId(), "Unexpected message type: " + message.getCode()), packet);
	}

	/**
	 * sends notifi message
	 * 
	 * @param message to be sent
	 * @param packet  to get the address and port
	 */
	private void sendMessage(NoTiFiMessage message, DatagramPacket packet) {
		sendMessage(message, packet.getAddress(), packet.getPort());
	}

	/**
	 * sends notifi message
	 * 
	 * @param message     to be sent
	 * @param inetAddress destination address
	 * @param port        destination port
	 */
	private void sendMessage(NoTiFiMessage message, InetAddress inetAddress, int port) {
		bytesToSend = message.encode();
		DatagramPacket sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, inetAddress, port);
		try {
			socket.send(sendPacket);
		} catch (IOException e) {
			System.err.println("Could not send message.");
		}
	}

	/**
	 * Class representing subscribed client.
	 *
	 */
	class AddressPort {

		private String addressName;
		private int port;

		/**
		 * construct subscribed client
		 * 
		 * @param address
		 * @param port
		 */
		public AddressPort(String addressName, int port) {
			super();
			this.addressName = addressName;
			this.port = port;
		}

		/**
		 * get client's address
		 * 
		 * @return address
		 */
		public String getAddressName() {
			return addressName;
		}

		/**
		 * get client's port
		 * 
		 * @return port
		 */
		public int getPort() {
			return port;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((addressName == null) ? 0 : addressName.hashCode());
			result = prime * result + port;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			AddressPort other = (AddressPort) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (addressName == null) {
				if (other.addressName != null)
					return false;
			} else if (!addressName.equals(other.addressName))
				return false;
			if (port != other.port)
				return false;
			return true;
		}

		/**
		 * get the notifi server as outer type
		 * 
		 * @return outer type
		 */
		private NoTiFiServer getOuterType() {
			return NoTiFiServer.this;
		}

	}

}
