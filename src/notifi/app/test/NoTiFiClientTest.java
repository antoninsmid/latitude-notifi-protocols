/************************************************
*
* Author: Antonin Smid
* Assignment: Program 5
* Class: CSI4321
*
************************************************/

package notifi.app.test;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;

import notifi.app.NoTiFiClient;
import notifi.serialization.NoTiFiACK;
import notifi.serialization.NoTiFiDeregister;
import notifi.serialization.NoTiFiError;
import notifi.serialization.NoTiFiLocation;
import notifi.serialization.NoTiFiLocationAddition;
import notifi.serialization.NoTiFiLocationDeletion;
import notifi.serialization.NoTiFiMessage;
import notifi.serialization.NoTiFiRegister;

/**
 * class to perform tests on notifi client: various bad scenarios.
 *
 */
public class NoTiFiClientTest {

	private static DatagramSocket socket;
	private static int port;
	private static DatagramPacket receivePacket;
	private static DatagramPacket sendPacket;
	private static byte[] bytesToReceive;
	private static byte[] bytesToSend;
	private static Inet4Address address;

	/**
	 * Runs tests on client as fake server. First run this, then connect client to
	 * localhost 12345. Client should write something like:
	 * 
	 * java notifi.app.NoTiFiClient localhost 12345 localhost <br>
	 * Unexpected MSG ID<br>
	 * test error<br>
	 * Addition lat: 22.0 lon: 11.0 name: here desc: there<br>
	 * Deletion lat: 22.0 lon: 11.0 name: here desc: there<br>
	 * Unexpected message type<br>
	 * Unexpected message type<br>
	 * Unexpected message type<br>
	 * Unable to parse message: *Q/<br>
	 * quit<br>
	 * Unexpected MSG ID<br>
	 * test error<br>
	 * 
	 * @param args none
	 * @throws IOException          should not happen
	 * @throws InterruptedException should not happen
	 */
	public static void main(String[] args) throws IOException, InterruptedException {

		port = 12345;
		socket = new DatagramSocket(port);
		bytesToReceive = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];
		bytesToSend = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];
		receivePacket = new DatagramPacket(bytesToReceive, bytesToReceive.length);
		address = (Inet4Address) Inet4Address.getByName("localhost");

		// init
		socket.receive(receivePacket);
		int length = receivePacket.getLength();
		byte[] data = new byte[length];
		System.arraycopy(receivePacket.getData(), 0, data, 0, length);
		NoTiFiRegister m = (NoTiFiRegister) NoTiFiMessage.decode(data);
		System.out.println("received first register");

		bytesToSend = (new NoTiFiACK(m.getMsgId() + 25)).encode(); // wrong message id first
		sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, receivePacket.getAddress(),
				receivePacket.getPort());

		Thread.sleep(1000);

		System.out.println("sending wrong ACK");
		socket.send(sendPacket);

		// send error to test if client works while waiting for ACK
		System.out.println("sending test message");
		bytesToSend = (new NoTiFiError(42, "test error")).encode(); // wrong message id first
		sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, receivePacket.getAddress(),
				receivePacket.getPort());
		socket.send(sendPacket);

		// second time ok
		socket.receive(receivePacket);

		length = receivePacket.getLength();
		data = new byte[length];
		System.arraycopy(receivePacket.getData(), 0, data, 0, length);
		m = (NoTiFiRegister) NoTiFiMessage.decode(data);
		bytesToSend = (new NoTiFiACK(m.getMsgId())).encode(); // wrong message id first
		sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, receivePacket.getAddress(),
				receivePacket.getPort());
		System.out.println("received second register");

		Thread.sleep(500);

		System.out.println("sending correct ACK");
		socket.send(sendPacket);

		// send add and delete
		Thread.sleep(200);
		System.out.println("sending add message");
		bytesToSend = (new NoTiFiLocationAddition(42, new NoTiFiLocation(321, 11, 22, "here", "there"))).encode();
		sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, receivePacket.getAddress(),
				receivePacket.getPort());
		socket.send(sendPacket);

		Thread.sleep(200);
		System.out.println("sending delete message");
		bytesToSend = (new NoTiFiLocationDeletion(42, new NoTiFiLocation(321, 11, 22, "here", "there"))).encode();
		sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, receivePacket.getAddress(),
				receivePacket.getPort());
		socket.send(sendPacket);

		// send unexpected message types
		Thread.sleep(200);
		System.out.println("sending register message");
		bytesToSend = (new NoTiFiRegister(42, address, 12113)).encode();
		sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, receivePacket.getAddress(),
				receivePacket.getPort());
		socket.send(sendPacket);

		Thread.sleep(200);
		System.out.println("sending deregister message");
		bytesToSend = (new NoTiFiDeregister(42, address, 12113)).encode();
		sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, receivePacket.getAddress(),
				receivePacket.getPort());
		socket.send(sendPacket);

		Thread.sleep(200);
		System.out.println("sending ack message");
		bytesToSend = (new NoTiFiACK(250)).encode();
		sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, receivePacket.getAddress(),
				receivePacket.getPort());
		socket.send(sendPacket);

		// unable to parse message
		Thread.sleep(200);
		System.out.println("sending bad message");
		bytesToSend = (new NoTiFiRegister(42, address, 12113)).encode();
		bytesToSend[0] = 1;
		sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, receivePacket.getAddress(),
				receivePacket.getPort());
		socket.send(sendPacket);

		// deregister - must be inizialized by client
		System.out.println("<<< now type 'quit' into client's console >>>>");

		socket.receive(receivePacket);
		length = receivePacket.getLength();
		data = new byte[length];
		System.arraycopy(receivePacket.getData(), 0, data, 0, length);
		NoTiFiDeregister d = (NoTiFiDeregister) NoTiFiMessage.decode(data);
		System.out.println("received first register");

		bytesToSend = (new NoTiFiACK(d.getMsgId() + 25)).encode(); // wrong message id first
		sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, receivePacket.getAddress(),
				receivePacket.getPort());

		Thread.sleep(500);

		System.out.println("sending wrong ACK");
		socket.send(sendPacket);

		// send error to test if client works while waiting for ACK
		System.out.println("sending test message");
		bytesToSend = (new NoTiFiError(42, "test error")).encode(); // wrong message id first
		sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, receivePacket.getAddress(),
				receivePacket.getPort());
		socket.send(sendPacket);

		// second time ok
		socket.receive(receivePacket);
		System.out.println("received second deregister");

		length = receivePacket.getLength();
		data = new byte[length];
		System.arraycopy(receivePacket.getData(), 0, data, 0, length);
		d = (NoTiFiDeregister) NoTiFiMessage.decode(data);
		bytesToSend = (new NoTiFiACK(d.getMsgId())).encode(); // wrong message id first
		sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, receivePacket.getAddress(),
				receivePacket.getPort());
		socket.send(sendPacket);
		System.out.println("send valid deregister ACK");
		System.out.println("client should shut down (it should NOT write Unable to deregister).");

	}

}
