/************************************************
*
* Author: Antonin Smid
* Assignment: Program 6
* Class: CSI4321
*
************************************************/

package notifi.app.test;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;

import notifi.app.NoTiFiClient;
import notifi.serialization.NoTiFiDeregister;
import notifi.serialization.NoTiFiError;
import notifi.serialization.NoTiFiMessage;
import notifi.serialization.NoTiFiRegister;

/**
 * Class to test the notifi server. In fact it is client with possible wrong
 * scenarios
 *
 */
public class NoTiFiServerTest {

	static int serverPort = 12345;
	static int clientPort = 12346;
	static Inet4Address serverAddress;

	/**
	 * Runs the test. First run the server and then check the output and log.
	 * 
	 * @param args non needed
	 * @throws IllegalArgumentException should not throw
	 * @throws IOException              should not throw
	 * @throws InterruptedException     should not throw
	 */
	public static void main(String[] args) throws IllegalArgumentException, IOException, InterruptedException {
		serverAddress = (Inet4Address) InetAddress.getByName("localhost");
		registerMulticast();
		alreadyRegistered();
		incorrectPort();
		deregister();
		otherMessage();
		unexpectedMessageCode();
		unableToParse();
	}

	/**
	 * test register multicast address, receive NoTiFiError [errorMessage=Bad
	 * address, msgId=42]
	 * 
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static void registerMulticast() throws IllegalArgumentException, IOException, InterruptedException {

		DatagramSocket socket = new DatagramSocket(clientPort);
		NoTiFiRegister message = new NoTiFiRegister(42, (Inet4Address) InetAddress.getByName("224.0.0.5"), clientPort);
		byte[] sendData = message.encode();
		DatagramPacket packet = new DatagramPacket(sendData, sendData.length, serverAddress, serverPort);
		socket.send(packet);

		Thread.sleep(200);

		byte[] receiveData = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, NoTiFiClient.MAX_DATAGRAM_LENGTH);
		socket.receive(receivePacket);

		int length = receivePacket.getLength();
		byte[] data = new byte[length];
		System.arraycopy(receivePacket.getData(), 0, data, 0, length);

		NoTiFiMessage m = NoTiFiMessage.decode(data);

		System.out.println(m);

		socket.close();

	}

	/**
	 * test register one subscriber twice, should produce NoTiFiACK [msgId=42]
	 * NoTiFiError [errorMessage=Already registered, msgId=42]
	 * 
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static void alreadyRegistered() throws IllegalArgumentException, IOException, InterruptedException {

		DatagramSocket socket = new DatagramSocket(clientPort);
		NoTiFiRegister message = new NoTiFiRegister(42, (Inet4Address) InetAddress.getByName("localhost"), clientPort);
		byte[] sendData = message.encode();
		DatagramPacket packet = new DatagramPacket(sendData, sendData.length, serverAddress, serverPort);
		socket.send(packet);

		Thread.sleep(200);

		byte[] receiveData = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, NoTiFiClient.MAX_DATAGRAM_LENGTH);
		socket.receive(receivePacket);
		int length = receivePacket.getLength();
		byte[] data = new byte[length];
		System.arraycopy(receivePacket.getData(), 0, data, 0, length);
		NoTiFiMessage m = NoTiFiMessage.decode(data);
		System.out.println(m);

		Thread.sleep(200);

		socket.send(packet);
		socket.receive(receivePacket);
		length = receivePacket.getLength();
		data = new byte[length];
		System.arraycopy(receivePacket.getData(), 0, data, 0, length);
		m = NoTiFiMessage.decode(data);
		System.out.println(m);

		socket.close();

	}

	/**
	 * test to put different port to socket and inside of the register message
	 * NoTiFiError [errorMessage=Incorrect Port, msgId=42]
	 * 
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static void incorrectPort() throws IllegalArgumentException, IOException, InterruptedException {

		DatagramSocket socket = new DatagramSocket(clientPort + 25);
		NoTiFiRegister message = new NoTiFiRegister(42, (Inet4Address) InetAddress.getByName("224.0.0.5"), clientPort);
		byte[] sendData = message.encode();
		DatagramPacket packet = new DatagramPacket(sendData, sendData.length, serverAddress, serverPort);
		socket.send(packet);

		Thread.sleep(200);

		byte[] receiveData = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, NoTiFiClient.MAX_DATAGRAM_LENGTH);
		socket.receive(receivePacket);

		int length = receivePacket.getLength();
		byte[] data = new byte[length];
		System.arraycopy(receivePacket.getData(), 0, data, 0, length);

		NoTiFiMessage m = NoTiFiMessage.decode(data);

		System.out.println(m);

		socket.close();

	}

	/**
	 * test to deregister non existent address, then deregister correct address, and
	 * receive ACK NoTiFiError [errorMessage=Unknown client, msgId=42] NoTiFiACK
	 * [msgId=42]
	 * 
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static void deregister() throws IllegalArgumentException, IOException, InterruptedException {

		DatagramSocket socket = new DatagramSocket(clientPort);
		NoTiFiDeregister message = new NoTiFiDeregister(42, (Inet4Address) InetAddress.getByName("127.0.0.42"),
				clientPort);
		byte[] sendData = message.encode();
		DatagramPacket packet = new DatagramPacket(sendData, sendData.length, serverAddress, serverPort);
		socket.send(packet);

		Thread.sleep(200);

		byte[] receiveData = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, NoTiFiClient.MAX_DATAGRAM_LENGTH);
		socket.receive(receivePacket);
		int length = receivePacket.getLength();
		byte[] data = new byte[length];
		System.arraycopy(receivePacket.getData(), 0, data, 0, length);
		NoTiFiMessage m = NoTiFiMessage.decode(data);
		System.out.println(m);

		Thread.sleep(200);

		message = new NoTiFiDeregister(42, (Inet4Address) InetAddress.getByName("localhost"), clientPort);
		sendData = message.encode();
		packet = new DatagramPacket(sendData, sendData.length, serverAddress, serverPort);
		socket.send(packet);
		receiveData = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];
		receivePacket = new DatagramPacket(receiveData, NoTiFiClient.MAX_DATAGRAM_LENGTH);
		socket.receive(receivePacket);
		length = receivePacket.getLength();
		data = new byte[length];
		System.arraycopy(receivePacket.getData(), 0, data, 0, length);
		m = NoTiFiMessage.decode(data);
		System.out.println(m);

		socket.close();

	}

	/**
	 * test to send error message which is unexpected by the server, produces
	 * NoTiFiError [errorMessage=Unexpected message type: 4, msgId=42]
	 * 
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static void otherMessage() throws IllegalArgumentException, IOException, InterruptedException {

		DatagramSocket socket = new DatagramSocket(clientPort);
		NoTiFiError message = new NoTiFiError(42, "yo yo error");
		byte[] sendData = message.encode();
		DatagramPacket packet = new DatagramPacket(sendData, sendData.length, serverAddress, serverPort);
		socket.send(packet);

		Thread.sleep(200);

		byte[] receiveData = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, NoTiFiClient.MAX_DATAGRAM_LENGTH);
		socket.receive(receivePacket);

		int length = receivePacket.getLength();
		byte[] data = new byte[length];
		System.arraycopy(receivePacket.getData(), 0, data, 0, length);
		NoTiFiMessage m = NoTiFiMessage.decode(data);
		System.out.println(m);

		socket.close();

	}

	/**
	 * test to produce message with code 7, which is not defined by notifi.
	 * Produces: NoTiFiError [errorMessage=Unknown code: 7, msgId=0]
	 * 
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static void unexpectedMessageCode() throws IllegalArgumentException, IOException, InterruptedException {

		DatagramSocket socket = new DatagramSocket(clientPort);
		NoTiFiError message = new NoTiFiError(42, "yo yo error");
		byte[] sendData = message.encode();
		sendData[0] = (byte) 55;
		DatagramPacket packet = new DatagramPacket(sendData, sendData.length, serverAddress, serverPort);
		socket.send(packet);

		Thread.sleep(200);

		byte[] receiveData = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, NoTiFiClient.MAX_DATAGRAM_LENGTH);
		socket.receive(receivePacket);

		int length = receivePacket.getLength();
		byte[] data = new byte[length];
		System.arraycopy(receivePacket.getData(), 0, data, 0, length);
		NoTiFiMessage m = NoTiFiMessage.decode(data);
		System.out.println(m);

		socket.close();

	}

	/**
	 * test to send short packet which can not be parsed, produces NoTiFiError
	 * [errorMessage=Unable to parse message, msgId=0]
	 * 
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private static void unableToParse() throws IllegalArgumentException, IOException, InterruptedException {

		DatagramSocket socket = new DatagramSocket(clientPort);
		NoTiFiRegister message = new NoTiFiRegister(42, (Inet4Address) InetAddress.getByName("localhost"), clientPort);
		byte[] sendData = message.encode();
		DatagramPacket packet = new DatagramPacket(sendData, sendData.length - 3, serverAddress, serverPort);
		socket.send(packet);

		Thread.sleep(200);

		byte[] receiveData = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, NoTiFiClient.MAX_DATAGRAM_LENGTH);
		socket.receive(receivePacket);

		int length = receivePacket.getLength();
		byte[] data = new byte[length];
		System.arraycopy(receivePacket.getData(), 0, data, 0, length);
		NoTiFiMessage m = NoTiFiMessage.decode(data);
		System.out.println(m);

		socket.close();

	}

}
