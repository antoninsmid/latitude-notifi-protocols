/************************************************
*
* Author: Antonin Smid
* Assignment: Program 5
* Class: CSI4321
*
************************************************/
package notifi.app;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Scanner;

import notifi.serialization.NoTiFiDeregister;
import notifi.serialization.NoTiFiMessage;
import notifi.serialization.NoTiFiRegister;
import notifi.util.IOHelper;
import notifi.util.Validator;

/**
 * Client application for the NoTiFi protocol
 *
 */
public class NoTiFiClient {
	
	static protected Inet4Address serverAddress;
	static protected Inet4Address localAddress;
	static protected int serverPort;
	static protected int localPort;
	static private DatagramSocket socket;
	static public final int CONNECTION_TIMEOUT = 3000; // used for de/registration
	static public final int RUN_TIMEOUT = 500; // used during the normal run time
	static private final int REGISTER_TRIES_COUNT = 2; // during de/registration how many times try to resend packet
														// before fail
	static public final int MAX_DATAGRAM_LENGTH = 536; // max possible length of packet used to init byte arrays to
															// hold received data
	private static String QUIT_CODE = "quit";
	public static boolean shutdown = false; // this flag is used to initiate shut down procedure

	/**
	 * Runnable Client NoTiFi application
	 * 
	 * @param args server name, server port, client IP
	 */
	public static void main(String[] args) {

		// check the correct length of arguments
		if (args.length != 3) {
			System.err.println("Wrong number of arguments. Should be <serverIP/name> <server port> <local client IP>");
			System.exit(1);
		}

		// parse the arguments
		try {
			serverAddress = (Inet4Address) Inet4Address.getByName(args[0]);
		} catch (UnknownHostException e) {
			System.err.println("Could not parse the server address argument.");
			System.exit(1);
		}
		
		try {
			localAddress = (Inet4Address) Inet4Address.getByName(args[2]);
		} catch (UnknownHostException e) {
			System.err.println("Could not parse the client address argument.");
			System.exit(1);
		}

		if (Validator.validatePort(Integer.parseInt(args[1]))) {
			serverPort = Integer.parseInt(args[1]);
		} else {
			System.err.println("Invalid server port number.");
			System.exit(1);
		}

		// init the array and packet to receive UDP packets
		byte[] bytesToReceive = new byte[MAX_DATAGRAM_LENGTH];
		DatagramPacket receivePacket = new DatagramPacket(bytesToReceive, MAX_DATAGRAM_LENGTH);

		// init the setup and register to receive updates from server
		try {
			socket = new DatagramSocket();
			localPort = socket.getLocalPort();
			socket.setReuseAddress(true);
			socket.setSoTimeout(CONNECTION_TIMEOUT);
			openCloseProcedure(receivePacket, true);
		} catch (IOException e) {
			System.err.println("Unable to Register.");
			System.exit(1);
		}

		// infinite loop in new thread
		Thread worker = new Thread(new NoTiFiClientWorker(socket));
		worker.start();

		// scanner to harvest user input
		Scanner userInputScanner = new Scanner(System.in);
		while (true) {
			String line = userInputScanner.nextLine();
			if (QUIT_CODE.equals(line)) {
				break;
			}
		}

		// deregister sequence
		shutdown = true;
		userInputScanner.close();
		try {
			Thread.sleep(RUN_TIMEOUT);
		} catch (InterruptedException e) {
		}
		try {
			openCloseProcedure(receivePacket, false);
		} catch (IOException e) {
			System.err.println("Unable to deregister.");
		}

	}

	/**
	 * opens new connection or close the current connection in the time between
	 * waiting for the ACK to confirm the desired actions, other packets are
	 * processed normally
	 * 
	 * @param receivePacket UDP packet to receive messages
	 * @param init          if true - init sequence, otherwise shutdown
	 * @throws IOException if does not receive response to action for the second
	 *                     time within the timer
	 */
	private static void openCloseProcedure(DatagramPacket receivePacket, boolean init) throws IOException {

		int messageId = IOHelper.getRandomMessageId();

		// prepare message based on the action
		NoTiFiMessage msg;
		if (init) {
			msg = new NoTiFiRegister(messageId, localAddress, localPort);
		} else {
			msg = new NoTiFiDeregister(messageId, localAddress, localPort);
		}

		byte[] bytesToSend = msg.encode();
		DatagramPacket sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, serverAddress, serverPort);

		int tries = 0;
		while (tries < REGISTER_TRIES_COUNT) {

			socket.send(sendPacket);

			// wait for ACK
			try {
				while (true) {
					socket.receive(receivePacket);
					if (!receivePacket.getAddress().equals(serverAddress) || receivePacket.getPort() != serverPort) {
						System.err.println("Received packet from unknown source.");
						continue;
					}
					if (NoTiFiClientProcessor.processData(messageId, true, receivePacket)) {
						// everything went smooth, ACK received
						return;
					}
				}
			} catch (SocketTimeoutException e) {
				tries++;
			}
		}
		throw new IOException("Unable to de/register.");
	}

}
