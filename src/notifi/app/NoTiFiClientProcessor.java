/************************************************
*
* Author: Antonin Smid
* Assignment: Program 8
* Class: CSI4321
*
************************************************/

package notifi.app;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;

import notifi.serialization.NoTiFiACK;
import notifi.serialization.NoTiFiDeregister;
import notifi.serialization.NoTiFiError;
import notifi.serialization.NoTiFiLocation;
import notifi.serialization.NoTiFiLocationAddition;
import notifi.serialization.NoTiFiLocationDeletion;
import notifi.serialization.NoTiFiMessage;
import notifi.serialization.NoTiFiRegister;
import notifi.util.IOHelper;

/**
 * Class enclosing the processing function - the printing of the messages.
 * Common code for both notificlient and notifimulticast client
 *
 */
public class NoTiFiClientProcessor {

	/**
	 * Process the message - decode and decide how to handle id
	 * 
	 * @param expectedAck    message id of the expected ack
	 * @param isExpectingAck if the ACK is expected
	 * @param packet         received datagram packet with the
	 * @return if the ACK was correct in case that ACK is expected, otherwise if
	 *         addition or deletion received
	 */
	protected static boolean processData(int expectedAck, boolean isExpectingAck, DatagramPacket packet) {
		// System.out.println("process data ");
		byte[] data = new byte[packet.getLength()];
		System.arraycopy(packet.getData(), 0, data, 0, packet.getLength());
		// IOHelper.printByteArrayBinary(data);

		NoTiFiMessage message;

		try {
			message = NoTiFiMessage.decode(data);
		} catch (IllegalArgumentException | IOException e) {
			try {
				System.err.println("Unable to parse message: " + new String(data, IOHelper.CHARENC));
				// e.printStackTrace();
			} catch (UnsupportedEncodingException e1) {
				// does not happen
			}
			return false;
		}

		// in case the processing is called from init/shutdown sequence and ACK is
		// expected
		if (isExpectingAck) {
			if (message instanceof NoTiFiACK) {
				NoTiFiACK ack = (NoTiFiACK) message;
				if (expectedAck == ack.getMsgId()) {
					// System.out.println(ack.getMsgId());
					// expected ACK, return true to upper layer
					return true;
				}
				// received unexpected ACK
				System.err.println("Unexpected MSG ID");
				return false;
			}
		}

		// here is processing of other message types

		// add or delete update
		if (message instanceof NoTiFiLocationAddition) {
			NoTiFiLocationAddition addition = (NoTiFiLocationAddition) message;
			printLocation(addition);
			return true;
		}

		if (message instanceof NoTiFiLocationDeletion) {
			NoTiFiLocationDeletion deletion = (NoTiFiLocationDeletion) message;
			printLocation(deletion);
			return true;
		}

		// received error
		if (message instanceof NoTiFiError) {
			NoTiFiError err = (NoTiFiError) message;
			System.err.println(err.getErrorMessage());
			return false;
		}

		// unexpected message type
		if (message instanceof NoTiFiACK || message instanceof NoTiFiDeregister || message instanceof NoTiFiRegister) {
			System.err.println("Unexpected message type");
			return false;
		}

		// in case client waits for correct ACK, process messages, but in the end return
		// false,
		// it was not the ACK that client expected
		return false;
	}

	/**
	 * Prints the location addition update
	 * 
	 * @param message containing the addition update to print
	 */
	protected static void printLocation(NoTiFiLocationAddition message) {
		NoTiFiLocation l = message.getLocation();
		printLocation(l, "Addition");
	}

	/**
	 * Prints the location deletion update
	 * 
	 * @param message message containing the deletion update to print
	 */
	protected static void printLocation(NoTiFiLocationDeletion message) {
		NoTiFiLocation l = message.getLocation();
		printLocation(l, "Deletion");
	}

	/**
	 * Prints locations latitude, longitude, name and desctiption togerher with
	 * prefix (what happened: add/delete)
	 * 
	 * @param l      location to print out
	 * @param prefix description of the action happening with the location
	 *               (add/delete)
	 */
	private static void printLocation(NoTiFiLocation l, String prefix) {
		System.out.println(prefix + " lat: " + l.getLatitude() + " lon: " + l.getLongitude() + " name: "
				+ l.getLocationName() + " desc: " + l.getLocationDescription());
	}

}
