/************************************************
*
* Author: Antonin Smid
* Assignment: Program 8
* Class: CSI4321
*
************************************************/

package notifi.app;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * NoTiFi client that uses multicast receiving. Takes source and port, does not
 * validate the source of messages.
 *
 */
public class NoTiFiMulticastClient {

	// source multicast address to join
	static protected Inet4Address serverAddress;
	// source UDP port
	static protected int serverPort;
	// the future socket used to receive messages
	static private MulticastSocket socket;

	static public final int MAX_DATAGRAM_LENGTH = 536; // max possible length of packet used to init byte arrays to
	static public final int SHUT_DOWN_TIMEOUT = 1000; // wait on quit, so that the client can read the rest of messages

	// hold received data
	private static String QUIT_CODE = "quit";
	public static boolean shutdown = false; // this flag is used to initiate shut down procedure

	// the client code - first load and check arguments, then create the worker
	// thread and wait for user to write quit, then terminate.
	public static void main(String[] args) {

		// check the correct length of arguments
		if (args.length != 2) {
			System.err.println("Wrong number of arguments. Should be <multicast address> <server port>");
			System.exit(1);
		}

		// try to assign multicast address based on user-provided parameter
		try {
			serverAddress = (Inet4Address) Inet4Address.getByName(args[0]);
		} catch (UnknownHostException e) {
			System.out.println("Problem parsing thhe multicast address");
			System.exit(1);
		}

		// in case the address is not multicast, write error and terminate
		if (!serverAddress.isMulticastAddress()) {
			System.out.println("The address must be a multicast address.");
			System.exit(1);
		}

		// assign the port
		serverPort = Integer.parseInt(args[1]);

		// init the setup and register to receive updates from server
		try {
			socket = new MulticastSocket(serverPort);
			socket.setReuseAddress(true);
			socket.joinGroup(serverAddress);
		} catch (IOException e) {
			System.err.println("Unable to join the multicast.");
			System.exit(1);
		}

		// create and start the worker thread
		Thread workerThread = new Thread(new NoTiFiMulticastWorker(socket));
		workerThread.start();

		// scanner to harvest user input
		Scanner userInputScanner = new Scanner(System.in);
		while (true) {
			String line = userInputScanner.nextLine();
			if (QUIT_CODE.equals(line)) {
				break;
			}
		}

		// shut down, leave group, close the socket
		shutdown = true;
		userInputScanner.close();

		try {
			Thread.sleep(SHUT_DOWN_TIMEOUT);
		} catch (InterruptedException e) {
		}

		try {
			socket.leaveGroup(serverAddress);
			socket.close();
		} catch (IOException e) {
			System.out.println("Failed to leave the multicast group.");
		}

	}
}
