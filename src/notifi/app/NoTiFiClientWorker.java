/************************************************
*
* Author: Antonin Smid
* Assignment: Program 5
* Class: CSI4321
*
************************************************/
package notifi.app;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

/**
 * Location Client Worker class runs the infinite loop waiting for the location
 * updates
 *
 */
public class NoTiFiClientWorker implements Runnable {

	
	private DatagramSocket socket;
	private byte[] bytesToReceive;
	private DatagramPacket receivePacket;

	/**
	 * constructs the worker, inits the values for socket, byte array to save data
	 * to and new datagram packet to receive data, also set's faster timeout
	 * 
	 * @param socket to use for communication
	 */
	public NoTiFiClientWorker(DatagramSocket socket) {
		this.socket = socket;
		bytesToReceive = new byte[NoTiFiClient.MAX_DATAGRAM_LENGTH];
		receivePacket = new DatagramPacket(bytesToReceive, NoTiFiClient.MAX_DATAGRAM_LENGTH);

		try {
			socket.setSoTimeout(NoTiFiClient.RUN_TIMEOUT);
		} catch (SocketException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void run() {

		// until user writes 'quit' and the client is shutdown, runs cycle to update locations
		while (!NoTiFiClient.shutdown) {
			try {
				// receive updates
				socket.receive(receivePacket);

				// check the source of update
				if (!receivePacket.getAddress().equals(NoTiFiClient.serverAddress)
						|| receivePacket.getPort() != NoTiFiClient.serverPort) {
					System.err.println("Received packet from unknown source.");
					continue;
				}

				// process update
				NoTiFiClientProcessor.processData(0, false, receivePacket);

			} catch (SocketTimeoutException ste) {
			} catch (IOException e) {
				//e.printStackTrace();
				break;
			}
		}

		// prepare for deregistration, set timeout back
		try {
			socket.setSoTimeout(NoTiFiClient.CONNECTION_TIMEOUT);
		} catch (SocketException e) {
			//e.printStackTrace();
		}

	}

}
