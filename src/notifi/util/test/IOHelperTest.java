/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4 Test
* Class: CSI4321
*
************************************************/
package notifi.util.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.Inet4Address;
import java.net.UnknownHostException;

import org.junit.jupiter.api.Test;

import notifi.util.IOHelper;

/**
 * Test class for IO helper utility
 */
class IOHelperTest {

    // taken from
    // https://stackoverflow.com/questions/1119385/junit-test-for-system-out-println
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    // test int to 4 bit convert
    @Test
    void testIntTo4bitUI() {
        byte b = IOHelper.intTo4bitUI(14, true);
        int read = b & 0xFF;
        assertEquals(224, read);

        b = IOHelper.intTo4bitUI(14, false);
        read = b & 0xFF;
        assertEquals(14, read);

        assertThrows(IllegalArgumentException.class, () -> {
            IOHelper.intTo4bitUI(16, true);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            IOHelper.intTo4bitUI(16, false);
        });
    }

    // test int to 8 bit convert
    @Test
    void testIntTo8bitUI() {
        byte b = IOHelper.intTo8bitUI(254);
        int read = b & 0xFF;
        assertEquals(254, read);
    }

    // test byte bit printer
    @Test
    void testPrintByteBinary() {
        System.setOut(new PrintStream(outContent));
        IOHelper.printByteBinary((byte) 254);
        assertEquals("11111110\n", outContent.toString());
        System.setOut(originalOut);
    }

    // test byte array bit printer
    @Test
    void testPrintByteArrayBinary() {
        System.setOut(new PrintStream(outContent));
        byte[] arr = new byte[] { (byte) 254, 126 };
        IOHelper.printByteArrayBinary(arr);
        assertEquals("11111110-01111110\n", outContent.toString());
        System.setOut(originalOut);
    }

    // test endian swapper
    @Test
    void testSwapEndianity() {
        byte[] arr = new byte[] { 1, 3 };
        byte[] arrSwap = new byte[] { 3, 1 };
        assertEquals(arrSwap[0], IOHelper.swapEndianity(arr)[0]);
        assertEquals(arrSwap[1], IOHelper.swapEndianity(arr)[1]);
    }

    // test address cloning
    @Test
    void testCloneAddress() throws UnknownHostException {

        byte[] addrBytes = new byte[] { (byte) 192, 0, (byte) 168, (byte) 253 };
        Inet4Address addr = (Inet4Address) Inet4Address.getByAddress(addrBytes);
        Inet4Address addrClone = IOHelper.cloneAddress(addr);
        assertEquals(addr.getHostAddress(), addrClone.getHostAddress());

    }

    // test long to byte array conversion
    @Test
    void testLongToBytes() {
        long l = 15;
        byte[] array = IOHelper.longToBytes(l);
        byte[] arrayTrue = new byte[] { 15, 0, 0, 0, 0, 0, 0, 0 }; // little endian
        for (int i = 0; i < 8; i++) {
            assertEquals(arrayTrue[i], array[i]);
        }
    }

    // test byte array to long conversion
    @Test
    void testBytesToLong() {
        byte[] array = new byte[] { 15, 0, 0, 0, 0, 0, 0, 0 }; // little endian
        long l = IOHelper.bytesToLong(array);
        assertEquals(15l, l);
    }

    // test string to NoTiFi bytes encode
    @Test
    void testEncodeString() throws UnsupportedEncodingException {
        byte[] encoded = IOHelper.encodeString("ahoj");
        assertEquals(4, encoded[0]);
        byte[] ahojEnc = "ahojEnc".getBytes(IOHelper.CHARENC);
        for (int i = 0; i < 4; i++) {
            assertEquals(encoded[i + 1], ahojEnc[i]);
        }
    }

    // test double to NoTiFi byte array encode
    @Test
    void testEncodeDouble() {
        byte[] encoded = IOHelper.encodeDouble(25.0); // little endian
        assertEquals(64, encoded[7]);
        assertEquals(57, encoded[6]);
        for (int i = 0; i < 6; i++) {
            assertEquals(0, encoded[i]);
        }

    }

    // test double from byte array decode
    @Test
    void testDecodeDouble() {
        byte[] encoded = new byte[] { 0, 0, 0, 0, 0, 0, 57, 64 }; // little endian
        double d = IOHelper.decodeDouble(encoded);
        assertEquals(25.0, d);
    }

}
