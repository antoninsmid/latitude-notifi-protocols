/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4 Test
* Class: CSI4321
*
************************************************/
package notifi.util.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import notifi.util.Validator;

/**
 * test validation utility class
 */
class ValidatorTest {

    // test 4 bit validation
    @Test
    void testCheck4bitUI() {
        assertFalse(Validator.check4bitUI(16));
        assertFalse(Validator.check4bitUI(-1));
        assertTrue(Validator.check4bitUI(0));
        assertTrue(Validator.check4bitUI(15));
    }

    // test 8 bit validation
    @Test
    void testCheck8bitUI() {
        assertFalse(Validator.check8bitUI(256));
        assertFalse(Validator.check8bitUI(-1));
        assertTrue(Validator.check8bitUI(0));
        assertTrue(Validator.check8bitUI(255));
    }

    // test NoFiTi code validation
    @Test
    void testValidateCode() {
        assertFalse(Validator.validateCode(6));
        assertFalse(Validator.validateCode(-1));
        assertTrue(Validator.validateCode(0));
        assertTrue(Validator.validateCode(5));
    }

    // test port validation
    @Test
    void testValidatePort() {
        assertFalse(Validator.validatePort(-1));
        assertFalse(Validator.validatePort(65536));
        assertTrue(Validator.validatePort(0));
        assertTrue(Validator.validatePort(65535));
    }

    // test unsigned int 4 byte validation
    @Test
    void testValidateUnsignedInt() {
        assertFalse(Validator.validateUnsignedInt(-1));
        assertFalse(Validator.validateUnsignedInt(4_294_967_296L));
        assertTrue(Validator.validateUnsignedInt(0));
        assertTrue(Validator.validateUnsignedInt(4_294_967_295L));
    }

    // test string length validation
    @Test
    void testValidateStringLength() {
        String s = "ahoj";
        assertTrue(Validator.validateStringLength(s, 4));
        assertFalse(Validator.validateStringLength(s, 3));
    }

    // test longitude bounds validation
    @Test
    void testValidateLongitude() {
        assertFalse(Validator.validateLongitude(180.01));
        assertFalse(Validator.validateLongitude(-180.01));
        assertTrue(Validator.validateLongitude(180.0));
        assertTrue(Validator.validateLongitude(-180.0));
    }

    // test latitude bounds validation
    @Test
    void testValidateLatitude() {
        assertFalse(Validator.validateLatitude(90.01));
        assertFalse(Validator.validateLatitude(-90.01));
        assertTrue(Validator.validateLatitude(90.0));
        assertTrue(Validator.validateLatitude(-90.0));
    }

}
