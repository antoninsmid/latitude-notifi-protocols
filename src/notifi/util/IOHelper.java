/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4
* Class: CSI4321
*
************************************************/
package notifi.util;

import java.io.UnsupportedEncodingException;
import java.net.Inet4Address;
import java.net.UnknownHostException;

/**
 * Class that helps with conversions and IO operations
 */
public class IOHelper {

    public static final String CHARENC = "ASCII"; // used character coding ascii
    public static final int NOTIFI_DOUBLE_BYTE_SIZE = 8;

    /**
     * Convert integer to unsigned 4 bit integer and save it to lower or high 4
     * bits.
     * 
     * @param number   input to convert
     * @param position if true - high 4 bits, otherwise low 4
     * @return byte with converted number
     * @throws IllegalArgumentException if input validation out of bounds
     */
    public static byte intTo4bitUI(int number, boolean position) throws IllegalArgumentException {
        if (!Validator.check4bitUI(number)) {
            throw new IllegalArgumentException("wrong number to convert");
        }
        if (!position) {
            return (byte) number;
        } else {
            return (byte) (number << 4);
        }

    }

    /**
     * Convert integer to unsigned 8 bit integer and save to byte
     * 
     * @param number input to convert
     * @return byte with the converted value
     * @throws IllegalArgumentException if integer is out of bounds
     */
    public static byte intTo8bitUI(int number) throws IllegalArgumentException {
        if (!Validator.check8bitUI(number)) {
            throw new IllegalArgumentException("wrong number to convert");
        }
        return (byte) number;
    }

    /**
     * Print byte as binary representation
     * 
     * @param b byte to print
     */
    public static void printByteBinary(byte b) {
        String s1 = String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0');
        System.out.println(s1);
    }

    /**
     * Print an array of bytes as a binary representation
     * 
     * @param array byte array to print
     */
    public static void printByteArrayBinary(byte[] array) {

        for (int i = 0; i < array.length; i++) {
            String s1 = String.format("%8s", Integer.toBinaryString(array[i] & 0xFF)).replace(' ', '0');
            if (i < array.length - 1) {
                System.out.print(s1 + "-");
            } else {
                System.out.print(s1 + "\n");
            }
        }
    }

    /**
     * Swap the order of bytes in byte array
     * 
     * @param array an array to swap
     * @return swapped order byte array
     */
    public static byte[] swapEndianity(byte[] array) {
        byte[] swapped = new byte[array.length];

        for (int i = 0; i < array.length; i++) {
            swapped[i] = array[array.length - i - 1];
        }

        return swapped;
    }

    /**
     * Implements clone for Inet4Addresss
     * 
     * @param address source address
     * @return new cloned address
     */
    public static Inet4Address cloneAddress(Inet4Address address) {
        Inet4Address addressnew;
        try {
            addressnew = (Inet4Address) Inet4Address.getByAddress(address.getAddress());
            return addressnew;
        } catch (UnknownHostException e) {
            // should not ever happen
            // e.printStackTrace();
        }
        return null;
    }

    /**
     * Converts long to bytes
     * 
     * @param in long to convert
     * @return byte array representing the long
     */
    public static byte[] longToBytes(long in) {
        byte[] result = new byte[NOTIFI_DOUBLE_BYTE_SIZE];
        for (int i = 0; i < NOTIFI_DOUBLE_BYTE_SIZE; i++) {
            result[i] = (byte) (in & 0xFF);
            in >>= Byte.SIZE;
        }
        return result;
    }

    /**
     * Converts array of bytes to long
     * 
     * @param bytes to convert, needs to be 8 bytes long array
     * @return long value of the bytes
     */
    public static long bytesToLong(byte[] bytes) {
        long result = 0;
        // IOHelper.printByteArrayBinary(bytes);
        for (int i = 0; i < NOTIFI_DOUBLE_BYTE_SIZE; i++) {
            result <<= Byte.SIZE;
            result |= (bytes[NOTIFI_DOUBLE_BYTE_SIZE - 1 - i] & 0xFF);
        }
        return result;
    }

    /**
     * Encode string to NoTiFi bytes
     * 
     * @param text string to encode
     * @return array of bytes encoding the string
     */
    public static byte[] encodeString(String text) {

        byte[] bytesTranslated;
        try {
            bytesTranslated = text.getBytes(CHARENC);
            byte[] bytesEncoded = new byte[bytesTranslated.length + 1];
            bytesEncoded[0] = (byte) bytesTranslated.length;
            System.arraycopy(bytesTranslated, 0, bytesEncoded, 1, bytesTranslated.length);
            return bytesEncoded;

        } catch (UnsupportedEncodingException e) {
            // should not ever happen
            // e.printStackTrace();
        }
        return null;
    }

    public static byte[] encodeDouble(Double value) {
        long valueLong = Double.doubleToLongBits(value);
        return IOHelper.longToBytes(valueLong);
    }

    public static double decodeDouble(byte[] array) {
        long valueLong = IOHelper.bytesToLong(array);
        return Double.longBitsToDouble(valueLong);
    }
    
    public static int getRandomMessageId() {
    	return (int)(Math.random() * 255);
    }

}
