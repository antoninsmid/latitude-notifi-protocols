/************************************************
*
* Author: Antonin Smid
* Assignment: Program 4
* Class: CSI4321
*
************************************************/
package notifi.util;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

/**
 * Helper class to perform various validations
 */
public class Validator {

    private static final Long UNSIGNED_INT_TOP_LIMIT = 4_294_967_296L; // max unsigned int, user id must be smaller
    private static final int MAX_ABS_LONGITUDE = 180;
    private static final int MAX_ABS_LATITUDE = 90;
    private static final String CHARENC = "ASCII";
    private static CharsetEncoder encoder = Charset.forName(CHARENC).newEncoder();

    /**
     * Validates if the int can fit into 4 bits unsigned
     * 
     * @param number the number to validate
     * @return result if the validation was successful
     */
    public static boolean check4bitUI(int number) {
        if (number < 0 || number > 15) {
            return false;
        }
        return true;
    }

    /**
     * Validates if the int can fit into 8 bits unsigned
     * 
     * @param number the number to validate
     * @return result if the validation was successful
     */
    public static boolean check8bitUI(int number) {
        if (number < 0 || number > 255) {
            return false;
        }
        return true;
    }

    /**
     * Validate correct NoTiFy message code
     * 
     * @param code number to validate as a code
     * @return if the number is NoTiFy code
     */
    public static boolean validateCode(int code) {
        if (code < 0 || code > 5) {
            return false;
        }
        return true;
    }

    /**
     * Validate correct port number
     * 
     * @param port number to validate
     * @return if the number is port
     */
    public static boolean validatePort(int port) {
        if (port < 0 || port > 65535) {
            return false;
        }
        return true;
    }

    /**
     * Validates that unsigned integer is smaller than 2^31 representable in 32 bits
     * 
     * @param number to validate
     * @return if the number is valid unsigned int
     */
    public static boolean validateUnsignedInt(long number) {
        if (number >= UNSIGNED_INT_TOP_LIMIT || number < 0) {
            return false;
        }
        return true;
    }

    /**
     * Validate if string is no too long
     * 
     * @param text   string to validate
     * @param length max acceptable length
     * @return if the string has length 0 to max
     */
    public static boolean validateStringLength(String text, int length) {
        if (text == null) {
            return false;
        }
        // System.out.println(text.length() + ", max: " +length);
        if (text.length() > length) {
            return false;
        }
        return true;
    }
    
	/**
	 * Check if the chars in string are from standard ASCII
	 * 
	 * @param text string to check
	 * @return if the string is valid standard ASCII
	 */
    public static boolean validateStringChars(String text) {
    	for (int i = 0; i < text.length(); i++) {
			if(text.charAt(i) > 128 || text.charAt(i) < 0) {
				return false;
			}
		}
		return encoder.canEncode(text);
    }

    /**
     * Validates if the longitude is within range
     * 
     * @param value longitude to validate
     * @return result of the validation
     */
    public static boolean validateLongitude(double value) {
        if (Math.abs(value) > MAX_ABS_LONGITUDE) {
            return false;
        }
        return true;
    }

    /**
     * Validates if the latitude is within range
     * 
     * @param value latitude to validate
     * @return result of the validation
     */
    public static boolean validateLatitude(double value) {
        if (Math.abs(value) > MAX_ABS_LATITUDE) {
            return false;
        }
        return true;
    }

}
