/************************************************
*
* Author: Antonin Smid
* Assignment: Program 3
* Class: CSI4321
*
************************************************/

package latitude.model;

import java.util.Map;

import mapservice.MapManager;
import notifi.app.NoTiFiServer;

/**
 * Represents latitude map.
 *
 */
public class LatitudeMap {

	private long id;
	private String name;
	private LocationRecordLibrary library;
	

	/**
	 * 	 * generic constructor using fields
	 * @param id map id
	 * @param name map name
	 * @param mapManager publisher to html
	 * @param users set of valid users
	 * @param server notifi server to use for update publishing
	 */
	public LatitudeMap(long id, String name, MapManager mapManager, Map<Long, LatitudeUser> users, NoTiFiServer server) {
		this.id = id;
		this.name = name;
		this.library = new LocationRecordLibrary(mapManager, users, server);
	}

	/**
	 * returns map id
	 * @return id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * returns maps name
	 * @return map name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets new map name
	 * @param name new name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * returns the library of positions
	 * @return location record library
	 */
	public LocationRecordLibrary getLibrary() {
		return library;
	}
	
}
