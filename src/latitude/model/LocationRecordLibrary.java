/************************************************
*
* Author: Antonin Smid
* Assignment: Program 3
* Class: CSI4321
*
************************************************/

package latitude.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import latitude.serialization.LocationRecord;
import latitude.serialization.ValidationException;
import mapservice.Location;
import mapservice.MapManager;
import notifi.app.NoTiFiServer;

/**
 * Represents the collection of records and provides access and management.
 *
 */
public class LocationRecordLibrary {

	// structure to hold the current location records
	private Map<Long, LocationRecord> recordLibrary;
	// html publisher
	private MapManager mapManager;
	// collection of users loaded from file
	private Map<Long, LatitudeUser> users;

	private NoTiFiServer noTiFiServer;

	public static final String USER_NOT_FOUND_CODE = "usernotfound";
	public static final String SUCCESS_CODE = "success";

	/**
	 * constructor for record library
	 * 
	 * @param mapManager map manager to use
	 * @param users      collection of users for this map
	 * @param server	notifi server to be used for updates publishing
	 */
	public LocationRecordLibrary(MapManager mapManager, Map<Long, LatitudeUser> users, NoTiFiServer server) {
		this.mapManager = mapManager;
		this.recordLibrary = new HashMap<>();
		this.users = users;
		this.noTiFiServer = server;
	}

	/**
	 * Write new location record to library. If the ID is already contained,
	 * overrides the current value.
	 * 
	 * @param record new location record to add
	 * @return code informing about the result - user not found or success
	 * @throws ValidationException if record validation fails
	 */
	synchronized public String addLocation(LocationRecord record) throws ValidationException {

		// validates the user id
		if (!users.containsKey(record.getUserId())) {
			return USER_NOT_FOUND_CODE;
		}

		LatitudeUser user = users.get(record.getUserId());
		String newName = user.getName() + ": " + record.getLocationName();

		// if the location is going to be overridden, publish deletion of the current
		// value
		if (recordLibrary.containsKey(record.getUserId())) {
			noTiFiServer.publishDelete(recordLibrary.get(record.getUserId()));
		}

		// Add location to hashset
		recordLibrary.put(record.getUserId(), new LocationRecord(record.getUserId(), record.getLongitude(),
				record.getLatitude(), newName, record.getLocationDescription()));

		// Add location to public html map
		// System.out.println("name: " + newName + " desc: " +
		// record.getLocationDescription());
		Location l = new Location(newName.toString(), record.getLongitude(), record.getLatitude(),
				record.getLocationDescription().toString(), Location.Color.BLUE);
		// System.out.println("name: " + l.getName() + " desc: " + l.getDescription());
		mapManager.addLocation(l);

		// publish addition of the new location
		noTiFiServer.publishAdd(record);

		return SUCCESS_CODE;
	}

	/**
	 * provides access to location records
	 * 
	 * @return list of location records
	 */
	public List<LocationRecord> getLocationRecords() {
		List<LocationRecord> recordsToReturn = new ArrayList<>();
		recordLibrary.values().stream().forEach(kv -> {
			recordsToReturn.add(kv);
		});
		return recordsToReturn;
	}

}
