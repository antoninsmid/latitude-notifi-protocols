/************************************************
*
* Author: Antonin Smid
* Assignment: Program 3
* Class: CSI4321
*
************************************************/

package latitude.model;

/**
 * Representation of user
 *
 */
public class LatitudeUser {
	
	private long id;
	private String name;
	private String password;

	/**
	 * initializes user
	 * @param id user id
	 * @param name user name
	 * @param password password for new user
	 */
	public LatitudeUser(long id, String name, String password) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
	}

	/**
	 * Returns user id
	 * @return user id
	 */
	public long getId() {
		return id;
	}

	/**
	 * sets user id
	 * @param id new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * returns user's name
	 * @return user name
	 */
	public String getName() {
		return name;
	}

	/**
	 * sets user name
	 * @param name new user name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * gets password
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * sets password
	 * @param password to update
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "latitudeUser [id=" + id + ", name=" + name + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LatitudeUser other = (LatitudeUser) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
