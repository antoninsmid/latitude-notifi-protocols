/************************************************
*
* Author: Antonin Smid
* Assignment: Program 3 test
* Class: CSI4321
*
************************************************/

package latitude.model.test;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import latitude.model.LatitudeUser;

/**
 * Test class for LatitudeUserTest.
 *
 */
class LatitudeUserTest {


	// test constructor
	@Test
	@DisplayName("simple constructor test")
	void userConstructorTest() {
		var u = new LatitudeUser(25, "john", "john123");
		assertEquals(25, u.getId());
		assertEquals("john", u.getName());
		assertEquals("john123", u.getPassword());
	}
	
	// test setters
	@Test
	@DisplayName("simple setter test")
	void userSettertest() {
		var u = new LatitudeUser(25, "john", "john123");
		u.setId(42);
		assertEquals(42, u.getId());
		u.setName("jammie");
		assertEquals("jammie", u.getName());
		u.setPassword("jammie321");
		assertEquals("jammie321", u.getPassword());
	}	
	
	// test tostring
	@Test
	@DisplayName("simple to string test")
	void userToStringTest() {
		var u = new LatitudeUser(25, "john", "john123");
		assertEquals("latitudeUser [id=25, name=john]", u.toString());
	}
	
	// test hash
	@Test
	@DisplayName("simple hash test")
	void userToHashTest() {
		var u = new LatitudeUser(25, "john", "john123");
		var v = new LatitudeUser(25, "yoyo", "yoyo123");
		assertEquals(u.hashCode(), v.hashCode());
		
		v.setId(26);
		assertNotEquals(u.hashCode(), v.hashCode());	
		
	}
	
	// test equals
	@SuppressWarnings("unlikely-arg-type")
	@Test
	@DisplayName("simple equals test")
	void userToEqualsTest() {
		var u = new LatitudeUser(25, "john", "john123");
		assertTrue(u.equals(u));
		assertFalse(u.equals(null));
		assertFalse(u.equals("hello"));
		var v = new LatitudeUser(25, "yoyo", "yoyo123");
		assertTrue(u.equals(v));
		v.setId(26);
		assertFalse(u.equals(v));
	}
	
	

}




































