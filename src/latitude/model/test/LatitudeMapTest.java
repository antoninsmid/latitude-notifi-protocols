package latitude.model.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.junit.jupiter.api.Test;

import latitude.model.LatitudeMap;
import latitude.model.LatitudeUser;
import mapservice.MapManager;
import mapservice.MemoryMapManager;
import notifi.app.NoTiFiServer;

/**
 * Test for latitude map
 *
 */
class LatitudeMapTest {

	@Test
	void testLatitudeMap() throws IOException {
		
		MapManager mapManager = new MemoryMapManager();
		Map<Long, LatitudeUser> users = new HashMap<>();
		NoTiFiServer noTiFiServer = new NoTiFiServer(5000, Logger.getLogger("testlog"));
		users.put(1L, new LatitudeUser(1L, "john", "john123"));
		var m = new LatitudeMap(20, "mojemapa", mapManager, users, noTiFiServer);
		assertEquals(20, m.getId());
		assertEquals("mojemapa", m.getName());
		m.setName("mojemapa2");
		assertEquals("mojemapa2", m.getName());
		assertNotNull(m.getLibrary());
	}

}
