package latitude.model.test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.junit.jupiter.api.Test;

import latitude.model.LatitudeUser;
import latitude.model.LocationRecordLibrary;
import latitude.serialization.LocationRecord;
import latitude.serialization.ValidationException;
import mapservice.MapManager;
import mapservice.MemoryMapManager;
import notifi.app.NoTiFiServer;

/**
 * Testing the record library
 *
 */
class LocationRecordLibraryTest {

	//test constructor
	@Test
	void testLocationRecordLibrary() throws IOException {
		MapManager mapManager = new MemoryMapManager();
		Map<Long, LatitudeUser> users = new HashMap<>();
		NoTiFiServer noTiFiServer = new NoTiFiServer(5001, Logger.getLogger("testlog"));
		users.put(1L, new LatitudeUser(1L, "john", "john123"));
		new LocationRecordLibrary(mapManager, users, noTiFiServer);
	}

	// test adding and getting records
	@Test
	void testAddLocation() throws ValidationException, IOException {
		MapManager mapManager = new MemoryMapManager();
		Map<Long, LatitudeUser> users = new HashMap<>();
		NoTiFiServer noTiFiServer = new NoTiFiServer(5002, Logger.getLogger("testlog"));
		users.put(1L, new LatitudeUser(1L, "john", "john123"));
		var l = new LocationRecordLibrary(mapManager, users, noTiFiServer);
		LocationRecord lr = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		l.addLocation(lr);
		LocationRecord nlr = l.getLocationRecords().get(0);
		assertEquals(lr.getUserId(), nlr.getUserId());
		assertEquals("john: " + lr.getLocationName(), nlr.getLocationName());
		assertEquals(lr.getLatitude(), nlr.getLatitude());
		assertEquals(lr.getLongitude(), nlr.getLongitude());
		assertEquals(lr.getLocationDescription(), nlr.getLocationDescription());
		lr = new LocationRecord(25, "1.2", "3.4", "BU", "Baylor");
		l.addLocation(lr);	//fails, does not have correct user id
		assertEquals(1, l.getLocationRecords().size());
	}

}
