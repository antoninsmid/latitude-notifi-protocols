/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1
* Class: CSI4321
*
************************************************/
package latitude.serialization;

import java.io.IOException;

import latitude.serialization.util.IOHelper;
import latitude.serialization.util.ValidationHelper;

/**
 * Represents a generic latitude message
 *
 */
public abstract class LatitudeMessage {

	private long mapId;
	private final static String PROTOCOL_HEAD = "LATITUDEv1 "; // Beginning of every encoded message, the space in
																// the end is important.
	private final static String PROTOCOL_HEAD_PREFIX_PART = "LATITUDEv"; // prefix part of the latitude message head.
																			// Used to distinguish between wrong version
																			// and completely wrong header
	public final static String WRONG_HEADER_MESSAGE = "wrong header, LATITUDEv1 not found";
	public final static String WRONG_VERSION = "wrong version";
	public final static String WRONG_TYPE = "the message operation was not recognized";

	protected final static String LATITUDE_NEW = "NEW";
	protected final static String LATITUDE_ALL = "ALL";
	protected final static String LATITUDE_RESPONSE = "RESPONSE";
	protected final static String LATITUDE_ERROR = "ERROR";

	/**
	 * 
	 * Deserializes message from byte source
	 * 
	 * @param in deserialization input source
	 * @return a specific message resulting from deserialization
	 * @throws ValidationException if validation fails
	 * @throws IOException         if I/O problem
	 */
	public static LatitudeMessage decode(MessageInput in) throws ValidationException, IOException {
		
		// check the beginning of message
		String begin = IOHelper.readStringBeforeSpace(in);
		if (!PROTOCOL_HEAD.equals(begin + " ")) {
			if (begin.length() > 9) {
				String prefix = begin.substring(0, 9);
				if (PROTOCOL_HEAD_PREFIX_PART.equals(prefix)) {
					throw new ValidationException(begin.substring(9, begin.length()), WRONG_VERSION);
				}
			}
			throw new ValidationException("message input", WRONG_HEADER_MESSAGE);
		}

		// read map id
		long mapId = IOHelper.readUnsignedInt(in);

		// distinguish the type of operation and choose the corresponding message
		// subtype
		String type = IOHelper.readStringBeforeSpace(in);
		LatitudeMessage message;
		switch (type) {
		case LATITUDE_NEW:
			message = new LatitudeNewLocation(mapId);
			break;
		case LATITUDE_ALL:
			message = new LatitudeLocationRequest(mapId);
			break;
		case LATITUDE_RESPONSE:
			message = new LatitudeLocationResponse(mapId);
			break;
		case LATITUDE_ERROR:
			message = new LatitudeError(mapId);
			break;
		default:
			throw new ValidationException(type, WRONG_TYPE);
		}

		// decode the rest of message at subtype
		message.decodeSpecific(in);
		return message;
	}

	/**
	 * Decodes message specific variables from stream. The super method just
	 * validates the valid end of line, while the particular functionality is
	 * implemented in children, according to their type.
	 * 
	 * @param in message with input stream
	 * @return Specific LatitudeMessage sub-type decoded from stream
	 * @throws ValidationException if validation fails
	 * @throws IOException         if I/O problem
	 */
	protected void decodeSpecific(MessageInput in) throws ValidationException, IOException {
		// do the work in sub-type, then
		// validate the end of message

		IOHelper.readValidateEOLN(in);
	}

	/**
	 * Serializes message to MessageOutput
	 * 
	 * @param out serialization output destination
	 * @throws IOException if I/O problem
	 */
	public void encode(MessageOutput out) throws IOException {
		if (out == null) {
			throw new NullPointerException("encode output can not be null");
		}
		IOHelper.writeBytesToMessage(IOHelper.getStringNoEncode(PROTOCOL_HEAD), out);
		IOHelper.writeEncodedUnsignedIntegerToMessage(mapId, out);
		out.getStream().flush();
	}

	/**
	 * Returns map ID
	 * 
	 * @return map ID
	 */
	public long getMapId() {
		return mapId;
	}

	/**
	 * Sets map ID
	 * 
	 * @param mapId new map ID
	 * @throws ValidationException if validation fails
	 */
	public void setMapId(long mapId) throws ValidationException {
		ValidationHelper.validateUnsignedInt(mapId);
		this.mapId = mapId;
	}

	/**
	 * returns the operation represented by the message
	 * 
	 * @return operation code
	 */
	public abstract String getOperation();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (mapId ^ (mapId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LatitudeMessage other = (LatitudeMessage) obj;
		if (mapId != other.mapId)
			return false;
		return true;
	}

}
