/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1 test
* Class: CSI4321
*
************************************************/
package latitude.serialization.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.jupiter.api.DisplayName;

import latitude.serialization.LatitudeError;
import latitude.serialization.LatitudeMessage;
import latitude.serialization.LatitudeNewLocation;
import latitude.serialization.LocationRecord;
import latitude.serialization.MessageInput;
import latitude.serialization.MessageOutput;
import latitude.serialization.ValidationException;

/**
 * Test class for New Location Messages.
 *
 */
class LatitudeNewLocationTest {

	private static final String CHARENC = "ASCII";

	// tests constructor
	@Test
	@DisplayName("constructor test valid")
	void constructorValidTest() throws ValidationException {

		LocationRecord lrs[] = new LocationRecord[2];
		lrs[0] = new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there ");
		lrs[1] = new LocationRecord(3_000_000_000L, "180.0", "-90.0", "whats", "up");

		for (int i = 0; i < lrs.length; i++) {
			LatitudeNewLocation lnl = new LatitudeNewLocation(25, lrs[i]);
			assertEquals(lnl.getMapId(), 25);
			assertEquals(lnl.getLocationRecord(), lrs[i]);
		}
	}

	// tests constructor invalid
	@Test
	@DisplayName("constructor test invalid")
	void constructorInvalidTest() throws ValidationException {

		LocationRecord lrs[] = new LocationRecord[2];
		lrs[0] = new LocationRecord();
		lrs[1] = null;

		for (int i = 0; i < lrs.length; i++) {
			LocationRecord l = lrs[i];
			assertThrows(ValidationException.class, () -> {
				new LatitudeNewLocation(25, l);
			});
		}

		var l = new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there ");
		assertThrows(ValidationException.class, () -> {
			new LatitudeNewLocation(-5, l);
		});
		assertThrows(ValidationException.class, () -> {
			new LatitudeNewLocation(6_000_000_000L, l);
		});

	}
	

	// test setter valid
	@Test
	@DisplayName("location record setter valid")
	void lrSetterValidTest() throws ValidationException {

		LocationRecord lrs[] = new LocationRecord[2];
		var l = new LocationRecord(5, "-2.5", "50.0", "there", "hello");
		lrs[0] = new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there ");
		lrs[1] = new LocationRecord(3_000_000_000L, "180.0", "-90.0", "whats", "up");

		for (int i = 0; i < lrs.length; i++) {
			LatitudeNewLocation lnl = new LatitudeNewLocation(25, l);
			lnl.setLocationRecord(lrs[i]);
			assertEquals(lnl.getMapId(), 25);
			assertEquals(lnl.getLocationRecord(), lrs[i]);
		}
	}

	// test setter invalid
	@Test
	@DisplayName("location record setter invalid")
	void lrSetterInvalidTest() throws ValidationException {

		LocationRecord lrs[] = new LocationRecord[2];
		lrs[0] = new LocationRecord();
		lrs[1] = null;
		var lr = new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there ");

		for (int i = 0; i < lrs.length; i++) {
			LocationRecord lproblem = lrs[i];
			assertThrows(ValidationException.class, () -> {
				var testLR = new LatitudeNewLocation(25, lr);
				testLR.setLocationRecord(lproblem);
			});
		}
	}

	// testEncode
	@Test
	@DisplayName("test encode valid")
	void encodeTest() throws ValidationException, IOException {

		LocationRecord lrs[] = new LocationRecord[2];
		lrs[0] = new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there ");
		lrs[1] = new LocationRecord(3_000_000_000L, "180.0", "-90.0", "whats", "up");
		for (int i = 0; i < lrs.length; i++) {
			var lnl = new LatitudeNewLocation(25, lrs[i]);
			var stream = new ByteArrayOutputStream();
			var out = new MessageOutput(stream);
			lnl.encode(out);
			assertArrayEquals(("LATITUDEv1 25 NEW " + lrs[i].getUserId() + " " + lrs[i].getLongitude() + " "
					+ lrs[i].getLatitude() + " " + lrs[i].getLocationName().length() + " " + lrs[i].getLocationName()
					+ +lrs[i].getLocationDescription().length() + " " + lrs[i].getLocationDescription() + "\r\n")
							.getBytes(CHARENC),
					stream.toByteArray());
		}
	}

	// testEncode with IO exception
	@Test
	@DisplayName("test encode IO exception")
	void encodeTestIOException() throws IOException, ValidationException {
		var lr = new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there ");
		var lnl = new LatitudeNewLocation(25, lr);
		MessageOutput out = new MessageOutput(new TestIOOutputStream());
		assertThrows(IOException.class, () -> {
			lnl.encode(out);

		});	
	}
	
	// testEncode with null exception
	@Test
	@DisplayName("test encode null exception")
	void encodeTestNullException() throws IOException, ValidationException {
		var lr = new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there ");
		var lnl = new LatitudeNewLocation(25, lr);
		assertThrows(NullPointerException.class, () -> {
			lnl.encode(null);
		});	
	}

	// testDecode
	@Test
	@DisplayName("decode valid")
	void decodeValidTest() throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(
				"LATITUDEv1 42 NEW 3000000000 180.0 -90.0 4 here5 there\r\n".getBytes(CHARENC));
		var messageInput = new MessageInput(stream);
		var message = LatitudeMessage.decode(messageInput);
		assertEquals(message.getClass().getName(), LatitudeNewLocation.class.getName());
		assertEquals(((LatitudeNewLocation) message).getMapId(), 42);
		var lr = ((LatitudeNewLocation) message).getLocationRecord();
		assertEquals(lr.getUserId(), 3000000000L);
		assertEquals(lr.getLongitude(), "180.0");
		assertEquals(lr.getLatitude(), "-90.0");
		assertEquals(lr.getLocationName(), "here");
		assertEquals(lr.getLocationDescription(), "there");
	}

	// test decode invalid
	@ParameterizedTest
	@ValueSource(strings = { "LATITUDEv2 42 NEW 3000000000 180.0 -90.0 4 here5 there\r\n",
			"LATITUDEv1 -42 NEW 3000000000 180.0 -90.0 4 here5 there\r\n",
			"LATITUDEv1 42 NEW 6000000000 180.0 -90.0 4 here5 there\r\n",
			"LATITUDEv1 42 NEW 3000000000 181.0 -90.0 4 here5 there\r\n",
			"LATITUDEv1 42 NEW 3000000000 180.0 -90. 4 here5 there\\r\\n",
			"LATITUDEv1 -42 NEW 3000000000 180.0 -91.0 4 here5 there\r\n" })
	@DisplayName("decode invalid")
	void decodeInvalidTest(String param) throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(param.getBytes(CHARENC));
		var messageInput = new MessageInput(stream);
		assertThrows(ValidationException.class, () -> {
			LatitudeMessage.decode(messageInput);
		});
	}

	// decode invalid chars
	@ParameterizedTest
	@ValueSource(strings = { "LATITUDEv1 42 NEW 3000000000 180.0 -90.0 9 ěščřžýáíé5 there\r\n" })
	@DisplayName("decode invalid special chars")
	void decodeInvalidTest2(String param) throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(param.getBytes());
		var messageInput = new MessageInput(stream);
		assertThrows(ValidationException.class, () -> {
			LatitudeMessage.decode(messageInput);
		});
	}
	
	// test decode io exception
	@ParameterizedTest
	@ValueSource(strings = { "LATITUDEv1 42 NEW 3000000000 180.0 -90.0 4 here5 there\r",
			"LATITUDEv1 42 NEW 3000000000 180.0 -90.0 4 here5 there",
			"LATITUDEv1 42 NEW 3000000000 180.0 -90.0 4 here5 th", "LATITUDEv1 42 NEW 3000000000 180.0 -90.0 4",
			"LATITUDEv1 42 NEW 3000000000 18.0", "LATITUDEv1 42 NEW 300", "LATITUDEv1 42 NEW", "LATITUDEv1 42 ",
			"LATITUDEv1" })
	@DisplayName("decode io exception")
	void decodeIOExTest(String param) throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(param.getBytes(CHARENC));
		var messageInput = new MessageInput(stream);
		assertThrows(IOException.class, () -> {
			LatitudeMessage.decode(messageInput);
		});
	}
	
	
	@Test
	@DisplayName("to string test for no reason")
	public void testToString() throws ValidationException {		
		var lr = new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there ");
		var lnl = new LatitudeNewLocation(25, lr);
		assertEquals("LatitudeNewLocation [locationRecord=id: 3 name: heredesc: hello ! there  lon: 2.5 lat: -50.0, MapId=25]", lnl.toString());
	}
	
	
	@Test
	@DisplayName("hashcode test for no reason")
	public void testHash() throws ValidationException {		
		var m = new LatitudeNewLocation(26, new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there "));
		var n = new LatitudeNewLocation(26, new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there "));
		assertEquals(m.hashCode(), n.hashCode());
		n.setMapId(27);
		assertNotEquals(m.hashCode(), n.hashCode());
		n.setMapId(26);
		n.makeLRNull();
		assertNotEquals(m.hashCode(), n.hashCode());
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	@DisplayName("equals test for no reason")
	public void testEquals() throws ValidationException {		
		var m = new LatitudeNewLocation(26, new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there "));
		assertTrue(m.equals(m));
		var n = new LatitudeNewLocation(26, new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there "));
		assertTrue(m.equals(n));
		
		assertFalse(m.equals(new LatitudeError(25, "msg")));
			
		m.makeLRNull();
		assertFalse(m.equals(n));
		
		n.makeLRNull();
		assertTrue(m.equals(n));

		m = new LatitudeNewLocation(26, new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there "));
		n = new LatitudeNewLocation(26, new LocationRecord(4, "2.5", "-50.0", "here", "hello ! there "));
		assertFalse(m.equals(n));
		
	}
	
	@Test
	@DisplayName("operation")
	public void testOperation() throws ValidationException{		
		var m = new LatitudeNewLocation(26, new LocationRecord(3, "2.5", "-50.0", "here", "hello ! there "));
		assertEquals("NEW ", m.getOperation());
	}
	

}





















