/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1 test
* Class: CSI4321
*
************************************************/
package latitude.serialization.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import latitude.serialization.LatitudeLocationRequest;
import latitude.serialization.LatitudeMessage;
import latitude.serialization.MessageInput;
import latitude.serialization.MessageOutput;
import latitude.serialization.ValidationException;

/**
 * Test class for Location Request.
 *
 */
class LatitudeLocationRequestTest {

	private static final String CHARENC = "ASCII";

//	// tests correct map ids
	@ParameterizedTest
	@ValueSource(longs = { 0, 250, 3000000000L })
	@DisplayName("mapid setter test valid")
	void mapIdSetterValidTest(long param) throws ValidationException {
		var m = new LatitudeLocationRequest(345);
		m.setMapId(param);
		assertEquals(param, m.getMapId());
	}

	// tests invalid map ids
	@ParameterizedTest
	@ValueSource(longs = { -1, -250, -3000000000L, 6000000000L, -6000000000L })
	@DisplayName("mapid setter test invalid")
	void mapIdSetterInvalidTest(long param) throws ValidationException {
		var m = new LatitudeLocationRequest(345);
		assertThrows(ValidationException.class, () -> {
			m.setMapId(param);
		});
	}

	// testEncode
	@ParameterizedTest
	@ValueSource(longs = { 0, 250, 3000000000L })
	@DisplayName("simple encode")
	void encodeSimpleTest(long param) throws ValidationException, IOException {
		var m = new LatitudeLocationRequest(param);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		MessageOutput out = new MessageOutput(stream);
		m.encode(out);
		assertArrayEquals(("LATITUDEv1 " + param + " ALL \r\n").getBytes(CHARENC), stream.toByteArray());
	}

	// testDecode
	@Test
	@DisplayName("decode valid")
	void decodeValidTest() throws ValidationException, IOException {
		var stream = new ByteArrayInputStream("LATITUDEv1 42 ALL \r\n".getBytes(CHARENC));
		var messageInput = new MessageInput(stream);
		var message = LatitudeMessage.decode(messageInput);
		assertEquals(message.getClass().getName(), LatitudeLocationRequest.class.getName());
		assertEquals(((LatitudeLocationRequest) message).getMapId(), 42);
	}

//	//test decode invalid
	@ParameterizedTest
	@ValueSource(strings = { "LATITUDEv2 42 ALL \r\n", "LATITUDEv1 42 RANDOM \r\n", "LATITUDEv1 -42 ALL \r\n",
			"LATITUDEv1 6000000000 ALL \r\n", "LATITUDEv1 42 RANDOM \r", "LATITUDEv1 42 RAN", "LATITUDEv2 ",
			"LATITOUDEv1", "LATITUDEv25" })
	@DisplayName("decode invalid")
	void decodeInvalidTest(String param) throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(param.getBytes(CHARENC));
		var messageInput = new MessageInput(stream);
		assertThrows(ValidationException.class, () -> {
			LatitudeMessage.decode(messageInput);
		});
	}

	// test decode io exception
	@ParameterizedTest
	@ValueSource(strings = { "", "LATITUDEv1", "LATITUDEv1 " })
	@DisplayName("decode io exception")
	void decodeIOExTest(String param) throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(param.getBytes(CHARENC));
		var messageInput = new MessageInput(stream);
		assertThrows(IOException.class, () -> {
			LatitudeMessage.decode(messageInput);
		});
	}

	@Test
	@DisplayName("to string test for no reason")
	public void testToString() throws ValidationException {
		var m = new LatitudeLocationRequest(26);
		assertEquals("LatitudeLocationRequest [mapId=26]", m.toString());
	}

	@Test
	@DisplayName("operation")
	public void testOperation() throws ValidationException {
		var m = new LatitudeLocationRequest(26);
		assertEquals("ALL ", m.getOperation());
	}

}
