/************************************************
*
* Author: Antonin Smid
* Assignment: Program 0 Test
* Class: CSI4321
*
************************************************/

package latitude.serialization.test;


import static org.junit.jupiter.api.Assertions.assertThrows;
import java.io.ByteArrayOutputStream;
import org.junit.jupiter.api.Test;
import latitude.serialization.MessageOutput;

/**
 * Class for simple test of message output.
 */
class MessageOutputTest {

        
        /**
         * Basic constructor test
         */
        @Test
        void constructMessagOutputTest() {
            
            new MessageOutput(new ByteArrayOutputStream()); 
            
            assertThrows(NullPointerException.class, ()->{
                new MessageOutput(null);
            });       
            
        }


}
