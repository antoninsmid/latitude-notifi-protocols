/************************************************
*
* Author: Antonin Smid
* Assignment: Program 0 Test
* Class: CSI4321
*
************************************************/

package latitude.serialization.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import latitude.serialization.ValidationException;

/**
 * Class testing the validationException.
 */
class ValidationExceptionTest {

    @Test
    void constructorShortTest() {

        ValidationException ex = new ValidationException("ahoj", "ahoj, something went wrong.");
        assertEquals("ahoj, something went wrong.", ex.getMessage());

    }

    @Test
    void constructorLongTest() {
        ValidationException ex2 = new ValidationException("ahoj", "ahoj, something went wrong.",
                new NullPointerException("its null"));
        assertEquals("ahoj, something went wrong.", ex2.getMessage());
        assertEquals("ahoj", ex2.getInvalidToken());
        assertNotNull(ex2.getCause());
        assertEquals(new NullPointerException().getClass(), ex2.getCause().getClass());
        assertEquals("its null", ex2.getCause().getMessage());
    }
    
    @Test
    void getTokenTest() {
        ValidationException ex = new ValidationException("greeting", "ahoj, something went wrong.");
        assertEquals("greeting", ex.getInvalidToken());        
    }

}
