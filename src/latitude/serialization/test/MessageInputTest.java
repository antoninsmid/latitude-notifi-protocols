/************************************************
*
* Author: Antonin Smid
* Assignment: Program 0 Test
* Class: CSI4321
*
************************************************/

package latitude.serialization.test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import java.io.ByteArrayInputStream;
import org.junit.jupiter.api.Test;

import latitude.serialization.MessageInput;

/**
 * Class for simple test of message input.
 */
class MessageInputTest {

    /**
     * Basic constructor test
     */
    @Test
    void constructMessagInputTest() {

        byte[] buf = { 42, 42, 42 };
        new MessageInput(new ByteArrayInputStream(buf));

        assertThrows(NullPointerException.class, () -> {
            new MessageInput(null);
        });

    }

}
