/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1 test
* Class: CSI4321
*
************************************************/
package latitude.serialization.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import latitude.serialization.LatitudeLocationResponse;
import latitude.serialization.LatitudeMessage;
import latitude.serialization.LocationRecord;
import latitude.serialization.MessageInput;
import latitude.serialization.MessageOutput;
import latitude.serialization.ValidationException;

/**
 * test class for Location Response.
 *
 */
class LatitudeLocationResponseTest {

	private static final String CHARENC = "ASCII";

	// test constructor
	@ParameterizedTest
	@ValueSource(strings = { "a", "ahoj", "ahoj 228 !!!"})
	@DisplayName("constructor test valid")
	void locationResponseConstructorValidTest(String param) throws ValidationException {
		var m = new LatitudeLocationResponse(3_000_000_000L, param);
		assertEquals(param, m.getMapName());
	}

	// tests constructor invalid values
	@Test
	@DisplayName("constructor test invalid")
	void locationResponseConstructorInvalidTest() throws ValidationException {
		assertThrows(ValidationException.class, () -> {
			new LatitudeLocationResponse(42, "ěščřžýáíé");
		});
		assertThrows(ValidationException.class, () -> {
			new LatitudeLocationResponse(42, null);
		});
		assertThrows(ValidationException.class, () -> {
			new LatitudeLocationResponse(-5, "a");
		});
		assertThrows(ValidationException.class, () -> {
			new LatitudeLocationResponse(6_000_000_000L, "a");
		});
	}

	// test mapName setter valid
	@ParameterizedTest
	@ValueSource(strings = { "a", "ahoj", "ahoj 228 !!!" })
	@DisplayName("mapname setter test valid")
	void locationResponseMapNameValidTest(String param) throws ValidationException {
		var m = new LatitudeLocationResponse(3_000_000_000L, "ahojMap");
		m.setMapName(param);
		assertEquals(param, m.getMapName());
	}

	// test mapName setter invalid
	@Test
	@DisplayName("mapname setter test invalid")
	void locationResponseMapNameInvalidTest() throws ValidationException {
		assertThrows(ValidationException.class, () -> {
			var lr = new LatitudeLocationResponse(42, "ahojMap");
			lr.setMapName("ěščřžýáíé");
		});
		assertThrows(ValidationException.class, () -> {
			var lr = new LatitudeLocationResponse(42, "ahojMap");
			lr.setMapName(null);
			new LatitudeLocationResponse(42, null);
		});
		
		assertThrows(ValidationException.class, () -> {
			var lr = new LatitudeLocationResponse(42, "");
			lr.setMapName(null);
			new LatitudeLocationResponse(42, null);
		});

	}

	// test adding location records valid
	@Test
	@DisplayName("add/get location record test valid")
	void addLocationRecordTest() throws ValidationException {
		var response = new LatitudeLocationResponse(42, "ahojMap");
		List<LocationRecord> lrs = new ArrayList<>();
		lrs.add(new LocationRecord(1, "1.2", "3.4", "BU", "Baylor"));
		lrs.add(new LocationRecord(28, "8.2", "-23.4", "BUBUBU", "BaylorBears"));

		for (int i = 0; i < lrs.size(); i++) {
			response.addLocationRecord(lrs.get(i));
		}

		List<LocationRecord> check = response.getLocationRecordList();
		for (int i = 0; i < lrs.size(); i++) {
			assertTrue(check.contains(lrs.get(i)));
		}
	}
	
	// test adding location records invalid
	@Test
	@DisplayName("add/get location record test invalid")
	void addLocationRecordInvalidTest() throws ValidationException {
		var response = new LatitudeLocationResponse(42, "ahojMap");
		var lr = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		lr.makeNull(true, true, false, true);
		response.forceAddLocationRecord(lr);
		response.getLocationRecordList();			
		
	}

	// testEncode
	@Test
	@DisplayName("simple encode")
	void encodeSimpleTest() throws ValidationException, IOException {
		var response = new LatitudeLocationResponse(42, "ahoj");
		List<LocationRecord> lrs = new ArrayList<>();
		lrs.add(new LocationRecord(1, "1.2", "3.4", "BU", "Baylor"));
		lrs.add(new LocationRecord(28, "8.2", "-23.4", "BUBUBU", "BaylorBears"));

		for (int i = 0; i < lrs.size(); i++) {
			response.addLocationRecord(lrs.get(i));
		}
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		MessageOutput out = new MessageOutput(stream);
		response.encode(out);
		assertArrayEquals(
				("LATITUDEv1 42 RESPONSE 4 ahoj2 1 1.2 3.4 2 BU6 Baylor28 8.2 -23.4 6 BUBUBU11 BaylorBears\r\n")
						.getBytes(CHARENC),
				stream.toByteArray());
	}

	// testEncode IO exception
	@Test
	@DisplayName("simple encode IO exception error")
	void encodeIOExceptionTest() throws ValidationException, IOException {
		var response = new LatitudeLocationResponse(42, "ahoj");
		List<LocationRecord> lrs = new ArrayList<>();
		lrs.add(new LocationRecord(1, "1.2", "3.4", "BU", "Baylor"));

		for (int i = 0; i < lrs.size(); i++) {
			response.addLocationRecord(lrs.get(i));
		}
		MessageOutput out = new MessageOutput(new TestIOOutputStream());
		assertThrows(IOException.class, () -> {
			response.encode(out);
		});
	}

	// testDecode
	@Test
	@DisplayName("decode valid")
	void decodeValidTest() throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(
				"LATITUDEv1 42 RESPONSE 5 mymap2 3000000000 180.0 -90.0 4 here5 there42 18.0 -9.0 5 here16 there1\r\n"
						.getBytes(CHARENC));
		var messageInput = new MessageInput(stream);
		var message = LatitudeMessage.decode(messageInput);
		assertEquals(message.getClass().getName(), LatitudeLocationResponse.class.getName());
		assertEquals(((LatitudeLocationResponse) message).getMapId(), 42);
		var lr = ((LatitudeLocationResponse) message).getLocationRecordList();
		assertEquals(lr.get(0).getUserId(), 3000000000L);
		assertEquals(lr.get(0).getLongitude(), "180.0");
		assertEquals(lr.get(0).getLatitude(), "-90.0");
		assertEquals(lr.get(0).getLocationName(), "here");
		assertEquals(lr.get(0).getLocationDescription(), "there");
		assertEquals(lr.get(1).getUserId(), 42);
		assertEquals(lr.get(1).getLongitude(), "18.0");
		assertEquals(lr.get(1).getLatitude(), "-9.0");
		assertEquals(lr.get(1).getLocationName(), "here1");
		assertEquals(lr.get(1).getLocationDescription(), "there1");
	}

	// test decode invalid
	@ParameterizedTest
	@ValueSource(strings = { "LATITUDEv1 -42 RESPONSE 5 mymap1 3000000000 180.0 -90.0 4 here5 there\r\n",
			"LATITUDEv2 42 RESPONSE 5 mymap2 3000000000 180.0 -90.0 4 here5 there42 18.0 -9.0 5 here16 there1\r\n",
			"LATITUDEv1 42 RESPONSE 5 mymap-1 3000000000 180.0 -90.0 4 here5 there42 18.0 -9.0 5 here16 there1\r\n",
			"LATITUDEv1 -42 RESPONSE 5 mymap1 3000000000 180.0 -91.0 4 here5 there\r\n" })
	@DisplayName("decode invalid")
	void decodeInvalidTest(String param) throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(param.getBytes(CHARENC));
		var messageInput = new MessageInput(stream);
		assertThrows(ValidationException.class, () -> {
			LatitudeMessage.decode(messageInput);
		});
	}

	// decode invalid chars
	@ParameterizedTest
	@ValueSource(strings = { 
			"LATITUDEv1 42 RESPONSE 9 ěščřžýáíé 5 3000000000 180.0 -90.0 2 bu there\r\n",
			"LATITUDEv1 42 RESPONSE 5 mymap1 6000000000 180.0 -90.0 4 here5 there\r\n",
			"LATITUDEv1 42 RESPONSE 5 mymap1 3000000000 181.0 -90.0 4 here5 there\r\n",
			"LATITUDEv1 42 RESPONSE 5 mymap1 3000000000 180.0 -90. 4 here5 there\\r\\n"		
	})
	@DisplayName("decode invalid special chars")
	void decodeInvalidTest2(String param) throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(param.getBytes());
		var messageInput = new MessageInput(stream);
		assertThrows(ValidationException.class, () -> {
			LatitudeMessage.decode(messageInput);
		});
	}
	
	// decode invalid chars
	@ParameterizedTest
	@ValueSource(strings = { 
			"LATITUDEv1 ěščřžýáíé",
			"LATITUDEv1 \r\n "
	})
	@DisplayName("decode invalid special chars")
	void decodeInvalidTest3(String param) throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(param.getBytes());
		var messageInput = new MessageInput(stream);
		assertThrows(IOException.class, () -> {
			LatitudeMessage.decode(messageInput);
		});
	}

	// test decode io exception
	@ParameterizedTest
	@ValueSource(strings = {
			"LATITUDEv1 42 RESPONSE 5 mymap2 3000000000 180.0 -90.0 4 here5 there42 18.0 -9.0 5 here16 there1\r",
			"LATITUDEv1 42 RESPONSE 5 mymap2 3000000000 180.0 -90.0 4 here5 there42 18.0 -9.0 5 here16 t",
			"LATITUDEv1 42 RESPONSE 5 mymap2 3000000000 180.0 -90.0 4 here5 there42 18.0 -9.0 5 her",
			"LATITUDEv1 42 RESPONSE 5 mymap2 3000000000 180.0 -90.0 4 here5 there42 18.0 ",
			"LATITUDEv1 42 RESPONSE 5 mymap2 3000000000 180.0 -90.0 4 here5 there",
			"LATITUDEv1 42 RESPONSE 5 mymap2 3000000000 180.0 -90.0 4 here5 \\r ",
			"LATITUDEv1 42 RESPONSE 5 mymap2 3000000000 180.0 -90.0 4 here5 \\n ",
			"LATITUDEv1 5 RESPONSE 2 XY3 1 -5.3 9.7 3 NAM5 Des",
			"LATITUDEv1 5 RESPONSE 2 XY3 1 -5.3 9.7 3 NAM5 Des p",
			"LATITUDEv1 \r ",
			"LATITUDEv1 \n "	
			})
	@DisplayName("decode io exception")
	void decodeIOExTest(String param) throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(param.getBytes(CHARENC));
		var messageInput = new MessageInput(stream);
		assertThrows(IOException.class, () -> {
			LatitudeMessage.decode(messageInput);
		});
	}
	
	@Test
	@DisplayName("to string test for no reason")
	public void testToString() throws ValidationException {		
		var m = new LatitudeLocationResponse(26, "hungry map");
		assertEquals("LatitudeLocationResponse [mapName=hungry map, mapId=26, locationRecordList=[]]", m.toString());
	}
	
	@Test
	@DisplayName("hashcode test for no reason")
	public void testHash() throws ValidationException {		
		var m = new LatitudeLocationResponse(26, "tired map");
		var n = new LatitudeLocationResponse(26, "tired map");
		assertEquals(m.hashCode(), n.hashCode());
		n.setMapId(27);
		assertNotEquals(m.hashCode(), n.hashCode());
		n.setMapId(26);
		n.setMapNull();
		assertNotEquals(m.hashCode(), n.hashCode());
		n.setMapId(26);
		n.setLocationRecordListNull();	
		assertNotEquals(m.hashCode(), n.hashCode());
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	@DisplayName("equals test for no reason")
	public void testEquals() throws ValidationException {		
		var m = new LatitudeLocationResponse(26, "tired map");
		assertTrue(m.equals(m));
		var n = new LatitudeLocationResponse(26, "tired map");
		assertTrue(m.equals(n));
		
		assertFalse(m.equals(null));
		assertFalse(m.equals("ahoj"));
		
		m.setLocationRecordListNull();	
		assertFalse(m.equals(n));
		n.setLocationRecordListNull();
		assertTrue(m.equals(n));
		
		m = new LatitudeLocationResponse(26, "tired map");
		n = new LatitudeLocationResponse(26, "tired map");
		n.addLocationRecord(new LocationRecord(1, "1.2", "3.4", "BU", "Baylor"));
		assertFalse(m.equals(n));
		
		n = new LatitudeLocationResponse(26, "tired map");
		
		m.setMapNull();
		assertFalse(m.equals(n));
		n.setMapNull();
		assertTrue(m.equals(n));
		
		m = new LatitudeLocationResponse(26, "tired map");
		n = new LatitudeLocationResponse(26, "really tired map");	
		assertFalse(m.equals(n));
	}
	
	@Test
	@DisplayName("operation")
	public void testOperation() throws ValidationException{		
		var m = new LatitudeLocationResponse(26, "tired map");
		assertEquals("RESPONSE ", m.getOperation());
	}
	
}































