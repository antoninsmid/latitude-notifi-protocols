/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1 test
* Class: CSI4321
*
************************************************/
package latitude.serialization.test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Mock input stream class used to force IO exception during reading
 */
class TestIOInputStream extends InputStream {

	@Override
	public int read() throws IOException {
		throw new IOException();
	}

	@Override
	public int read(byte[] b) throws IOException {
		throw new IOException();
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		throw new IOException();
	}

}
