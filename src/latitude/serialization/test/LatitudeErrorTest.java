/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1 test
* Class: CSI4321
*
************************************************/
package latitude.serialization.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import latitude.serialization.LatitudeError;
import latitude.serialization.LatitudeLocationRequest;
import latitude.serialization.LatitudeMessage;
import latitude.serialization.MessageInput;
import latitude.serialization.MessageOutput;
import latitude.serialization.ValidationException;

/**
 * Test class for LatitudeError.
 *
 */
class LatitudeErrorTest {

	private static final String CHARENC = "ASCII";

	// tests constructor
	@ParameterizedTest
	@ValueSource(strings = { "a", "ahoj", "ahoj 228 !!!", "" })
	@DisplayName("error setter test valid")
	void errorMessageValidTest(String param) throws ValidationException {
		var m = new LatitudeError(42, param);
		assertEquals(param, m.getErrorMessage());
	}

	// tests constructor invalid values
	@Test
	@DisplayName("error setter test invalid")
	void errorMessageInvalidTest() throws ValidationException {
		assertThrows(ValidationException.class, () -> {
			new LatitudeError(42, "ěščřžýáíé");
		});
		assertThrows(ValidationException.class, () -> {
			new LatitudeError(42, null);
		});
	}

	// testEncode
	@ParameterizedTest
	@ValueSource(strings = { "a", "ahoj", "ahoj 228 !!!", "" })
	@DisplayName("simple encode")
	void encodeSimpleTest(String param) throws ValidationException, IOException {
		var m = new LatitudeError(42, param);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		MessageOutput out = new MessageOutput(stream);
		m.encode(out);
		assertArrayEquals(("LATITUDEv1 42 ERROR " + param.length() + " " + param + "\r\n").getBytes(CHARENC),
				stream.toByteArray());
	}

	// testDecode
	@Test
	@DisplayName("decode valid")
	void decodeValidTest() throws ValidationException, IOException {
		var stream = new ByteArrayInputStream("LATITUDEv1 42 ERROR 12 random fail \r\n".getBytes(CHARENC));
		var messageInput = new MessageInput(stream);
		var message = LatitudeMessage.decode(messageInput);
		assertEquals(message.getClass().getName(), LatitudeError.class.getName());
		assertEquals(((LatitudeError) message).getMapId(), 42);
		assertEquals(((LatitudeError) message).getErrorMessage(), "random fail ");
	}

	// test decode invalid
	@ParameterizedTest
	@ValueSource(strings = { "LATITUDEv1 -42 ERROR 4 fail\r\n", "LATITUDEv1 6000000000 ERROR 4 fail\r\n" })
	@DisplayName("decode invalid")
	void decodeInvalidTest(String param) throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(param.getBytes(CHARENC));
		var messageInput = new MessageInput(stream);
		assertThrows(ValidationException.class, () -> {
			LatitudeMessage.decode(messageInput);
		});
	}

	// test decode invalid special
	@ParameterizedTest
	@ValueSource(strings = { "LATITUDEv1 345 ERROR 2 GoLATITUDEv1 345 ALL ", "LATITUDEv2 42 ERROR 4 fail\r\n" })
	@DisplayName("decode invalid special")
	void decodeInvalidTestSpecial(String param) throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(param.getBytes(CHARENC));
		var messageInput = new MessageInput(stream);
		assertThrows(ValidationException.class, () -> {
			LatitudeMessage.decode(messageInput);
		});
	}

	@ParameterizedTest
	@ValueSource(strings = { "LATITUDEv1 42 ERROR 9 ěščřžýáíé\r\n" })
	@DisplayName("decode invalid special chars")
	void decodeInvalidTest2(String param) throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(param.getBytes());
		var messageInput = new MessageInput(stream);
		assertThrows(ValidationException.class, () -> {
			LatitudeMessage.decode(messageInput);
		});
	}

	// test decode io exception
	@ParameterizedTest
	@ValueSource(strings = { "LATITUDEv1 42 ERROR 3 fail\r\n", "LATITUDEv1 42 ERROR 6 fail\r\n",
			"LATITUDEv1 42 ERROR 12 fail\r\n", "LATITUDEv1 42 ERROR 4 fail\r", "LATITUDEv1 42 ERROR 4 fail",
			"LATITUDEv1 42 ERROR 4 ", "LATITUDEv1 42 ERROR 4", "LATITUDEv1 42 ERROR", "LATITUDEv1 42 ", "LATITUDEv1 4",
			"LATITUDEv1" })
	@DisplayName("decode io exception")
	void decodeIOExTest(String param) throws ValidationException, IOException {
		var stream = new ByteArrayInputStream(param.getBytes(CHARENC));
		var messageInput = new MessageInput(stream);
		assertThrows(IOException.class, () -> {
			LatitudeMessage.decode(messageInput);
		});
	}
	
	@Test
	@DisplayName("to string test for no reason")
	public void testToString() throws ValidationException {		
		var m = new LatitudeError(26, "unnecessary test count exceeded.");
		assertEquals("LatitudeError [errorMessage=unnecessary test count exceeded., mapId=26]", m.toString());
	}

	@Test
	@DisplayName("hashcode test for no reason")
	public void testHash() throws ValidationException {		
		var m = new LatitudeError(26, "unnecessary test count exceeded.");
		var n = new LatitudeError(26, "unnecessary test count exceeded.");
		assertEquals(m.hashCode(), n.hashCode());
		n.setMapId(27);
		assertNotEquals(m.hashCode(), n.hashCode());
		n.setMapId(26);
		n.setErrorMessage("unnecessary test count exceeded twice");
		assertNotEquals(m.hashCode(), n.hashCode());	
		n.makeNull();
		assertNotEquals(m.hashCode(), n.hashCode());	
		 n = new LatitudeError(26, "unnecessary test count exceeded.");
	}

	@Test
	@DisplayName("equals test for no reason")
	public void testEquals() throws ValidationException {		
		var m = new LatitudeError(25, "unnecessary test count exceeded.");
		assertTrue(m.equals(m));
		m.setMapId(25);
		var n = new LatitudeError(25, "unnecessary test count exceeded.");
		assertTrue(m.equals(n));
		
		assertFalse(m.equals(null));
		assertFalse(m.equals(new LatitudeLocationRequest(345)));
		n.setMapId(26);
		assertFalse(m.equals(n));
		n.setMapId(25);
		n.setErrorMessage("unnecessary test count exceeded twice.");
		assertFalse(m.equals(n));
		n.setErrorMessage("unnecessary test count exceeded.");
		m.makeNull();
		assertFalse(m.equals(n));
		n.makeNull();
		assertTrue(m.equals(n));
	}
	
	@Test
	@DisplayName("operation")
	public void testOperation() throws ValidationException{		
		var m = new LatitudeError(25, "unnecessary test count exceeded.");
		assertEquals("ERROR ", m.getOperation());
	}
	
}




































