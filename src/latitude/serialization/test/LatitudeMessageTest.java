/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1 test
* Class: CSI4321
*
************************************************/
package latitude.serialization.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import latitude.serialization.LatitudeLocationRequest;
import latitude.serialization.MessageOutput;
import latitude.serialization.ValidationException;

/**
 * Test class for generic Latitude Message.
 *
 */
class LatitudeMessageTest {

	private static final String CHARENC = "ASCII";

	// tests correct map ids
	@ParameterizedTest
	@ValueSource(longs = { 0, 250, 3000000000L })
	@DisplayName("mapid setter test valid")
	void mapIdSetterValidTest(long param) throws ValidationException {
		var m = new LatitudeLocationRequest(345);
		m.setMapId(param);
		assertEquals(param, m.getMapId());
	}

	// tests invalid map ids
	@ParameterizedTest
	@ValueSource(longs = { -1, -250, -3000000000L, 6000000000L, -6000000000L })
	@DisplayName("mapid setter test invalid")
	void mapIdSetterInvalidTest(long param) throws ValidationException {
		var m = new LatitudeLocationRequest(345);
		assertThrows(ValidationException.class, () -> {
			m.setMapId(param);
		});
	}

	// testEncode
	@ParameterizedTest
	@ValueSource(longs = { 0, 250, 3000000000L })
	@DisplayName("simple encode")
	void encodeSimpleTest(long param) throws ValidationException, IOException {
		var m = new LatitudeLocationRequest(345);
		m.setMapId(param);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		MessageOutput out = new MessageOutput(stream);
		m.encode(out);
		assertArrayEquals(("LATITUDEv1 " + param + " ALL \r\n").getBytes(CHARENC), stream.toByteArray());
	}
	
	
	@Test
	@DisplayName("hashcode test for no reason")
	public void testHash() throws ValidationException {		
		var m = new LatitudeLocationRequest(345);
		m.setMapId(25);
		var n = new LatitudeLocationRequest(345);
		n.setMapId(25);
		assertEquals(m.hashCode(), n.hashCode());
		
		n.setMapId(26);
		assertNotEquals(m.hashCode(), n.hashCode());
	}
	
	@SuppressWarnings("unlikely-arg-type")
	@Test
	@DisplayName("equals test for no reason")
	public void testEquals() throws ValidationException {		
		var m = new LatitudeLocationRequest(345);
		assertTrue(m.equals(m));
		m.setMapId(25);
		var n = new LatitudeLocationRequest(345);
		n.setMapId(25);
		assertTrue(m.equals(n));
		
		assertFalse(m.equals(null));
		assertFalse(m.equals("ahoj"));
		n.setMapId(26);
		assertFalse(m.equals(n));
	}
	
}



















