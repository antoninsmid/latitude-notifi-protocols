/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1 test
* Class: CSI4321
*
************************************************/
package latitude.serialization.test;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Mock output stream class used to force IO exception during writing
 */
class TestIOOutputStream extends OutputStream {

	@Override
	public void write(int b) throws IOException {
		throw new IOException();
	}

	@Override
	public void write(byte[] b) throws IOException {
		throw new IOException();
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		throw new IOException();
	}

}
