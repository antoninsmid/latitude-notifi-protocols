/************************************************
*
* Author: Antonin Smid
* Assignment: Program 0 Test
* Class: CSI4321
*
************************************************/

package latitude.serialization.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import latitude.serialization.LocationRecord;
import latitude.serialization.MessageInput;
import latitude.serialization.MessageOutput;
import latitude.serialization.ValidationException;

/**
 * Test class for Location Record creation, serialization and deserialization.
 */
class LocationRecordTest {

	private static final String CHARENC = "ASCII";

	/**
	 * Test setting and getting user id. The userId is an unsigned int provided as
	 * long from interval <0,2^32-1).
	 * 
	 * @throws ValidationException if validation fails, is caught by assert
	 */
	@Test
	@DisplayName("user id test")
	public void userIdTest() throws ValidationException {

		LocationRecord lr = new LocationRecord();

		// happy scenario
		lr.setUserId(25L);
		assertEquals(25, lr.getUserId());

		lr.setUserId(3_000_000_000L);
		assertEquals(3_000_000_000L, lr.getUserId());

		// negative value should fail validation
		assertThrows(ValidationException.class, () -> {
			lr.setUserId(-20);
		});

		// over 2^32-1
		assertThrows(ValidationException.class, () -> {
			lr.setUserId(6_000_000_000L);
		});
	}
	

	/**
	 * Test setting and getting the longitude and latitude. Longitude and latitude
	 * should be doubles from the interval <-180, 180> represented as Strings that
	 * match the regex "^-?[0-9].+\\.[0.9].+$"
	 * 
	 * @throws ValidationException if the input string to set method is invalid,
	 *                             caught by assert.
	 */
	@Test
	@DisplayName("position test")
	public void positionTest() throws ValidationException {

		LocationRecord lr = new LocationRecord();

		// happy scenario
		lr.setLatitude("5.0");
		assertEquals("5.0", lr.getLatitude());
		lr.setLongitude("-150.000");
		assertEquals("-150.000", lr.getLongitude());

		// problematic scenarios
		String[] inputValuesLatitude = { "100", "-100", null, "+2.5", "2a2.5", "2a.5", "2.a5", "25", "22.5a", "22!5",
				"22.", ".5", "ahoj", "" };

		String[] inputValuesLongitude = { "200", "-200", null, "+2.5", "2a2.5", "2a.5", "2.a5", "25", "22.5a", "22!5",
				"22.", ".5", "ahoj", "" };

		for (String value : inputValuesLongitude) {
			assertThrows(ValidationException.class, () -> {
				lr.setLongitude(value);
			});
		}

		for (String value : inputValuesLatitude) {
			assertThrows(ValidationException.class, () -> {
				lr.setLatitude(value);
			});
		}

	}

	@Test
	@DisplayName("Location name test")
	public void locationNameTest() throws ValidationException {

		LocationRecord lr = new LocationRecord();

		lr.setLocationName("s?ring");

		// happy scenario
		LocationRecord lr2 = new LocationRecord();
		lr2.setLocationName("Hankammer");
		assertEquals("Hankammer", lr2.getLocationName());

		//fail scenario
		assertThrows(ValidationException.class, () -> {
			LocationRecord lr3 = new LocationRecord();
			lr3.setLocationName(null);
		});

		assertThrows(ValidationException.class, () -> {
			LocationRecord lr4 = new LocationRecord();
			lr4.setLocationName("ěščřžýáíé");
		});
				

	}
	

	@Test
	@DisplayName("Location description test")
	public void locationDescriptionTest() throws ValidationException {

		LocationRecord lr = new LocationRecord();

		// happy scenario
		lr.setLocationDescription("Hankammer");
		assertEquals("Hankammer", lr.getLocationDescription());

		assertThrows(ValidationException.class, () -> {
			lr.setLocationDescription(null);
		});

		assertThrows(ValidationException.class, () -> {
			lr.setLocationDescription(null);
		});

		assertThrows(ValidationException.class, () -> {
			lr.setLocationDescription("ěščřžýáíé");
		});
	}

	@Test
	@DisplayName("Constructor test")
	public void longConstructorTest() throws ValidationException, NullPointerException, IOException {

		LocationRecord lr = new LocationRecord(0, "1.2", "3.4", "BU", "Baylor");
		assertEquals(0, lr.getUserId());
		assertEquals("1.2", lr.getLongitude());
		assertEquals("3.4", lr.getLatitude());
		assertEquals("BU", lr.getLocationName());
		assertEquals("Baylor", lr.getLocationDescription());

		ByteArrayOutputStream os = new ByteArrayOutputStream(100);
		lr = new LocationRecord(4294967295L, "180.0", "-90.0", "o n e", "hello there!");
		lr.encode(new MessageOutput(os));

		lr = new LocationRecord(3_000_000_000L, "-1.2", "-57.41678353", "BU123-++82365081!",
				"YCUAQgUPzk\n7PgymGxHLd\\nMeO57fFWqB\nVFt9i69Kq1\n1msJhGpwiG");
		assertEquals(3_000_000_000L, lr.getUserId());
		assertEquals("-1.2", lr.getLongitude());
		assertEquals("-57.41678353", lr.getLatitude());
		assertEquals("BU123-++82365081!", lr.getLocationName());
		assertEquals("YCUAQgUPzk\n7PgymGxHLd\\nMeO57fFWqB\nVFt9i69Kq1\n1msJhGpwiG", lr.getLocationDescription());

		assertThrows(ValidationException.class, () -> { // negative user id
			new LocationRecord(-1, "1.2", "3.4", "BU", "Baylor");
		});

		assertThrows(ValidationException.class, () -> { // position out of interval
			new LocationRecord(1, "180.2", "3.4", "BU", "Baylor");
		});

		assertThrows(ValidationException.class, () -> {
			new LocationRecord(1, "10.2", "100.4", "BU", "Baylor");
		});

		assertThrows(ValidationException.class, () -> {
			new LocationRecord(1, "-180.2", "3.4", "BU", "Baylor");
		});

		assertThrows(ValidationException.class, () -> {
			new LocationRecord(1, "10.2", "-340.4", "BU", "Baylor");
		});

		assertThrows(ValidationException.class, () -> { // position wrong format
			new LocationRecord(-1, "1.", "3.4", "BU", "Baylor");
		});

		assertThrows(ValidationException.class, () -> {
			new LocationRecord(-1, "1.2", ".4", "BU", "Baylor");
		});
		
		assertThrows(NullPointerException.class, () -> {
			new LocationRecord(null);
		});
			
		assertThrows(NullPointerException.class, () -> {
			LocationRecord lr2 = new LocationRecord(0, "1.2", "3.4", "BU", "Baylor");
			lr2.encode(null);
		});
		
	}

	@Test
	@DisplayName("Decode Test")
	public void testDecode() throws IOException, ValidationException {

		// happy scenario
		var in = new MessageInput(new ByteArrayInputStream("5 5.0 -10.0 4 here5 there".getBytes(CHARENC)));
		var msg = new LocationRecord(in);
		assertEquals(5, msg.getUserId());
		assertEquals("5.0", msg.getLongitude());
		assertEquals("-10.0", msg.getLatitude());
		assertEquals("here", msg.getLocationName());
		assertEquals("there", msg.getLocationDescription());

		// validation fail
		String[] validationInput = { "-5 1.2 3.4 2 BU6 Baylor", "1 1.2 3.4 1 BU2 Baylor",
				"6000000000 1.2 3.4 2 BU6 Baylor", "1 -200.2 3.4 2 BU6 Baylor", "1 -20. 3.4 2 BU6 Baylor",
				"1 ahoj 3.4 2 BU6 Baylor" };

		for (String initValue : validationInput) {
			var in0 = new MessageInput(new ByteArrayInputStream(initValue.getBytes(CHARENC)));
			assertThrows(ValidationException.class, () -> {
				new LocationRecord(in0);
			});
		}

		// I/O fail
		String[] validationInputIO = { "", "1", "1 ", "1 2.2", "1 2.2 ", "1 2.2 3.4", "1 2.2 3.4 ", "1 2.2 3.4",
				"1 2.2 3.4 ", "1 2.2 3.4 2 BU", "1 2.2 3.4 2 B", "1 2.2 3.4 2 BU " };

		for (String initValue : validationInputIO) {
			var in0 = new MessageInput(new ByteArrayInputStream(initValue.getBytes(CHARENC)));
			assertThrows(IOException.class, () -> {
				new LocationRecord(in0);
			});
		}

		var in8 = new MessageInput(new ByteArrayInputStream("1 2.2 3.4 2 BU8 Baylor".getBytes(CHARENC)));
		assertThrows(IOException.class, () -> {
			new LocationRecord(in8);
		});

		MessageInput in9 = new MessageInput(new TestIOInputStream());
		assertThrows(IOException.class, () -> {
			new LocationRecord(in9);
		});
	}

	@Test
	@DisplayName("Decode Test Special")
	public void testDecodeSpecial() throws IOException, ValidationException {

		// validation fail
		String[] validationInput = { "5 5.0", "5 5.0 -10.0 4 here" };

		for (String initValue : validationInput) {
			var in0 = new MessageInput(new ByteArrayInputStream(initValue.getBytes(CHARENC)));
			assertThrows(EOFException.class, () -> {
				new LocationRecord(in0);
			});

		}
	}

	@Test
	@DisplayName("Encode test")
	public void testEncode() throws IOException, ValidationException {

		var bOut = new ByteArrayOutputStream();
		var out = new MessageOutput(bOut);

		new LocationRecord(1, "1.2", "3.4", "BU", "Baylor").encode(out);
		assertArrayEquals("1 1.2 3.4 2 BU6 Baylor".getBytes(CHARENC), bOut.toByteArray());

		bOut = new ByteArrayOutputStream();
		out = new MessageOutput(bOut);
		new LocationRecord(3_000_000_000L, "-120.200651", "50.45646", "BU/25651/!@$%&)%_$(%_&",
				"Baylor/n//nr)!#%&^)!#&*$").encode(out);
		assertArrayEquals("3000000000 -120.200651 50.45646 22 BU/25651/!@$%&)%_$(%_&24 Baylor/n//nr)!#%&^)!#&*$"
				.getBytes(CHARENC), bOut.toByteArray());

		bOut = new ByteArrayOutputStream();
		out = new MessageOutput(bOut);
		new LocationRecord(1, "1.2", "3.4", "", "").encode(out);
		assertArrayEquals("1 1.2 3.4 0 0 ".getBytes(CHARENC), bOut.toByteArray());

		// I/O fail
		TestIOOutputStream bOutio = new TestIOOutputStream();
		var out2 = new MessageOutput(bOutio);

		assertThrows(IOException.class, () -> {
			new LocationRecord(1, "1.2", "3.4", "BU", "Baylor").encode(out2);
		});
	}
	
	@Test
	@DisplayName("hashcode test for no reason")
	public void testHash() throws ValidationException {
		
		var lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		var lr2 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");		
		assertEquals(lr1.hashCode(), lr2.hashCode());

		lr1.makeNull(false, false, false, true);
		assertNotEquals(lr1.hashCode(), lr2.hashCode());	
		
		lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		lr1.makeNull(false, false, true, false);
		assertNotEquals(lr1.hashCode(), lr2.hashCode());	

		lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		lr1.makeNull(false, true, false, false);
		assertNotEquals(lr1.hashCode(), lr2.hashCode());	
		
		lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		lr1.makeNull(true, false, false, false);
		assertNotEquals(lr1.hashCode(), lr2.hashCode());		
	}
	
	@Test
	@DisplayName("to string test for no reason")
	public void testToString() throws ValidationException {		
		var lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		assertEquals("id: 1 name: BUdesc: Baylor lon: 1.2 lat: 3.4", lr1.toString());
	}
	
	@SuppressWarnings("unlikely-arg-type")
	@Test
	@DisplayName("equals test for no reason")
	public void testEquals() throws ValidationException {		
		var lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		assertTrue(lr1.equals(lr1));
		var lr2 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");	
		assertTrue(lr1.equals(lr2));
		
		lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		lr2 = new LocationRecord(2, "1.2", "3.4", "BU", "Baylor");	
		assertFalse(lr2.equals(lr1));
		
		assertFalse(lr1.equals(null));
		assertFalse(lr1.equals("whats up"));
		
		lr1.makeNull(false, false, false, true);
		assertFalse(lr1.equals(lr2));
		
		lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		lr1.makeNull(false, false, true, false);
		assertFalse(lr1.equals(lr2));

		lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		lr1.makeNull(false, true, false, false);
		assertFalse(lr1.equals(lr2));
		
		lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		lr1.makeNull(true, false, false, false);
		assertFalse(lr1.equals(lr2));
		
		
		lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		lr2 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");	
		
		lr1.makeNull(false, false, false, true);
		assertFalse(lr2.equals(lr1));
		
		lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		lr1.makeNull(false, false, true, false);
		assertFalse(lr2.equals(lr1));

		lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		lr1.makeNull(false, true, false, false);
		assertFalse(lr2.equals(lr1));
		
		lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		lr1.makeNull(true, false, false, false);
		assertFalse(lr2.equals(lr1));
		
		lr1 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");
		lr2 = new LocationRecord(1, "1.2", "3.4", "BU", "Baylor");	
		
		
	}

	
	
}






























