/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1
* Class: CSI4321
*
************************************************/

package latitude.serialization;

import java.io.IOException;

import latitude.serialization.util.IOHelper;
import latitude.serialization.util.ValidationHelper;

/**
 * Representation of the error message.
 *
 */
public class LatitudeError extends LatitudeMessage {

	private final String ERROR_OPERATION = "ERROR "; // error operation signature, the space in the end is important
	private String errorMessage;
	
	/**
	 * Constructs error message using set values
	 * 
	 * @param mapId        ID for message map
	 * @param errorMessage error message
	 * @throws ValidationException if validation fails
	 */
	public LatitudeError(long mapId, String errorMessage) throws ValidationException {
		setMapId(mapId);
		setErrorMessage(errorMessage);
	}

	/**
	 * Constructor lite used by parent decoder.
	 * 
	 * @param mapId map ID for message map
	 * @throws ValidationException if validation fails
	 */
	protected LatitudeError(long mapId) throws ValidationException {
		setMapId(mapId);
	}

	/**
	 * Return error message
	 * 
	 * @return error message
	 */
	public final String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Set error message
	 * 
	 * @param errorMessage message to set for this exception
	 * @throws ValidationException if validation fails
	 */
	public void setErrorMessage(String errorMessage) throws ValidationException {
		if (errorMessage == null) {
			throw new ValidationException("errorMessage", "error message can not be null");
		}
		if (!ValidationHelper.checkChars(errorMessage)) {
			throw new ValidationException("errorMessage", "error message contains characters out of ascii");
		}
		this.errorMessage = errorMessage;
	}

	/**
	 * Decodes latitude error from stream.
	 */
	@Override
	protected void decodeSpecific(MessageInput in) throws ValidationException, IOException {
		setErrorMessage(IOHelper.readString(in));
		super.decodeSpecific(in);
	}

	/**
	 * Encodes the message into message output
	 */
	@Override
	public void encode(MessageOutput out) throws IOException {		
		super.encode(out); // write the header
		IOHelper.writeBytesToMessage(IOHelper.getStringNoEncode(ERROR_OPERATION), out); // write error message
		IOHelper.writeEncodedStringToMessage(errorMessage, out);
		IOHelper.writeEOLN(out); // write end of line and flush
		out.getStream().flush();
	}
	
	/**
	 * Help method for testing purposes. Need to cover hash and equals test.
	 */
	public void makeNull() {
		this.errorMessage = null;
	}
	
	@Override
	public String getOperation() {
		return ERROR_OPERATION;
	}

	@Override
	public String toString() {
		return "LatitudeError [errorMessage=" + errorMessage + ", mapId=" + getMapId() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((errorMessage == null) ? 0 : errorMessage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		LatitudeError other = (LatitudeError) obj;
		if (errorMessage == null) {
			if (other.errorMessage != null)
				return false;
		} else if (!errorMessage.equals(other.errorMessage))
			return false;
		return true;
	}

}
