/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1
* Class: CSI4321
*
************************************************/
package latitude.serialization;

import java.io.IOException;

import latitude.serialization.util.IOHelper;
import latitude.serialization.util.ValidationHelper;

/**
 * 
 * Represents a new location and provides serialization/deserialization
 */
public class LatitudeNewLocation extends LatitudeMessage {

	private LocationRecord locationRecord;	// location record with the information about new location
	private final String NEW_OPERATION = "NEW "; // new record operation signature, the space in the end is important

	/**
	 * Constructs new location using set values
	 * 
	 * Encapsulation testing note:
	 * 
	 * if this is supposed to be encapsulated, how to call it from ClientApp, which
	 * must be in a different package?
	 * 
	 * @param mapId    ID for message map
	 * @param lr new location
	 * @throws ValidationException if validation fails
	 */
	public LatitudeNewLocation(long mapId, LocationRecord lr) throws ValidationException {
		setMapId(mapId);
		ValidationHelper.validateLocationRecord(lr);
		LocationRecord newRecord = new LocationRecord(lr.getUserId(), lr.getLongitude(), lr.getLatitude(), lr.getLocationName(), lr.getLocationDescription());
		this.locationRecord = newRecord;
	}

	/**
	 * Constructor lite used by parent decoder.
	 * 
	 * @param mapId map ID for message map
	 * @throws ValidationException if validation fails
	 */
	protected LatitudeNewLocation(long mapId) throws ValidationException {
		setMapId(mapId);
	}

	/**
	 * Returns Location
	 * 
	 * @return location record
	 */
	public LocationRecord getLocationRecord() {
		return locationRecord;
	}

	/**
	 * Sets Location
	 * 
	 * Jupiter:Latitude New Location Message:location record setter:encapsulation is
	 * testing if the method is not public and needs it to be at least protected, at
	 * the same time: Jupiter:Latitude New Location Message:location record setter
	 * is trying to use the method and does not work, with refused access.
	 * 
	 * How to pass both tests at the same time?
	 * 
	 * @param lr new location
	 * @throws ValidationException if null or validation fails
	 * 
	 */
	public void setLocationRecord(LocationRecord lr) throws ValidationException {
		ValidationHelper.validateLocationRecord(lr);
		LocationRecord newRecord = new LocationRecord(lr.getUserId(), lr.getLongitude(), lr.getLatitude(), lr.getLocationName(), lr.getLocationDescription());
		this.locationRecord = newRecord;
	}

	/**
	 * Encodes message into output.
	 */
	@Override
	public void encode(MessageOutput out) throws IOException {
		super.encode(out); // write protocol header
		IOHelper.writeBytesToMessage(IOHelper.getStringNoEncode(NEW_OPERATION), out); // write operation header
		locationRecord.encode(out); // write location record
		IOHelper.writeEOLN(out); // write end of line and flush
		out.getStream().flush();
	}

	/**
	 * Decodes location record from stream, then calls super to check the valid EOL.
	 */
	@Override
	protected void decodeSpecific(MessageInput in) throws ValidationException, IOException {
		this.locationRecord = new LocationRecord(in);
		super.decodeSpecific(in);
	}

	@Override
	public String toString() {
		return "LatitudeNewLocation [locationRecord=" + locationRecord + ", MapId=" + getMapId() + "]";
	}
	
	/**
	 * Sets location record to null, for uncescessary testing purposes.
	 */
	public void makeLRNull() {
		this.locationRecord = null;
	}
	

	@Override
	public String getOperation() {
		return NEW_OPERATION;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((locationRecord == null) ? 0 : locationRecord.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		LatitudeNewLocation other = (LatitudeNewLocation) obj;
		if (locationRecord == null) {
			if (other.locationRecord != null)
				return false;
		} else if (!locationRecord.equals(other.locationRecord))
			return false;
		return true;
	}
	

}
