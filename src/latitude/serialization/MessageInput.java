/************************************************
*
* Author: Antonin Smid
* Assignment: Program 0 Test
* Class: CSI4321
*
************************************************/

package latitude.serialization;

import java.io.InputStream;
import java.util.Objects;

/**
 * Deserialization input source for messages. In its simplest form, this is just
 * a wrapper for the InputStream. You may add methods to assist you with general
 * deserialization. Do not add protocol-specific deserialization here. That
 * belongs in the specific message classes.
 */
public class MessageInput {

    private InputStream stream;

    /**
     * Constructs a new input source from an InputStream
     * 
     * @param in byte input source
     * 
     * @throws NullPointerException if provided stream is null
     */
    public MessageInput(InputStream in) throws NullPointerException {
        setStream(in);
    }

    public InputStream getStream() {
        return stream;
    }

    public void setStream(InputStream stream) throws NullPointerException {
        this.stream = Objects.requireNonNull(stream, "input stream can not be null");
    }

}
