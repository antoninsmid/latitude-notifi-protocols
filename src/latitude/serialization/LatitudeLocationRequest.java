/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1
* Class: CSI4321
*
************************************************/
package latitude.serialization;

import java.io.IOException;

import latitude.serialization.util.IOHelper;

/**
 * Representation of the request all locations message.
 *
 */
public class LatitudeLocationRequest extends LatitudeMessage {

	private final String REQUEST_OPERATION = "ALL "; // request all operation signature, the space in the end is
														// important

	/**
	 * Constructs location request using set values
	 * 
	 * @param mapId ID for message map
	 * @throws ValidationException if validation fails
	 */
	public LatitudeLocationRequest(long mapId) throws ValidationException {
		setMapId(mapId);
	}
	
	@Override
	public String getOperation() {
		return REQUEST_OPERATION;
	}

	/**
	 * Encodes the message into message output
	 */
	@Override
	public void encode(MessageOutput out) throws IOException {
		super.encode(out); // write header
		IOHelper.writeBytesToMessage(IOHelper.getStringNoEncode(REQUEST_OPERATION), out); // write the request message
		IOHelper.writeEOLN(out); // write end of line and flush
		out.getStream().flush();
	}

	@Override
	protected void decodeSpecific(MessageInput in) throws ValidationException, IOException {
		// the request message is very simple, this method does not need to do anything
		super.decodeSpecific(in);
	}

	@Override
	public String toString() {
		return "LatitudeLocationRequest [mapId=" + getMapId() + "]";
	}
	
	

}
