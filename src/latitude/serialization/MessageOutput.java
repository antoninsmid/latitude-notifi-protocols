/************************************************
*
* Author: Antonin Smid
* Assignment: Program 0 Test
* Class: CSI4321
*
************************************************/

package latitude.serialization;

import java.io.OutputStream;
import java.util.Objects;

/**
 * Serialization output source for messages. In its simplest form, this is just
 * a wrapper for the OutputStream. You may add methods to assist you with
 * general serialization. Do not add protocol-specific serialization here. That
 * belongs in the specific message classes.
 */
public class MessageOutput {

	// wrapped output stream  
    private OutputStream stream;

    /**
     * Constructs a new output source from an OutputStream
     * 
     * @param out byte output source
     */
    public MessageOutput(OutputStream out) throws NullPointerException {
        setStream(out);
    }

    /**
     * returns inner output stream
     * @return the output stream
     */
    public OutputStream getStream() {
        return stream;
    }

    /**
     * Sets new output stream
     * @param out the new output stream
     * @throws NullPointerException if output stream is null
     */
    public void setStream(OutputStream out) throws NullPointerException {
        this.stream = Objects.requireNonNull(out, "output stream can not be null.");
    }

}
