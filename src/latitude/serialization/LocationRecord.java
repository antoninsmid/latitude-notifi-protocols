/************************************************
*
* Author: Antonin Smid
* Assignment: Program 0 Test
* Class: CSI4321
*
************************************************/

package latitude.serialization;

import latitude.serialization.util.IOHelper;
import latitude.serialization.util.ValidationHelper;

/**
 * Represents a location record and provides serialization/deserialization
 * 
 */
public class LocationRecord {

	private long userId;

	private String longitude;
	private String latitude;

	private String locationName;
	private String locationDescription;

	/**
	 * Constructs location record with set values
	 * 
	 * @param userId              ID for user
	 * @param longitude           position of location
	 * @param latitude            position of location
	 * @param locationName        name of location
	 * @param locationDescription description of location
	 * 
	 * @throws ValidationException if validation failure
	 */
	public LocationRecord(long userId, String longitude, String latitude, String locationName,
			String locationDescription) throws ValidationException {
		// use getters and setters to ensure validation
		setUserId(userId);
		setLongitude(longitude);
		setLatitude(latitude);
		setLocationName(locationName);
		setLocationDescription(locationDescription);
	}

	/**
	 * Constructs location record using deserialization
	 * 
	 * @param in deserialization input source
	 * @throws ValidationException if validation failure
	 * @throws                     java.io.IOException if I/O problem
	 */
	public LocationRecord(MessageInput in) throws ValidationException, java.io.IOException {
		// get values from stream using helper methods that handle deserialization

		if (in == null) {
			throw new NullPointerException("Input can not be null.");
		}

		setUserId(IOHelper.readUnsignedInt(in));
		setLongitude(IOHelper.readStringBeforeSpace(in));
		setLatitude(IOHelper.readStringBeforeSpace(in));
		setLocationName(IOHelper.readString(in));
		setLocationDescription(IOHelper.readString(in));
	}

	/**
	 * An empty constructor mainly for testing purposes.
	 */
	public LocationRecord() {

	}

	/**
	 * Serializes location record
	 * 
	 * @param out serialization output destination
	 * @throws java.io.IOException if I/O problem
	 */
	public void encode(MessageOutput out) throws java.io.IOException {
		// write values to the stream using ascii char encoding and flush in the end

		if (out == null) {
			throw new NullPointerException("Output can not be null.");
		}

		IOHelper.writeEncodedUnsignedIntegerToMessage(userId, out);
		IOHelper.writeEncodedDoubleToMessage(longitude, out);
		IOHelper.writeEncodedDoubleToMessage(latitude, out);
		IOHelper.writeEncodedStringToMessage(locationName, out);
		IOHelper.writeEncodedStringToMessage(locationDescription, out);
		out.getStream().flush();
	}

	/**
	 * Returns user ID
	 * 
	 * @return user ID
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * Sets user ID
	 * 
	 * @param userId new user ID
	 * @throws ValidationException if validation fails
	 */
	public void setUserId(long userId) throws ValidationException {
		ValidationHelper.validateUnsignedInt(userId);
		this.userId = userId;
	}

	/**
	 * Returns longitude
	 * 
	 * @return longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * Sets longitude
	 * 
	 * @param longitude new longitude
	 * @throws ValidationException if validation fails
	 */
	public void setLongitude(String longitude) throws ValidationException {
		ValidationHelper.validatePositionDouble(longitude, "longitude", 180);
		this.longitude = longitude;
	}

	/**
	 * Returns latitude
	 * 
	 * @return latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * Sets latitude
	 * 
	 * @param latitude new latitude
	 * @throws ValidationException if validation fails
	 */
	public void setLatitude(String latitude) throws ValidationException {
		ValidationHelper.validatePositionDouble(latitude, "latitude", 90);
		this.latitude = latitude;
	}

	/**
	 * Returns location name
	 * 
	 * @return location name
	 */
	public String getLocationName() {
		return locationName;
	}

	/**
	 * Sets location name
	 * 
	 * @param locationName new location name
	 * @throws ValidationException if validation fails0
	 */
	public void setLocationName(String locationName) throws ValidationException {
		if (locationName == null) {
			throw new ValidationException(locationDescription, "The location name can not be null.");
		}
		if (!ValidationHelper.checkChars(locationName)) {
			throw new ValidationException(locationDescription, "only ascii characters.");
		}
		this.locationName = locationName;
	}

	/**
	 * Returns location description
	 * 
	 * @return location description
	 */
	public String getLocationDescription() {
		return locationDescription;
	}

	/**
	 * Sets location description
	 * 
	 * @param locationDescription new location description
	 * @throws ValidationException if validation fails
	 */
	public void setLocationDescription(String locationDescription) throws ValidationException {
		if (locationDescription == null) {
			throw new ValidationException(locationDescription, "The location description can not be null.");
		}
		if (!ValidationHelper.checkChars(locationDescription)) {
			throw new ValidationException(locationDescription, "only ascii characters.");
		}
		this.locationDescription = locationDescription;
	}

	/**
	 * In order to fully test hash function, I need to set some values null although
	 * it violates the business rules
	 * 
	 * @param lat if latitude make null
	 * @param lon if longitude make null
	 * @param name if name make null
	 * @param desc if description make null
	 */
	public void makeNull(boolean lat, boolean lon, boolean name, boolean desc) {
		if (lat) {
			this.latitude = null;
		}
		if (lon) {
			this.longitude = null;
		}
		if (name) {
			this.locationName = null;
		}
		if (desc) {
			this.locationDescription = null;
		}
	}

	/**
	 * Standard hashcode implementation generated by eclipse.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((locationDescription == null) ? 0 : locationDescription.hashCode());
		result = prime * result + ((locationName == null) ? 0 : locationName.hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + (int) (userId ^ (userId >>> 32));
		return result;
	}

	/**
	 * Standard equals implementation generated by eclipse.
	 * 
	 * @param obj other object to compare to
	 * @return whether the objects are the same or not
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocationRecord other = (LocationRecord) obj;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (locationDescription == null) {
			if (other.locationDescription != null)
				return false;
		} else if (!locationDescription.equals(other.locationDescription))
			return false;
		if (locationName == null) {
			if (other.locationName != null)
				return false;
		} else if (!locationName.equals(other.locationName))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "id: " + userId + " name: " + locationName + "desc: " + locationDescription + " lon: " + longitude
				+ " lat: " + latitude;
	}
}