/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1
* Class: CSI4321
*
************************************************/
package latitude.serialization.util;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import latitude.serialization.LocationRecord;
import latitude.serialization.ValidationException;

/**
 * Class providing various Latitude protocol validations.
 *
 */
public class ValidationHelper {

	private static final Long UNSIGNED_INT_TOP_LIMIT = 4_294_967_296L; // max unsigned int, user id must be smaller
	private static final String CHARENC = "ASCII";
	private static CharsetEncoder encoder = Charset.forName(CHARENC).newEncoder();

	/**
	 * Validates that unsigned integer is smaller than 2^31 representable in 32
	 * bits
	 * 
	 * @param number to validate
	 * @throws ValidationException if the long can not be converted to unsigned int
	 */
	public static void validateUnsignedInt(Long number) throws ValidationException {
		if (number >= UNSIGNED_INT_TOP_LIMIT || number < 0) {
			throw new ValidationException(Long.toString(number),
					"Unsigned Integer must be positive and strictly smaller than 2^32-1");
		}
	}

	/**
	 * Check if the chars in string are from standard ASCII
	 * 
	 * @param s string to check
	 * @return if the string is valid standard ASCII
	 */
	public static boolean checkChars(String s) {
		return encoder.canEncode(s);
	}

	/**
	 * Validates the double values for position represented by String
	 * 
	 * @param positionInput the value to validate
	 * @param inputName     name of the field (latitude, longitude)
	 * @param limit         the interval (latitude 90, longitude 180)
	 * @throws ValidationException if validation fails - null. bad parsing or out of
	 *                             the interval.
	 */
	public static void validatePositionDouble(String positionInput, String inputName, int limit)
			throws ValidationException {

		if (positionInput == null) {
			throw new ValidationException(inputName,
					inputName + " can not be null. - Validation Helper, positionDouble");
		}

		String patternString = "^-?[0-9]+\\.[0-9]+$";
		Pattern pattern = Pattern.compile(patternString);

		Matcher matcher = pattern.matcher(positionInput);

		if (!matcher.matches()) {
			throw new ValidationException(inputName, inputName + " does not match the <DOUBLE_STRING> pattern.");
		}

		Double parsed = Double.valueOf(positionInput);

		if (parsed > limit || parsed < -limit) {
			throw new ValidationException(inputName, "Poorly formatted double");
		}
	}

	/**
	 * Validates location record
	 * 
	 * @param record record to validate
	 * @throws ValidationException if validation fails
	 */
	public static void validateLocationRecord(LocationRecord record) throws ValidationException {
		if (record == null) {
			throw new ValidationException("location record", "the location record can not be null");
		}
		new LocationRecord(record.getUserId(), record.getLongitude(), record.getLatitude(), record.getLocationName(),
				record.getLocationDescription());
	}

}
