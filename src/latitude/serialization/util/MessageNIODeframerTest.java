/************************************************
*
* Author: Antonin Smid
* Assignment: Program 7
* Class: CSI4321
*
************************************************/

package latitude.serialization.util;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.junit.jupiter.api.Test;

/**
 * Class with tests of message NIO deframer.
 *
 */
class MessageNIODeframerTest {

	/**
	 * Tests simple add method. First adds buffer with data to add, then retrieves
	 * the stream output and compares the bytes.
	 * 
	 * @throws IOException if stream get bytes fails (should not happen)
	 */
	@Test
	void addTest() throws IOException {

		MessageNIODeframer df = new MessageNIODeframer();
		byte[] arr = "Ahoj, jak se mas\r\n".getBytes();
		ByteBuffer buf = ByteBuffer.wrap(arr);
		df.addData(buf, arr.length);

		byte[] arr2 = df.hasNext().getStream().readAllBytes();
		for (int i = 0; i < arr2.length; i++) {
			assertEquals(arr2[i], arr[i]);
		}
	}

	/**
	 * Tests the hasNext method of MessageNIODeframer. First, adds some data, which
	 * do not create full message. Tries to retrieve, but should receive null, since
	 * there is no complete message in the buffer. Next, it adds two more messages,
	 * reads them and eventually tries the buffer again and gets null, since there
	 * are no more messages.
	 * 
	 * @throws IOException if stream read all bytes fails (should not happen)
	 */
	@Test
	void hasNextTest() throws IOException {

		MessageNIODeframer df = new MessageNIODeframer();
		byte[] arr = "Ahoj, jak se mas".getBytes();
		ByteBuffer buf = ByteBuffer.wrap(arr);
		df.addData(buf, arr.length);

		assertNull(df.hasNext());

		arr = ", kamo?\r\nyoyo, man.\r\n".getBytes();
		df.addData(ByteBuffer.wrap(arr), arr.length);

		byte[] arr2 = df.hasNext().getStream().readAllBytes();
		byte[] arrcheck = "Ahoj, jak se mas, kamo?\r\n".getBytes();

		for (int i = 0; i < arrcheck.length; i++) {
			assertEquals(arr2[i], arrcheck[i]);
		}

		arr2 = df.hasNext().getStream().readAllBytes();
		arrcheck = "yoyo, man.\r\n".getBytes();

		for (int i = 0; i < arrcheck.length; i++) {
			assertEquals(arr2[i], arrcheck[i]);
		}

		assertNull(df.hasNext());
	}

}
