/************************************************
*
* Author: Antonin Smid
* Assignment: Program 7
* Class: CSI4321
*
************************************************/

package latitude.serialization.util;
import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

import latitude.serialization.MessageInput;

/**
 * Deframer for Latitude server AIO.
 * This class receives the data chunks from the network
 * and provides iterator-like method hasNext for complete messages.
 * Single class instance for each client (channel)
 *
 */
public class MessageNIODeframer {
	
	// buffer for the unprocessed data
	private byte[] data = new byte[0];
	// end of line \r\n means end of the message
	private static final int[] EOLN = new int[] {13, 10};	
	// to distinguish clients, debugging purposes
	private int id;
	
	/**
	 * constructor sets random id to the client
	 */
	public MessageNIODeframer() {
		this.id = (int) (Math.random() * 1000);
	}
	
	/**
	 * Adds new data to be processed
	 * @param buf buffer with new byte data
	 * @param bytesRead length of the data in bytes
	 */
	public void addData(ByteBuffer buf, int bytesRead) {
		buf.rewind();
		byte[] arrayToAppend = new byte[bytesRead];
		buf.get(arrayToAppend, 0, bytesRead);
		data = IOHelper.concat(data, arrayToAppend);
	}
	
	/**
	 * Returns complete message to be processed without any blocking
	 * in case there is one. If there is none, returns null.
	 * 
	 * @return latitude message in form of MessageInput with ByteArrayStream inside
	 */
	public MessageInput hasNext() {
		
		//iterate over the array and try to find complete message ending with EOLN
		int EOLNidx = -1;	// index of the last byte of the message (including eoln)
		for (int i = 0; i < data.length - 1; i++) {
			if(EOLN[0] == data[i] && EOLN[1] == data[i + 1]) {
				EOLNidx = i + 1;
				break;
			}
		}
		
		// no complete message in the buffer
		if(EOLNidx == -1) {
			return null;
		}
		
		// found a complete message
		byte[] message = Arrays.copyOfRange(data, 0, EOLNidx + 1);
		data = Arrays.copyOfRange(data, EOLNidx + 1, data.length);
		
		return new MessageInput(new ByteArrayInputStream(message));
	}
	
	@Override
	public String toString() {
		return "cli " + id;
	}
	
}
