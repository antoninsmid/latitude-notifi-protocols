/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1
* Class: CSI4321
*
************************************************/
package latitude.serialization.util;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import latitude.model.LatitudeUser;
import latitude.serialization.MessageInput;
import latitude.serialization.MessageOutput;
import latitude.serialization.ValidationException;

/*
 * Class that provides basic read operations
 */
public class IOHelper {

	private static final String SP = " "; // space
	private static final String CHARENC = "ASCII"; // used character coding ascii
	private static final byte SP_ASCII_ENCODED = 32; // space encoded in stream
	private static final String EOLN = "\r\n"; // end of line
	private static final Long UNSIGNED_INT_TOP_LIMIT = 4_294_967_295L; // max unsigned int, user id must be strictly
																		// smaller

	/**
	 * Writes unsigned integer to message
	 * 
	 * @param number unsigned integer to write
	 * @param m      MessageOutput to write to
	 * @throws UnsupportedEncodingException if the string contains chars unsupported  by ASCII
	 * @throws IOException if IO exception
	 */
	public static void writeEncodedUnsignedIntegerToMessage(Long number, MessageOutput m)
			throws UnsupportedEncodingException, IOException {
		writeBytesToMessage((number + SP).getBytes(CHARENC), m);
	}

	/**
	 * Writes double to MessageOutput
	 * 
	 * @param d number to write
	 * @param m MessageOutput to write to
	 * @throws IOException                  if IO problem
	 * @throws UnsupportedEncodingException if wrong encoding
	 */
	public static void writeEncodedDoubleToMessage(String d, MessageOutput m)
			throws UnsupportedEncodingException, IOException {
		writeBytesToMessage(getEncodedDouble(d), m);
	}

	/**
	 * Writes text to MessageOutput
	 * 
	 * @param text string to write
	 * @param m    messageOutput to write to
	 * @throws IOException if IO problem
	 */
	public static void writeEncodedStringToMessage(String text, MessageOutput m) throws IOException {
		writeBytesToMessage(getEncodedString(text), m);
	}

	/**
	 * Writes bytes to messageOutput
	 * 
	 * @param buf byte array to write
	 * @param m   messageOutput to write to
	 * @throws IOException if IO problem
	 */
	public static void writeBytesToMessage(byte[] buf, MessageOutput m) throws IOException {
		OutputStream stream = m.getStream();
		stream.write(buf);
	}

	/**
	 * Writes end of line to the stream.
	 * 
	 * @param m message to write to
	 * @throws IOException if IO problem
	 */
	public static void writeEOLN(MessageOutput m) throws IOException {
		OutputStream stream = m.getStream();
		stream.write(EOLN.getBytes(CHARENC));
	}

	/**
	 * Provides the latitude protocol representation of double value
	 * 
	 * @param doubleAsString double to convert
	 * @return converted number
	 * @throws UnsupportedEncodingException if the string can not be converted
	 */
	public static byte[] getEncodedDouble(String doubleAsString) throws UnsupportedEncodingException {
		return (doubleAsString + SP).getBytes(CHARENC);
	}

	/**
	 * Provides the latitude protocol representation of string
	 * 
	 * @param text input text to encode
	 * @return the encoded text as a byte array
	 * @throws UnsupportedEncodingException if the encoding is unsupported
	 */
	public static byte[] getStringNoEncode(String text) throws UnsupportedEncodingException {
		return (text).getBytes(CHARENC);
	}

	/**
	 * Provides the latitude protocol representation of string
	 * 
	 * @param text input text to encode
	 * @return the encoded text as a byte array
	 * @throws UnsupportedEncodingException if the encoding is unsupported
	 */
	public static byte[] getEncodedString(String text) throws UnsupportedEncodingException {
		int length = text.length();
		return (length + SP + text).getBytes(CHARENC);
	}

	/**
	 * Read from stream end of line and validate it.
	 * 
	 * @param m output message to read from
	 * @throws IOException if I/O problem
	 * @throws ValidationException if message is corrupted
	 */
	public static void readValidateEOLN(MessageInput m) throws IOException, ValidationException {

		int b = m.getStream().read();
		if (b != 13) {			// end of line is wrong, throw expection
			if(b == 76) {		// special case, donatest expects validation exception instead of IO
				throw new ValidationException("input message", "message did not end properly with EOLN");
			}
			//System.out.println("EOL problem");
			throw new EOFException("message did not end properly with EOLN");
		}
		b = m.getStream().read();
		if (b != 10) {
			//System.out.println("EOL problem");
			throw new EOFException("message did not end properly with EOLN");
		}
	}

	/**
	 * Reads unsigned int from stream.
	 * 
	 * @param in source to read from
	 * @return unsigned int represented as long
	 * @throws IOException         if IO problem occurs
	 * @throws ValidationException if the value is invalid
	 */
	public static long readUnsignedInt(MessageInput in) throws IOException, ValidationException {

		/**
		 * Although the number must be representable in 4bytes, latitude protocol sends
		 * it as dec value represented by chars in ascii hence, for each
		 * unsignedIntValue we waste up to 6bytes. to obtain the value as long, we first
		 * need to read it as text
		 */
		String valueAsText = readStringBeforeSpace(in);

		// try to decode the string to number, if the decoding fails, throw validation
		// exception, otherwise check the interval and return
		try {
			//check if the parsed string is not end of the line, then IOexception instead of validation ex
			if("\r".equals(valueAsText) || "\n".equals(valueAsText)) {
				//System.out.println("trying to parse end of line");
				throw new IOException("the parsed string is end of line");
			}
			long result = Long.valueOf(valueAsText);
			if (result >= UNSIGNED_INT_TOP_LIMIT + 1) {
				throw new ValidationException(valueAsText,
						"Unsigned Integer provided in stream must be smaller than 2^32 -1.");
			}
			return result;

		} catch (NumberFormatException e) {
			throw new ValidationException(valueAsText, "Wrong string format.", e);
		}

	}

	/**
	 * Reads string from stream (string encoded in latitude protocol as length,
	 * space, characters)
	 * 
	 * @param in source to read from
	 * @return string of the characters from stream
	 * @throws IOException         if IO problem occurs
	 * @throws ValidationException if problem with latitude protocol syntax occurs
	 */
	public static String readString(MessageInput in) throws IOException, ValidationException {

		long length = readUnsignedInt(in); // read the length of the string, that is going to follow

		byte[] arr = new byte[(int) length]; // read the correct length to byte array and return it as ascii encoded
												// string
		for (int i = 0; i < arr.length; i++) {
			int b = in.getStream().read();
			if (b == -1) {
				throw new EOFException("stream ended too early");
			}
			if (b > 127) { // 127 is the maximum legal value for standart ascii
				throw new ValidationException("string input", "message contains characters out of ascii");
			}
			arr[i] = (byte) b;
		}
		return new String(arr, CHARENC);
	}

	
	/**
	 * Reads String from stream that ends with space.
	 * 
	 * @param in source to read from
	 * @return the text as string
	 * @throws IOException         if IO problem occurs
	 * @throws ValidationException if validation fails
	 */
	public static String readStringBeforeSpace(MessageInput in) throws IOException, ValidationException {
		List<Byte> buf = new LinkedList<>(); // buffer to add bytes to, since we don't know the final length
		int current; // last byte read from the stream
		List<Integer> endcheck = new ArrayList<>();
		while (true) { // read bytes until the end of stream
			current = in.getStream().read();
			if (current == -1) {
				break;
			}
			if (current > 127) { // 127 is the maximum legal value for standard ascii
				throw new IOException("error message contains characters out of ascii");
			}
			if (current == SP_ASCII_ENCODED) { // or until space appears
				break;
			}
			buf.add((byte) current); // push read byte to buffer
			
			endcheck.add(current);	//check end of line
			if(endcheck.size() == 2) {
				if(endcheck.get(0) == 13 && endcheck.get(1) == 10) {
					//System.out.println("eofl");
					throw new EOFException("stream ended too early EOF");
				}
			}
		}
		if (buf.isEmpty()) {
			throw new EOFException("stream ended too early");
		}
		byte[] bytes = new byte[buf.size()]; // return the bytes from buffer as String of ascii encoded characters
		for (int i = 0; i < buf.size(); i++) {
			bytes[i] = buf.get(i);
		}
		return new String(bytes, CHARENC);
	}
	
	
	/**
	 * loads users from file
	 * @param path path to file to load users from
	 * @return the constructed set
	 * @throws FileNotFoundException  if the file does not exist
	 */
	public static Map<Long, LatitudeUser> loadUsers(String path) throws FileNotFoundException{
		Map<Long, LatitudeUser> users = new HashMap<>();
		
		File data = new File(path);
		var scanner = new Scanner(data);
		while(scanner.hasNextLine()) {
			String[] line = scanner.nextLine().split(":");
			Long id = Long.parseLong(line[0]);
			users.put(id, new LatitudeUser(id, line[1], line[2]));
		}	
		scanner.close();
		
		return users;
	}
	
	// https://stackoverflow.com/questions/80476/how-can-i-concatenate-two-arrays-in-java
	public static byte[] concat(byte[] first, byte[] second) {
		  byte[] result = Arrays.copyOf(first, first.length + second.length);
		  System.arraycopy(second, 0, result, first.length, second.length);
		  return result;
		}

}
