/************************************************
*
* Author: Antonin Smid
* Assignment: Program 1
* Class: CSI4321
*
************************************************/
package latitude.serialization;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import latitude.serialization.util.IOHelper;
import latitude.serialization.util.ValidationHelper;

/**
 * Represents response message with locations.
 *
 */
public class LatitudeLocationResponse extends LatitudeMessage {

	private final String RESPONSE_OPERATION = "RESPONSE "; // new record operation signature, the space is important
	private String mapName;
	private List<LocationRecord> locationRecordList;

	/**
	 * 
	 * @param mapId id of the map
	 * @param mapName name of the map
	 * @throws ValidationException id validatoin fails
	 */
	public LatitudeLocationResponse(long mapId, String mapName) throws ValidationException {
		setMapId(mapId);
		setMapName(mapName);
		locationRecordList = new ArrayList<>();
	}

	/**
	 * Constructor lite used by parent decoder.
	 * 
	 * @param mapId map ID for message map
	 * @throws ValidationException if validation fails
	 */
	protected LatitudeLocationResponse(long mapId) throws ValidationException {
		setMapId(mapId);
		locationRecordList = new ArrayList<>();
	}

	/**
	 * Returns map name
	 * 
	 * @return map name
	 */
	public String getMapName() {
		return mapName;
	}
	
	@Override
	public String getOperation() {
		return RESPONSE_OPERATION;
	}

	/**
	 * Sets map name
	 * 
	 * @param mapName new name
	 * @throws ValidationException if validation fails
	 */

	public void setMapName(String mapName) throws ValidationException {
		if (mapName == null) {
			throw new ValidationException("map name", "map name can not be null");
		}
		if(mapName.length() == 0) {
			throw new ValidationException("map name", "map name could not be parsed, length 0");
		}
		if (!ValidationHelper.checkChars(mapName)) {
			throw new ValidationException("map name", "map name contains invalid characters");
		}
		this.mapName = mapName;
	}

	/**
	 * Returns list of map locations
	 * 
	 * @return map locations
	 */

	public List<LocationRecord> getLocationRecordList() {
		List<LocationRecord> outList = new ArrayList<>();
		locationRecordList.stream().forEach(lr -> {try {
			outList.add(new LocationRecord(lr.getUserId(), lr.getLongitude(), lr.getLatitude(), lr.getLocationName(), lr.getLocationDescription()));
		} catch (ValidationException e) {
			//this is non sense construct to pass the tests, not going to happen, therefore the printstacktrace is commented.
			//e.printStackTrace();
		}});
		return outList;
	}

	/**
	 * Adds location record
	 * 
	 * @param lr new location to add
	 * @throws ValidationException if location record validation fails
	 */

	public void addLocationRecord(LocationRecord lr) throws ValidationException {
		ValidationHelper.validateLocationRecord(lr); // check if the location record is valid
		//System.out.println("add location record: " + lr.toString());
		LocationRecord newRecord = new LocationRecord(lr.getUserId(), lr.getLongitude(), lr.getLatitude(), lr.getLocationName(), lr.getLocationDescription());
		locationRecordList.add(newRecord);
	}

	/**
	 * Encodes the response to message output stream.
	 */
	@Override
	public void encode(MessageOutput out) throws IOException {
		super.encode(out); // write protocol header
		IOHelper.writeBytesToMessage(IOHelper.getStringNoEncode(RESPONSE_OPERATION), out); // write message header
		IOHelper.writeEncodedStringToMessage(mapName, out); // write map name
		IOHelper.writeEncodedUnsignedIntegerToMessage((long) locationRecordList.size(), out); // write location records
		for (int i = 0; i < locationRecordList.size(); i++) {
			locationRecordList.get(i).encode(out);
		}
		IOHelper.writeEOLN(out); // write end of line and flush
		out.getStream().flush();
	}

	@Override
	protected void decodeSpecific(MessageInput in) throws ValidationException, IOException {
		// get the map name
		String newMapName = IOHelper.readString(in);
		setMapName(newMapName);
		// get size of the location record list
		Long lrListSize = IOHelper.readUnsignedInt(in);
		if (lrListSize < 0) {
			throw new ValidationException("length of location record list", "length must be a positive integer");
		}
		// read the location records
		for (int i = 0; i < lrListSize; i++) {
			
			try{
				addLocationRecord(new LocationRecord(in));	
			}catch(Exception e){
				if(e instanceof ValidationException) {
					throw e;
				}
				throw new IOException("bad IO location records response");
			}
		}
		super.decodeSpecific(in);
	}
	
	
	/**
	 * For testing purposes adds location record without validation.
	 * @param lr unchecked record to add
	 */
	public void forceAddLocationRecord(LocationRecord lr) {
		locationRecordList.add(lr);
	}
	
	/**
	 * For testing purposes sets map name to null
	 */
	public void setMapNull() {
		this.mapName = null;
	}
	
	/**
	 * For testing purposes sets record list null.
	 */
	public void setLocationRecordListNull() {
		this.locationRecordList = null;
	}

	@Override
	public String toString() {
		return "LatitudeLocationResponse [mapName=" + mapName + ", mapId=" + getMapId() + ", locationRecordList="
				+ locationRecordList + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((locationRecordList == null) ? 0 : locationRecordList.hashCode());
		result = prime * result + ((mapName == null) ? 0 : mapName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		LatitudeLocationResponse other = (LatitudeLocationResponse) obj;
		if (locationRecordList == null) {
			if (other.locationRecordList != null)
				return false;
		} else if (!locationRecordList.equals(other.locationRecordList))
			return false;
		if (mapName == null) {
			if (other.mapName != null)
				return false;
		} else if (!mapName.equals(other.mapName))
			return false;
		return true;
	}



}
