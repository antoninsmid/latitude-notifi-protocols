/************************************************
*
* Author: Antonin Smid
* Assignment: Program 0 Test
* Class: CSI4321
*
************************************************/

package latitude.serialization;

/**
 * Exception for validation
 */
public class ValidationException extends Exception {

    /**
     * serialization uid
     */
    private static final long serialVersionUID = -8359463875150982348L;

    private String invalidToken;

    /**
     * Constructs validation exception
     * 
     * @param invalidToken token that failed validation
     * @param message      exception message
     * @param cause        exception cause
     */
    public ValidationException(String invalidToken, String message, Throwable cause) {
        super(message, cause);
        setInvalidToken(invalidToken);

    }

    /**
     * Constructs validation exception with null cause
     * 
     * @param invalidToken token that failed validation
     * @param message      exception message
     */
    public ValidationException(String invalidToken, String message) {
        super(message);
        setInvalidToken(invalidToken);
    }

    /**
     * Get token that failed validation
     * 
     * @return token that failed validation
     */
    public String getInvalidToken() {
        return invalidToken;

    }

    private void setInvalidToken(String invalidToken) {
        this.invalidToken = invalidToken;
    }

}
