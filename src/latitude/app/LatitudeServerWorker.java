/************************************************
*
* Author: Antonin Smid
* Assignment: Program 3
* Class: CSI4321
*
************************************************/

package latitude.app;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Map;
import java.util.logging.Logger;

import latitude.model.LatitudeMap;
import latitude.model.LocationRecordLibrary;
import latitude.serialization.LatitudeError;
import latitude.serialization.LatitudeLocationRequest;
import latitude.serialization.LatitudeLocationResponse;
import latitude.serialization.LatitudeMessage;
import latitude.serialization.LatitudeNewLocation;
import latitude.serialization.MessageInput;
import latitude.serialization.MessageOutput;
import latitude.serialization.ValidationException;

/**
 * Represents one runnable thread to provide functionality for clients.
 *
 */
public class LatitudeServerWorker implements Runnable {

	private ServerSocket socket;
	private Logger logger;
	private int timeout;
	private static Map<Long, LatitudeMap> maps;

	/**
	 * basic constructor for worker
	 * 
	 * @param socket  server side socket
	 * @param logger  prepared logger
	 * @param timeout how long until connection is closed, used for
	 */
	public LatitudeServerWorker(ServerSocket socket, Logger logger, int timeout) {
		this.socket = socket;
		this.logger = logger;
		this.timeout = timeout;	
	}
	
	public static void setMaps(Map<Long, LatitudeMap> maps) {
		LatitudeServerWorker.maps = maps;
	}
	

	@Override
	public void run() {

		// forever running loop
		while (true) {
			// wait and try to acccept client
			try {
				// System.out.println("waiting to accept client");
				Socket clientSocket = socket.accept();
				clientSocket.setSoTimeout(timeout); // client accepted
				logger.info("Handling client " + clientSocket.getInetAddress() + "-" + clientSocket.getPort()
						+ " with thread id " + Thread.currentThread().getId());

				InputStream in = clientSocket.getInputStream();
				OutputStream out = clientSocket.getOutputStream();
				MessageInput min = new MessageInput(in);
				MessageOutput mout = new MessageOutput(out);
				Boolean keepGoing = true;

				// loop for communication with a single client
				while (keepGoing) {

					keepGoing = processMessage(min, mout, logger);
					if(!keepGoing) {
						clientSocket.shutdownOutput();
						clientSocket.close();
					}				
							
				}
			} catch (IOException e) {
				if (e instanceof SocketTimeoutException) {
					// waiting for client to connect timed out, thread will call .accept() again
					// System.out.println("client to connect timed out, call .accept()" +
					// Thread.currentThread().getId());
//					logger.info("Connection waiting for client timed out, thread id " + Thread.currentThread().getId());
				} else {
					logger.info("IO exception, thread id " + Thread.currentThread().getId());
				}
			}
		}
	}
	
	
	protected static boolean processMessage(MessageInput min, MessageOutput mout, Logger logger) throws IOException {
		LatitudeMessage inputMessage;
		LatitudeMessage outputMessage;

		try {

			// try to decode the message
			inputMessage = LatitudeMessage.decode(min);

			if (inputMessage instanceof LatitudeNewLocation) { // received NEW
				LatitudeNewLocation lnl = (LatitudeNewLocation) inputMessage;

				// MAP ID not found
				if (!validateMapId(lnl.getMapId())) {
					outputMessage = createErrorNoSuchMap(lnl.getMapId());
					outputMessage.encode(mout);
					logger.info("New Location - Wrong Map ID: " + lnl.getMapId() + ", thread id: "
							+ Thread.currentThread().getId());
				}

				// ADD Location record
				String code = getLibrary(lnl.getMapId()).addLocation(lnl.getLocationRecord());
				// Success, send response.
				if (LocationRecordLibrary.SUCCESS_CODE.equals(code)) {
					outputMessage = createResponse(lnl.getMapId());
					outputMessage.encode(mout);
					logger.info("New Location ADDED. " + lnl.getLocationRecord().toString()
							+ ", thread id: " + Thread.currentThread().getId());
				} // fail USER ID not found
				else if (LocationRecordLibrary.USER_NOT_FOUND_CODE.equals(code)) {
					outputMessage = createErrorNoSuchUser(lnl.getMapId(),
							lnl.getLocationRecord().getUserId());
					outputMessage.encode(mout);
					logger.info("New Location - Wrong User ID: " + lnl.getLocationRecord().getUserId()
							+ ", thread id: " + Thread.currentThread().getId());
				}

			} // received ALL
			else if (inputMessage instanceof LatitudeLocationRequest) {
				LatitudeLocationRequest lrq = (LatitudeLocationRequest) inputMessage;
				if (!validateMapId(lrq.getMapId())) { // MAP ID not found
					outputMessage = createErrorNoSuchMap(lrq.getMapId());
					outputMessage.encode(mout);
					logger.info("Location Request ALL - Wrong Map ID: " + lrq.getMapId() + ", thread id: "
							+ Thread.currentThread().getId());
				} // map ID found, return the locations
				else {
					outputMessage = createResponse(lrq.getMapId());
					outputMessage.encode(mout);
					logger.info("Location Request ALL - Send Locations for Map ID: " + lrq.getMapId()
							+ ", thread id: " + Thread.currentThread().getId());
				}
			}
			// Received bad message type, not (NEW or ALL).
			else {
				outputMessage = createErrorUnexpectedOperation(inputMessage);
				outputMessage.encode(mout);
				logger.info("Wrong operation received: " + inputMessage.getOperation() + ", thread id: "
						+ Thread.currentThread().getId());
			}
		} // Handle bad messages, can not decode and IO problems
		catch (Exception e) {
			if (e instanceof ValidationException) {
				// Wrong protocol version
				if (LatitudeMessage.WRONG_VERSION.equals(e.getMessage())) {
					String wrongVersion = ((ValidationException) e).getInvalidToken();
					outputMessage = createErrorWrongVersion(wrongVersion);
					outputMessage.encode(mout);
					logger.info("Wrong protocol version: " + wrongVersion + ", thread id "
							+ Thread.currentThread().getId());
				} // unknown operation type used
				else if (LatitudeMessage.WRONG_TYPE.equals(e.getMessage())) {
					String type = ((ValidationException) e).getInvalidToken();
					outputMessage = createErrorUnknownOperation(type);
					outputMessage.encode(mout);
					logger.info("Wrong type of message: " + type + ", thread id "
							+ Thread.currentThread().getId());
				} else {
					String message = ((ValidationException) e).getMessage();
					outputMessage = createErrorValidationProblem(message);
					outputMessage.encode(mout);
					logger.info("Validation failed: " + message + ", thread id "
							+ Thread.currentThread().getId());
				}
			} else {
				if (e instanceof SocketException) {
					logger.info(
							"Client closed the connection, thread id " + Thread.currentThread().getId());

				} else if (e instanceof SocketTimeoutException) { // server closes connection
					logger.info("Server waiting for client timed out, thread id "
							+ Thread.currentThread().getId());

				} else if (e instanceof IOException) {
					if ("stream ended too early".equals(e.getMessage())) {
						logger.info(
								"Client closed connection, thread id " + Thread.currentThread().getId());

					} else {
						String message = e.getMessage();
						outputMessage = createErrorValidationProblem(message);
						outputMessage.encode(mout);
						logger.info("IO exception: " + message + ",thread id "
								+ Thread.currentThread().getId());
					}

				} else {
					logger.info(
							"Exception during message decode, thread id " + Thread.currentThread().getId());
				}

			}
			return false;
		}
		return true;
	}

	/**
	 * Create error - validation problem
	 * 
	 * @param message about the error
	 * @return latitude error object
	 */
	private static LatitudeError createErrorValidationProblem(String message) {
		LatitudeError error = null;
		try {
			error = new LatitudeError(0, message);
		} catch (ValidationException e) {
			// does not happen.
			e.printStackTrace();
		}
		return error;
	}

	/**
	 * Create error - wrong message type
	 * 
	 * @param version the wrong version received
	 * @return latitude error object
	 */
	private static LatitudeError createErrorUnknownOperation(String type) {
		LatitudeError error = null;
		try {
			error = new LatitudeError(0, "Unknown operation: " + type);
		} catch (ValidationException e) {
			// does not happen.
			e.printStackTrace();
		}
		return error;
	}

	/**
	 * Create error - wrong protocol version
	 * 
	 * @param version the wrong version received
	 * @return latitude error object
	 */
	private static LatitudeError createErrorWrongVersion(String version) {
		LatitudeError error = null;
		try {
			error = new LatitudeError(0, "Unexpected version: " + version);
		} catch (ValidationException e) {
			// does not happen.
			e.printStackTrace();
		}
		return error;
	}

	/**
	 * Create error based on unexpected message type
	 * 
	 * @param mapid requested map
	 * @return latitude error object
	 */
	private static LatitudeError createErrorUnexpectedOperation(LatitudeMessage message) {
		LatitudeError error = null;
		try {
			error = new LatitudeError(message.getMapId(), "Unexpected message type: " + message.getOperation());
		} catch (ValidationException e) {
			// does not happen.
			e.printStackTrace();
		}
		return error;
	}

	/**
	 * Create error based on non existent map id
	 * 
	 * @param mapid requested map
	 * @return latitude error object
	 */
	private static LatitudeError createErrorNoSuchMap(long mapId) {
		LatitudeError message = null;
		try {
			message = new LatitudeError(mapId, "No such map: " + mapId);
		} catch (ValidationException e) {
			// does not happen.
			e.printStackTrace();
		}
		return message;
	}

	/**
	 * Create error based on wrong user id
	 * 
	 * @param mapid  requested map
	 * @param userid wrong user id
	 * @return latitude error object
	 */
	private static LatitudeError createErrorNoSuchUser(long mapId, long userId) {
		LatitudeError message = null;
		try {
			message = new LatitudeError(mapId, "No such user: " + userId);
		} catch (ValidationException e) {
			// does not happen.
			e.printStackTrace();
		}
		return message;
	}

	/**
	 * Compose the response message with all current locations.
	 * 
	 * @return LatitudeLocationResponse massage with the actual positions
	 * @throws ValidationException if the records in library are corrupted
	 */
	private static LatitudeLocationResponse createResponse(long mapId) throws ValidationException {
		String mapName = getMapName(mapId);
		LatitudeLocationResponse message = new LatitudeLocationResponse(mapId, mapName);
		getLibrary(mapId).getLocationRecords().stream().forEach(lr -> {
			try {
				message.addLocationRecord(lr);
			} catch (ValidationException e) {
				// should not happen if the library is not corrupted.
				e.printStackTrace();
			}
		});
		return message;
	}

	/**
	 * Check if the server has map with this id.
	 * 
	 * @param id to check
	 * @return if there is map with this id
	 */
	private static boolean validateMapId(Long id) {
		return maps.containsKey(id);
	}

	/**
	 * Returns the location record library associated with provided id.
	 * 
	 * @param id of the map to use
	 * @return the location record library of the map
	 * @throws Validation Exception if map id does not exits
	 */
	private static LocationRecordLibrary getLibrary(Long id) throws ValidationException {
		if (!validateMapId(id)) {
			throw new ValidationException("mapid", "map id " + id + " does not exist");
		}
		return maps.get(id).getLibrary();
	}

	/**
	 * Returns the name associated with provided id.
	 * 
	 * @param id of the map to find
	 * @return the name of the map
	 * @throws Validation Exception if map id does not exits
	 */
	private static String getMapName(Long id) throws ValidationException {
		if (!validateMapId(id)) {
			throw new ValidationException("mapid", "map id " + id + " does not exist");
		}
		return maps.get(id).getName();
	}
}
