/************************************************
*
* Author: Antonin Smid
* Assignment: Program 3
* Class: CSI4321
*
************************************************/

package latitude.app;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import latitude.model.LatitudeMap;
import latitude.model.LatitudeUser;
import latitude.serialization.util.IOHelper;
import mapservice.MapBoxObserver;
import mapservice.MapManager;
import mapservice.MemoryMapManager;
import notifi.app.NoTiFiServer;

/**
 * Class representing the latitude server.
 *
 */
public class LatitudeServer {

	// File containing JS-encoded array of locations
	private static final String LOCATION_FILE = "src/latitude/markers.js";
	// Timeout on any blocking operations considering the server sockets
	private static int CONNECTION_TIMEOUT = 50000;
	// File to save server log
	private static final String LOGGER_FILE_NAME = "connections.log";
	// ID of the map we are using for this assignment
	private static final long OUR_MAP_ID = 345L;
	
	// Available maps
	private static Map<Long, LatitudeMap> maps = new HashMap<>();

	/**
	 * Runs Latitude Server
	 * 
	 * @param args server port, number of threads in pool, path to password file with users
	 * @throws IOException If IO exception occurs
	 */
	public static void main(String[] args) throws IOException {
	
	
		if (args.length != 3) { // test correct # of args
			throw new IllegalArgumentException("Parameters: <port>, <thread pool size>, <password file name>");
		}
	
		// logger to file
		final Logger logger = Logger.getLogger("GlobalLogger");
		logger.setUseParentHandlers(false);
		final FileHandler fileHandler = new FileHandler(LOGGER_FILE_NAME);
		logger.addHandler(fileHandler);
		SimpleFormatter simpleFormatter = new SimpleFormatter();
		fileHandler.setFormatter(simpleFormatter);

		int serverPort = Integer.parseInt(args[0]);
		int threadPoolSize = Integer.parseInt(args[1]);

		// create server socket to accept the connections
		final ServerSocket serverSocket = new ServerSocket(serverPort);
		// the socket can be reused immediately, important for quick restart
		// details at https://bit.ly/2EgKSvK
		serverSocket.setReuseAddress(true);
		serverSocket.setSoTimeout(CONNECTION_TIMEOUT);
		
		// Create map manager in memory
		MapManager mapManager = new MemoryMapManager();
		// Register listener to update MapBox location file
		mapManager.register(new MapBoxObserver(LOCATION_FILE, mapManager));
		
		// init set with users, ids and passwords
		Map<Long, LatitudeUser> users = IOHelper.loadUsers(args[2]);
		
		
		//init NoTiFi server
		NoTiFiServer notifiServer = new NoTiFiServer(serverPort, logger);
		Thread notifiThread = new Thread(notifiServer);
		notifiThread.start();
			
		maps.put(OUR_MAP_ID , new LatitudeMap(OUR_MAP_ID, "FirstMap", mapManager, users, notifiServer));
		LatitudeServerWorker.setMaps(maps);
		
		// spawn the threads
		for (int i = 0; i < threadPoolSize; i++) {
			Thread thread = new Thread(new LatitudeServerWorker(serverSocket, logger, CONNECTION_TIMEOUT));
			thread.start();
			logger.info("Created and started Thread id " + thread.getId());
		}
	}


	
}
