/************************************************
*
* Author: Antonin Smid
* Assignment: Program 2
* Class: CSI4321
*
************************************************/

package latitude.app;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import latitude.serialization.LatitudeError;
import latitude.serialization.LatitudeLocationRequest;
import latitude.serialization.LatitudeLocationResponse;
import latitude.serialization.LatitudeMessage;
import latitude.serialization.LatitudeNewLocation;
import latitude.serialization.LocationRecord;
import latitude.serialization.MessageInput;
import latitude.serialization.MessageOutput;
import latitude.serialization.ValidationException;
import latitude.serialization.util.ValidationHelper;

/**
 * Console client application allowing to read and send positions
 *
 */
public class LatitudeClientApp {

//	private static final int SERVER_PORT = 12345;
//	private static final String SERVER_NAME = "127.0.0.1";
	
	// codes to control while loop from inner methods
	private static final int CODE_CONTINUE = -1;
	private static final int CODE_BREAK = -2;
	private static final int CODE_VALID = -0;
	// operation codes
	private static final String ALL_OPERATION = "ALL";
	private static final String NEW_OPERATION = "NEW";
	private static final String NON_EXISTING_OPERATION = "FAIL";
	private static final String SERVER_DISCONNECTS = "Server unavailable";
	
	private static final int CONNECTION_TIMEOUT = 5000;

	/**
	 * Main method running the console client application
	 * 
	 * @param args no arguments
	 * @throws UnknownHostException if host server does not exist
	 * @throws IOException          if IO problem
	 * @throws InterruptedException  if Interrupted Exception
	 */
	public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException {
		
		if (args.length != 2) { // test correct # of args
			//throw new IllegalArgumentException("Parameters: <server idntity>, <port>");
			System.err.println("Wrong number of parameters. Should be: <server idntity>, <port>");
			return;
		}
		
		String SERVER_NAME = args[0];
		int SERVER_PORT;
		try {			
			SERVER_PORT = Integer.parseInt(args[1]);
		}catch(Exception e) {
			System.err.println("Wrong format of port number, could not parse it.");
			return;
			//throw new IllegalArgumentException("Could not parse the port number.");
		}

		// open server connection on startup
		Socket socket; // connection
		InputStream in; // streams from socket
		OutputStream out;
		MessageInput min; // latitude protocol messages
		MessageOutput mout;
		
		try {
			
			// initialize connection to server
			socket = new Socket(SERVER_NAME, SERVER_PORT);
			socket.setSoTimeout(CONNECTION_TIMEOUT);
			in = socket.getInputStream();
			out = socket.getOutputStream();
			min = new MessageInput(in);
			mout = new MessageOutput(out);

			// scanner to harvest user input
			Scanner userInputScanner = new Scanner(System.in);

			boolean keepGoing = true; // flag if the program should keep working
			boolean firstTime = true; // flag if this is the first while loop

			// main loop
			while (keepGoing) {
				
				// prompt if user wishes to continue, do not the first time
				if (!firstTime) {
					if (!promptContinue(userInputScanner)) {
						break;
					}
				} else {
					firstTime = false;
				}

				// let user pick the operation - all/new
				String operation = promptOperation(userInputScanner);
				LatitudeMessage outMessage; // future message to send

				if (ALL_OPERATION.equals(operation)) {
					// all prompt values and create
					try {
						outMessage = createLocationRequest(userInputScanner);
					}catch(ValidationException e) {
						firstTime = true;
						continue;
					}
					// send message
					outMessage.encode(mout);
					// handle server response
					int serverResponse = acceptResponse(min);
					if (serverResponse == CODE_CONTINUE) {
						continue;
					} else if (serverResponse == CODE_BREAK) {
						break;
					}
				} else if (NEW_OPERATION.equals(operation)) {
					// new prompt values and create
					try {
						outMessage = createNewLocation(userInputScanner);
					}catch(ValidationException e) {
						firstTime = true;
						continue;
					}
					
					outMessage.encode(mout);
					// handle server response
					int serverResponse = acceptResponse(min);	
					if (serverResponse == CODE_CONTINUE) {
						continue;
					} else if (serverResponse == CODE_BREAK) {
						break;
					}
				} else { // wrong user operation input, ask again, don't prompt for continue
					firstTime = true;
					continue;
				}
			}
			// close connection on exit
			socket.close();
		} catch (IOException e) { // can't connect to server, end
			printServerCommProblem("server socket problem");
		}
	}

	/**
	 * Method handles the server response accept
	 * 
	 * @param min InputMessage with response
	 * @return while loop control code (continue, break, valid)
	 * @throws InterruptedException 
	 */
	private static int acceptResponse(MessageInput min) throws InterruptedException {
		LatitudeMessage response;
		try {
			response = LatitudeMessage.decode(min); // try to decode the message
			if ((response instanceof LatitudeError)) { // received error
				printError(((LatitudeError) response).getErrorMessage());
				if(SERVER_DISCONNECTS.equals(((LatitudeError) response).getErrorMessage())) {
					return CODE_BREAK;
				}
				TimeUnit.MILLISECONDS.sleep(100);
				return CODE_CONTINUE;
			}
			if (!(response instanceof LatitudeLocationResponse)) { // received a different message
				printUnexpectedMessage("received an unexpected message. Not location response type.");
				return CODE_CONTINUE;
			}
			printResponse((LatitudeLocationResponse) response); // correct message, lets print it
			return CODE_VALID;
		} catch (ValidationException e) {
			//System.out.println("validation problem");
			printInvalidMessage("the received message was invalid "); // bad message
			return CODE_BREAK;
		} catch (IOException e) {			
			//System.out.println("IOproblem");		
			printServerCommProblem("server communication failed"); // failed communication
			return CODE_BREAK;
		}
	}

	/**
	 * Creates NewLocation object based on user's input
	 * 
	 * @param scanner to read user input from
	 * @return newLocation
	 * @throws ValidationException 
	 */
	private static LatitudeNewLocation createNewLocation(Scanner scanner) throws ValidationException {

		LocationRecord locationRecord = new LocationRecord();

		Long mapId = promptUnsignedInteger(scanner, "MapID");
		Long userId = promptUnsignedInteger(scanner, "UserID");
		String longitude = promptDouble(scanner, "Longitude", 180);
		String latitude = promptDouble(scanner, "Latitude", 90);
		String locationName = promptText(scanner, "Location Name");
		String locationDescription = promptText(scanner, "Location Description");
		try { // try to create the object - fires all validations
			locationRecord = new LocationRecord(userId, longitude, latitude, locationName, locationDescription);
			LatitudeNewLocation message = new LatitudeNewLocation(mapId, locationRecord);
			return message;
		} catch (Exception e) { // if validations failed, try again
			printInvalidUserInputMessage("Wrong location record input values.");
			throw new ValidationException("new location user input data", "user input failed");
		}
	}

	/**
	 * Creates LocationRequest based on user's input
	 * 
	 * @param scanner to read user input from
	 * @return new location request
	 * @throws ValidationException 
	 */
	private static LatitudeLocationRequest createLocationRequest(Scanner scanner) throws ValidationException {
		// get map id
		Long mapIdParsed = promptUnsignedInteger(scanner, "MapID");
		try { // create and return the location request, fires validations
			return new LatitudeLocationRequest(mapIdParsed);
		} catch (ValidationException e) {
			printInvalidUserInputMessage("Map ID invalid.");
			throw new ValidationException("map request user inpu", "user input failed");
		}
	}

	/**
	 * Get text from user, used for names and descriptions
	 * 
	 * @param scanner to load users input from
	 * @param name    of the field to read
	 * @return the text
	 * @throws ValidationException 
	 */
	private static String promptText(Scanner scanner, String name) throws ValidationException {
		System.out.print(name + "> ");
		String text = scanner.nextLine();
		if (!ValidationHelper.checkChars(text)) {
			printInvalidUserInputMessage(name + " can contain only ascii characters.");
			throw new ValidationException(name, "user input failed");
		}
		return text;
	}

	/**
	 * Get double value as string from user. Used for latitude, longitude, contains
	 * limit validation.
	 * 
	 * @param scanner to load user input from
	 * @param name    name of the field to load
	 * @param limit   maximum absolute value of the input
	 * @return double inserted by used as string in latitude format <double string>
	 * @throws ValidationException 
	 */
	private static String promptDouble(Scanner scanner, String name, int limit) throws ValidationException {
		System.out.print(name + "> ");
		String doubleRaw = scanner.nextLine();
		if (doubleRaw.length() == 0) {
			printInvalidUserInputMessage(name + " can not be empty.");
			throw new ValidationException(name, "user input failed");
		}
		try { // try to parse input
			Double doubleParsed = Double.parseDouble(doubleRaw);
			if (Math.abs(doubleParsed) > limit) { // check limits
				throw new ValidationException("double user input", "out of bounds");
			}
			String outputFormatted = Double.toString(doubleParsed); // ensure correct formatting
			if (!outputFormatted.contains(".")) {
				outputFormatted = outputFormatted + ".00";
			}
			ValidationHelper.validatePositionDouble(outputFormatted, name, limit); // validate result
			return outputFormatted;
		} catch (Exception e) {
			printInvalidUserInputMessage("Poorly formatted double");
			//printInvalidMessage("Poorly formatted double");
			throw new ValidationException(name, "user input failed");
		}
	}

	/**
	 * Get unsigned integer as long from user
	 * 
	 * @param scanner to load user input from
	 * @param name    of the field
	 * @return new unsigned integer
	 * @throws ValidationException 
	 */
	private static Long promptUnsignedInteger(Scanner scanner, String name) throws ValidationException {
		System.out.print(name + "> ");
		String mapIdRaw = scanner.nextLine();
		if (mapIdRaw.length() == 0) {
			printInvalidUserInputMessage(name + " can not be empty.");
			throw new ValidationException(name, "user input failed");
		}
		Long mapIdParsed = -1l;
		try {
			mapIdParsed = Long.valueOf(mapIdRaw);
			ValidationHelper.validateUnsignedInt(mapIdParsed);
			return mapIdParsed;
		} catch (Exception e) {
			printInvalidUserInputMessage(name + " must be a positive integer less than 2^32-1.");
			throw new ValidationException(name, "user input failed");
		}
	}

	/**
	 * Let user choose the next operation to do
	 * 
	 * @param scanner to load user input from
	 * @return operation as String
	 * @throws InterruptedException 
	 */
	private static String promptOperation(Scanner scanner) {
		System.out.print("Operation> ");
		String operation = scanner.nextLine();
		if (ALL_OPERATION.equals(operation)) {
			return ALL_OPERATION;
		} else if (NEW_OPERATION.equals(operation)) {
			return NEW_OPERATION;
		}		
		printInvalidUserInputMessage("Unknown operation: " + operation);
		//printInvalidMessage("Unknown operation: " + operation);
		return NON_EXISTING_OPERATION;
	}

	/**
	 * Let user choose to continue or stop the program, validates y/n
	 * 
	 * @param scanner to load user input from
	 * @return yes or no, raw user input y/n
	 */
	private static boolean promptContinue(Scanner scanner) {
		System.out.print("Continue (y/n)> ");
		String endUserAnswer = scanner.nextLine();
		if ("y".equals(endUserAnswer)) {
			return true;
		} else if ("n".equals(endUserAnswer)) {
			return false;
		}
		printInvalidUserInputMessage("Continue needs to be y for yes or n for no.");
		return promptContinue(scanner);
	}

	/**
	 * Prints the LatitudeLocationRecord to standard output
	 * 
	 * @param response LocationResponse object to print
	 */
	private static void printResponse(LatitudeLocationResponse response) {
		System.out.println("mapID=" + response.getMapId() + " - Location Response");
		for (int i = 0; i < response.getLocationRecordList().size(); i++) {
			LocationRecord record = response.getLocationRecordList().get(i);
			System.out.println("User " + record.getUserId() + ": " + record.getLocationName() + " - "
					+ record.getLocationDescription() + " at (" + record.getLongitude() + ", " + record.getLatitude()
					+ ")");
		}
	}

	/**
	 * Print invalid user input message
	 * 
	 * @param message detail
	 * @throws InterruptedException 
	 */
	private static void printInvalidUserInputMessage(String message) {
		//System.err.println("Invalid user input: " + message);
		System.err.println("Invalid message: " + message);
		try {
			TimeUnit.MILLISECONDS.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Print server communication problem message
	 * 
	 * @param message detail
	 */
	private static void printServerCommProblem(String message) {
		System.err.println("Unable to communicate: " + message);
		System.exit(0);
	}

	/**
	 * Print invalid input message warning
	 * 
	 * @param message detail
	 */
	private static void printInvalidMessage(String message) {
		System.err.println("Invalid Message: " + message);
		System.exit(0);
	}

	/**
	 * Print unexpected message warning
	 * 
	 * @param message detail
	 */
	private static void printUnexpectedMessage(String message) {
		System.err.println("Unexpected Message: " + message);
		try {
			TimeUnit.MILLISECONDS.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Print error message
	 * 
	 * @param message error detail
	 */
	private static void printError(String message) {
		System.err.println("Error: " + message);
		try {
			TimeUnit.MILLISECONDS.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
