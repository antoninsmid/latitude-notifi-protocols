/************************************************
*
* Author: Antonin Smid
* Assignment: Program 7
* Class: CSI4321
*
************************************************/

package latitude.app;

import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import latitude.model.LatitudeMap;
import latitude.model.LatitudeUser;
import latitude.serialization.MessageInput;
import latitude.serialization.MessageOutput;
import latitude.serialization.util.IOHelper;
import latitude.serialization.util.MessageNIODeframer;
import mapservice.MapBoxObserver;
import mapservice.MapManager;
import mapservice.MemoryMapManager;
import notifi.app.NoTiFiServer;

/**
 * Latitude Server with asynchronous message handling
 * Behaves the same way as the latitude server, but does not have any thread pool,
 * instead uses nio.channels async api to handle parts of the messages when they come
 * 
 * Based on BasicTCPEchoAIO.java example provided by jeffdonahoo.com
 */
public class LatitudeServerAIO {

	// File containing JS-encoded array of locations
	private static final String LOCATION_FILE = "src/latitude/markers.js";
	// File to save server log
	private static final String LOGGER_FILE_NAME = "connectionsAIO.log";
	// ID of the map we are using for this assignment
	private static final long OUR_MAP_ID = 345L;
	// Buffer size (bytes)
	private static final int BUFSIZE = 512;
	// opened channels and their deframers
	private static Map<AsynchronousSocketChannel, MessageNIODeframer> clients = new HashMap<>();
	// basic logger
	private static Logger logger;

	// Available maps
	private static Map<Long, LatitudeMap> maps = new HashMap<>();

	/**
	 * The main method running the server, does the setup and then passively blocks.
	 *  
	 * @param args port and the passwords file
	 * @throws IOException if IO exception happens
	 */
	public static void main(String[] args) throws IOException {

		if (args.length != 2) { // test correct # of args
			throw new IllegalArgumentException("Parameters: <port>, <password file name>");
		}

		// logger to file
		logger = Logger.getLogger("GlobalLogger");
		logger.setUseParentHandlers(false);
		final FileHandler fileHandler = new FileHandler(LOGGER_FILE_NAME);
		logger.addHandler(fileHandler);
		SimpleFormatter simpleFormatter = new SimpleFormatter();
		fileHandler.setFormatter(simpleFormatter);

		//setup server port
		int serverPort = Integer.parseInt(args[0]);

		// Create map manager in memory
		MapManager mapManager = new MemoryMapManager();
		// Register listener to update MapBox location file
		mapManager.register(new MapBoxObserver(LOCATION_FILE, mapManager));

		// init set with users, ids and passwords
		Map<Long, LatitudeUser> users = IOHelper.loadUsers(args[1]);

		// init NoTiFi server
		NoTiFiServer notifiServer = new NoTiFiServer(serverPort, logger);
		Thread notifiThread = new Thread(notifiServer);
		notifiThread.start();

		// init the maps (all management around saving the current locations etc.)
		maps.put(OUR_MAP_ID, new LatitudeMap(OUR_MAP_ID, "FirstMap", mapManager, users, notifiServer));
		LatitudeServerWorker.setMaps(maps);

		// setup async IO
		try (AsynchronousServerSocketChannel listenChannel = AsynchronousServerSocketChannel.open()) {
			// Bind local port
			listenChannel.bind(new InetSocketAddress(serverPort));

			// Create accept handler
			listenChannel.accept(null, new CompletionHandler<AsynchronousSocketChannel, Void>() {

				@Override
				public void completed(AsynchronousSocketChannel clntChan, Void attachment) {
					listenChannel.accept(null, this);
					try {
						handleAccept(clntChan, logger);
					} catch (IOException e) {
						failed(e, null);
					}
				}

				@Override
				public void failed(Throwable e, Void attachment) {
					logger.log(Level.WARNING, "Close Failed", e);
				}
			});
			// Block until current thread dies
			Thread.currentThread().join();
		} catch (InterruptedException e) {
			logger.log(Level.WARNING, "Server Interrupted", e);
		}
	}

	/**
	 * Called after each accept completion
	 * 
	 * @param clntChan channel of new client
	 * @param logger logger to pass to log the activity
	 * @throws IOException if I/O problem
	 */
	public static void handleAccept(final AsynchronousSocketChannel clntChan, Logger logger) throws IOException {
		
		ByteBuffer buf = ByteBuffer.allocateDirect(BUFSIZE);
		clients.put(clntChan, new MessageNIODeframer());
		//System.out.println("accepted, total clients: " + clients.size());
		clntChan.read(buf, buf, new CompletionHandler<Integer, ByteBuffer>() {
			public void completed(Integer bytesRead, ByteBuffer buf) {
				try {
					handleRead(clntChan, buf, bytesRead, logger);
				} catch (IOException e) {
					logger.log(Level.WARNING, "Handle Read Failed", e);
				}
			}

			public void failed(Throwable ex, ByteBuffer v) {
				closeChannel(clntChan);
			}
		});
	}
	
	/**
	 * Called after each read completion
	 * 
	 * @param clntChan channel of new client
	 * @param buf byte buffer used in read
	 * @param bytesRead how many bytes were read   
	 * @param logger	logger to log activity
	 * @throws IOException	if I/O problem
	 */
	public static void handleRead(final AsynchronousSocketChannel clntChan, ByteBuffer buf, int bytesRead, Logger logger)
			throws IOException {
		if (bytesRead == -1) { // Did the other end close?
			closeChannel(clntChan);
		} else if (bytesRead > 0) {
			// ************** process messages here *****************
			MessageNIODeframer framer = clients.get(clntChan);
			//System.out.println("Handle Read with " + framer.toString());
			// append latest received data
			framer.addData(buf, bytesRead);
			buf.clear();
			// Retrieve all messages to process
			List<MessageInput> msgs = new LinkedList<>();
			MessageInput in = null;
			do {
				in = framer.hasNext();
				if (in != null) {
					msgs.add(in);
				}
			} while (in != null);

			// no complete messages, back to reading
			if (msgs.isEmpty()) {
				clntChan.read(buf, buf, new CompletionHandler<Integer, ByteBuffer>() {
					public void completed(Integer bytesRead, ByteBuffer buf) {
						try {
							handleRead(clntChan, buf, bytesRead, logger);
						} catch (IOException e) {
							logger.log(Level.WARNING, "Handle Read Failed", e);
						}
					}

					public void failed(Throwable ex, ByteBuffer v) {
						closeChannel(clntChan);
					}
				});
			} else { // some complete messages, process them	
				List<MessageOutput> responses = new LinkedList<>();
				boolean keepGoing = true;	// if false, the channel should be closed
				for(MessageInput min:msgs) {	
					if(keepGoing) {
						OutputStream outs = new ByteArrayOutputStream(BUFSIZE);
						MessageOutput mout = new MessageOutput(outs);	
						try {
							keepGoing = LatitudeServerWorker.processMessage(min, mout, logger);
						} catch (IOException e) {
							keepGoing = false;
						}
						responses.add(mout); // save the response to be sent later						
					}
				}
				// allocate the buffer to send responses and copy them over from the messageOutputs
				byte[] outArray = new byte[0];
				for(MessageOutput mout : responses) {
					outArray = IOHelper.concat(outArray, ((ByteArrayOutputStream)mout.getStream()).toByteArray());
				}			
				ByteBuffer outBuffer = ByteBuffer.allocate(outArray.length);
				outBuffer.put(outArray);
				outBuffer.flip();
				// send the response messages
				handleWrite(clntChan, outBuffer, buf, logger, keepGoing);
			}
		}
	}

	/**
	 * Called after each write
	 * 
	 * @param clntChan channel of new client
	 * @param bufWrite byte buffer used in write
	 * @param bufRead byte buffer used for future read
	 * @param logger logger to log activity
	 * @param keepGoing flag if the channel should be closed after writing
	 * @throws IOException if I/O problem
	 */
	public static void handleWrite(final AsynchronousSocketChannel clntChan, ByteBuffer bufWrite, ByteBuffer bufRead, Logger logger, boolean keepGoing) throws IOException {
		if (bufWrite.hasRemaining()) { // More to write
			clntChan.write(bufWrite, bufWrite, new CompletionHandler<Integer, ByteBuffer>() {
				public void completed(Integer bytesWritten, ByteBuffer buf) {
					try {
						handleWrite(clntChan, bufWrite, bufRead, logger, keepGoing);
					} catch (IOException e) {
						logger.log(Level.WARNING, "Handle Write Failed", e);
					}
				}

				public void failed(Throwable ex, ByteBuffer buf) {
					closeChannel(clntChan);
				}
			});
		} else { // Back to reading
			if(keepGoing) {
				bufRead.clear();
				clntChan.read(bufRead, bufRead, new CompletionHandler<Integer, ByteBuffer>() {
					public void completed(Integer bytesRead, ByteBuffer buf) {
						try {
							handleRead(clntChan, bufRead, bytesRead, logger);
						} catch (IOException e) {
							logger.log(Level.WARNING, "Handle Read Failed", e);
						}
					}

					public void failed(Throwable ex, ByteBuffer v) {
						closeChannel(clntChan);
					}
				});				
			}else {
				closeChannel(clntChan);
			}
		}
	}

	/**
	 * Closes channel and clears the memory from MessageNIODeframer
	 * @param clntChan clients channel to close down
	 */
	public static void closeChannel(final AsynchronousSocketChannel clntChan) {
		//System.out.println("Closing " + clients.get(clntChan));
		clients.remove(clntChan);
		try {
			clntChan.close();
		} catch (IOException e) {
			logger.log(Level.WARNING, "Close Failed", e);
		}
	}

}
