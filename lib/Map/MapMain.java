import mapservice.Location;
import mapservice.MapBoxObserver;
import mapservice.MapManager;
import mapservice.MemoryMapManager;

/**
 * Demonstration of map manager and maker. This should create a file,
 * markers.js, with the specified points.
 */
public class MapMain {
    /**
     * File containing JS-encoded array of locations
     */
    private static final String LOCATIONFILE = "markers.js";

    /**
     * Test main
     * 
     * @param args ignored
     */
    public static void main(String[] args) {
        // Create map manager in memory
        MapManager mgr = new MemoryMapManager();
        // Register listener to update MapBox location file
        mgr.register(new MapBoxObserver(LOCATIONFILE, mgr));
        // Add first location
        mgr.addLocation(new Location("One: XXX", "3", "4", "One place", Location.Color.BLUE));
        // Location file should now contain one location

        // Add second location
        mgr.addLocation(new Location("two: YYY", "4", "5", "Two place", Location.Color.GREEN));
        // Location file should now contain two locations

        // Re-add first location
        mgr.addLocation(new Location("One: ZZZ", "6", "7", "New place", Location.Color.RED));
    }
}
